package testcases;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import application.server.AufgabenverwaltungServer;
import application.server.SqlStatements;
import application.server.WGServer;

public class AufgabenverwaltungTest {
	public static void main(String[] args) {
		try{
			
			//Connect
			java.sql.Connection connection;
			connection = DriverManager.getConnection( 
					"jdbc:mysql://localhost:3306/", "wgtest", "wgtest");
			Statement stm = connection.createStatement(); 
			System.out.println("Drop database;");
			
			//Clean DB
			stm.execute("drop database Test;"); 
			
			//Create fresh db
			WGServer.main("--simulate -u wgtest -p wgtest -d Test".split(" "));
			
			//Insert Aufgaben and people
			stm.close();
			
			SqlStatements statems = SqlStatements.getSqlStatements();
			PreparedStatement ps = connection.prepareStatement(statems.insertWG);
			ps.setString(1, "Test WG 1");
			ps.execute();
			ps.close();
			ps = connection.prepareStatement(statems.getWGs);
			ps.execute();
			ResultSet rs = ps.getResultSet();
			rs.next();
			int wg_id1 = rs.getInt(1);
			ps.close();
			ps = connection.prepareStatement(statems.insertUser);
			ps.setInt(4, wg_id1);
			ps.setString(1, "Fritz");
			ps.setString(2, "Mueller");
			ps.setString(3, "fmueller");
			ps.executeUpdate();
			
			ps.setString(1, "Hans-Peter");
			ps.setString(2, "Koenig");
			ps.setString(3, "hpkoenig");
			ps.executeUpdate();
			

			ps.setString(1, "Dieter");
			ps.setString(2, "Heck");
			ps.setString(3, "dtheck");
			ps.executeUpdate();
			

			ps.setString(1, "David");
			ps.setString(2, "Michelangelo");
			ps.setString(3, "dmichelang");
			ps.executeUpdate();
						
			System.out.println("Menschen eingefügt.");
			
			ps.close();
			ps = connection.prepareStatement(statems.insertAufgabe);
			ps.setString(1, "Treppe kehren");
			ps.setString(2, "");
			ps.setInt(3, 1);
			ps.setInt(4, 64+4);
			ps.setInt(5, wg_id1);
			ps.execute();
			
			ps.setString(1, "Bad Putzen");
			ps.setString(2, "");
			ps.setInt(3, 2);
			ps.setInt(4, 32+2);
			ps.setInt(5, wg_id1);
			ps.execute();
			
			System.out.println("Aufgaben eingefuegt.");
			Thread t = new AufgabenverwaltungServer(connection);
			t.start();
			t.join();
			System.out.println("Aufgaben zugewiesen.");
			
			//Manuell nachschauen im SQL Server
		}catch(SQLException | InterruptedException e){
			e.printStackTrace();
		}
		//legt eine Datenbank an.
	}
}
