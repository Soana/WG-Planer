package testcases;

import java.util.GregorianCalendar;

import javax.swing.JFrame;

import application.client.gui.background.BackgroundWindow;
import application.client.gui.background.LogInPanel;
import application.client.gui.background.LoggedInPanel;
import application.client.logik.LogicController;


public class TestFrame extends BackgroundWindow {

	public TestFrame(){

		
		String[] wgs = {"A", "B", "C"};
		System.out.println("TestFrame: " + wgs);
		this.setContentPane(new LoggedInPanel(this));
		this.setSize(500, 500);
		this.setVisible(true);
	}
	
	public static void main(String[] args){
		LogicController.connect("localhost", 1234);
		LogicController.checkLogin("", new char[]{});
		BackgroundWindow test = new BackgroundWindow();
		test.setMode(Mode.LOGGED_IN);
		test.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		test.setVisible(true);
	}
}
