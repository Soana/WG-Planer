package testcases;

import java.util.Date;

import javax.swing.JFrame;

import application.client.gui.background.BackgroundWindow;
import application.client.gui.foreground.EinkaufslistePanel;
import application.client.gui.foreground.EinstellungenPanel;
import application.client.gui.foreground.EintragenPanel;
import application.client.gui.foreground.KalenderPanel;
import application.client.gui.foreground.KassePanel;
import application.client.gui.foreground.MitbewohnerVerwaltenPanel;
import application.client.logik.Bewohner;
import application.client.logik.DataObject;
import application.client.logik.LogicController;


public class InternalTest extends BackgroundWindow {

	public InternalTest(){
		super();
		LogicController.connect("localhost", 1234);
//		this.add(new EintragenPanel(this, DataObject.getInstance(new Bewohner("acvb","bcvb", new Date(), "01234", 3))));
//		this.add(new MitbewohnerVerwaltenPanel(this, DataObject.getInstance(new Bewohner("acvb","bcvb", new Date(), "01234", 3))));
//		this.add(new EinkaufslistePanel(this, DataObject.getInstance(new Bewohner("acvb","bcvb", new Date(), "01234", 3))));
//		this.add(new EinstellungenPanel(this, DataObject.getInstance(new Bewohner("acvb","bcvb", new Date(), "01234", 3))));
//		this.add(new KalenderPanel(this, DataObject.getInstance(new Bewohner("acvb","bcvb", new Date(), "01234", 3))));
//		this.add(new MitbewohnerPanel(this));
//		this.add(new KassePanel(this));
		
	}
	
	public static void main(String[] args){
		InternalTest frame = new InternalTest();
		frame.setSize(640, 480);
		frame.setVisible(true);
//		while(true){
//			System.out.println(frame.getSize());
//		}
	}
}
