package testcases;

import java.io.IOException;
import java.net.ServerSocket;

import org.json.JSONObject;

import application.shared.network.Connection;
import application.shared.network.RequestHandler;
import application.shared.network.ResponseHandler;


public class ServerT {

	public static void main(String[] args){
		ServerSocket s;
		try {
			s = new ServerSocket(1234);
		
		
			System.out.println("I am the Server, please don't kill me!");
			
			while(true){
				Connection con = new Connection(s.accept());
				System.out.println("accepted!");
				con.registerRequestHandler("add_bewohner", new RequestHandler(){

					@Override
					public JSONObject onRequest(JSONObject jObj) {
						System.out.println("REquest!");
						JSONObject json = new JSONObject();
						json.put("vorname", jObj.get("vorname"));
						json.put("nachname", jObj.get("nachname"));
						json.put("id", 123);
						json.put("vorname", "" + jObj.getString("vorname").charAt(0) + jObj.getString("vorname").charAt(1) + jObj.getString("nachname").charAt(0));
						return json;
					}

					
				});
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
