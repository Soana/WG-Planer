package application.shared.data;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import application.client.logik.Bewohner;


public class Termin {

	private long id;
	private Bewohner ersteller;
	private Bewohner[] teilnehmer;
	private Calendar bis;
	private Calendar von;
	private String name;
	private boolean ganzTags;

	public Termin(Bewohner ersteller, String name, GregorianCalendar von,
			GregorianCalendar bis, Bewohner[] teilnehmer, boolean ganzTags, long id) {
		this(ersteller, name, von, bis, teilnehmer, ganzTags);
		this.id = id;
	}
	public Termin(Bewohner ersteller, String name, Calendar von,
			Calendar bis, Bewohner[] teilnehmer, boolean ganzTags) {
		this.ersteller = ersteller;
		this.name = name;
		von.set(Calendar.SECOND, 0);
		this.von = von;
		bis.set(Calendar.SECOND, 0);
		this.bis = bis;
		this.teilnehmer = teilnehmer;
		this.ganzTags = ganzTags;
	}

	public long getId()
	{
		return id;
	}
	
	public Bewohner[] getTeilnehmer() {
		return teilnehmer;
	}

	
	public void setTeilnehmer(Bewohner[] teilnehmer) {
		this.teilnehmer = teilnehmer;
	}

	
	public Calendar getBis() {
		return bis;
	}

	
	public void setBis(Calendar bis) {
		this.bis = bis;
	}

	
	public Calendar getVon() {
		return von;
	}

	
	public void setVon(Calendar von) {
		this.von = von;
	}

	
	public String getName() {
		return name;
	}

	
	public void setName(String name) {
		this.name = name;
	}


	public void setId(long id) {
		this.id = id;
		
	}
	public Bewohner getErsteller() {
		return ersteller;
	}
	public boolean isGanzTags() {
		return ganzTags;
	}
}
