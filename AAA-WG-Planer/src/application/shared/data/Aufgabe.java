package application.shared.data;

import java.io.Serializable;

/**
 * Die Klasse Aufgabe wird zur automatischen Zuteilung der Pflichten in
 * der WG sowie zum Nachschauen der genauen Anforderung.
 * @author Christoph Scherer
 *
 */
public class Aufgabe implements Serializable {

	long id;
	
	/**
	 * (Beschreibender) Name der Aufgabe.
	 */
	private String name;
	
	/**
	 * Spezifiziert die Aufgabe fuer die WG-Bewohner.
	 */
	private String beschreibung;
	
	/**
	 * Gibt fuer mehrfach auszufuehrende, sehr aehnliche Aufgaben an, 
	 * wie oft die Aufgabe ausgefuehrt wird.
	 */
	private int anzahl = 1;
	
	public long getId()
	{
		return id;
	}
	
	private boolean[] tage;
	
	public Aufgabe(String name, boolean montag, boolean dienstag, boolean mittwoch, boolean donnerstag, boolean freitag, boolean samstag,
			boolean sonntag) {
		this.name = name;
		tage = new boolean[]{montag, dienstag, mittwoch, donnerstag, freitag, samstag, sonntag};
	}

	
	public Aufgabe(String name, boolean[] tage) {
		this.name = name;
		this.tage = tage;
	}

	public Aufgabe(String name, boolean mo, boolean di, boolean mi,
			boolean don, boolean fr, boolean sa, boolean so, int anzahl) {
		this(name, mo, di, mi, don, fr, sa, so);
		this.anzahl = anzahl;
	}


	public Aufgabe(String name, boolean[] tage, int anzahl) {
		this(name, tage);
		this.anzahl = anzahl;
	}


	public String getName(){
		return this.name;
	}
	
	public String getBeschreibung(){
		return this.beschreibung;
	}
	
	public int getAnzahl(){
		return this.anzahl;
	}
	
	//Sinnvoll, das hier ohne nen Check public zu machen?
	public void setName(String name) {
		this.name = name;
	}

	
	public boolean[] getTage() {
		return tage;
	}

	public String getTageAsString(){
		String[] t = {"Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"};
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < tage.length; i++){
			if(tage[i]){
				sb.append(t[i] + ", ");
			}
		}
		sb.delete(sb.length() - 2, sb.length());
		return sb.toString();
	}
	
	public void setTage(boolean[] tage) {
		this.tage = tage;
	}
	
	public static String getTageAsString(boolean[] tage){
		String[] t = {"Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"};
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < tage.length; i++){
			if(tage[i]){
				sb.append(t[i] + ", ");
			}
		}
		sb.delete(sb.length() - 2, sb.length());
		return sb.toString();
	}


	public void setId(int id) {
		this.id = id;
	}
}
