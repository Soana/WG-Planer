package application.shared.data;

import application.client.logik.Bewohner;


public class Eintrag {

	private long id;
	private String was;
	private int wieviel;
	private int preis = -1;
	private Bewohner[] fuer;
	
	public Eintrag(String was, int wieviel, Bewohner[] fuer) {
		this.was = was;
		this.wieviel = wieviel;
		this.fuer = fuer;
	}

	public long getId() {
		return id;
	}
	
	public String getWas() {
		return was;
	}
	
	public void setWas(String was) {
		this.was = was;
	}

	
	public int getWieviel() {
		return wieviel;
	}

	
	public void setWieviel(int wieviel) {
		this.wieviel = wieviel;
	}

	
	public Bewohner[] getFuer() {
		return fuer;
	}

	public String getFuerAsString(){
		StringBuilder sb = new StringBuilder();
		for(int j = 0; j < fuer.length; j++){
			sb.append(fuer[j].getKurz() + ", ");
		}
		sb.delete(sb.length() - 2, sb.length());
		return sb.toString();
	}
	
	public void setFuer(Bewohner[] fuer) {
		this.fuer = fuer;
	}


	public void setId(long id) {
		this.id = id;
	}

	public void setPreis(int cent) {
		preis = cent;
	}

	public int getPreis(){
		return preis;
	}
}
