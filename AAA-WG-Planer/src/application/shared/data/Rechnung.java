package application.shared.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;

import application.client.logik.Bewohner;


public class Rechnung {
	Bewohner von;
	Bewohner[] fuer;
	String was;
	int wieviel;
	GregorianCalendar wann;
	
	public Rechnung(Bewohner von, Bewohner[] fuer, String was, int wieviel){
		this.von = von;
		this.fuer = fuer;
		this.was = was;
		this.wieviel = wieviel;
		wann = (GregorianCalendar) Calendar.getInstance();
	}
	
	public Bewohner getVon() {
		return von;
	}

	
	public Bewohner[] getFuer() {
		return fuer;
	}

	
	public String getWas() {
		return was;
	}

	
	public int getWieviel() {
		return wieviel;
	}


	public GregorianCalendar getWann() {
		return wann;
	}	
}
