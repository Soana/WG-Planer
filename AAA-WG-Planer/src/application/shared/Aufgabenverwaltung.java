package application.shared;

import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import org.json.JSONArray;
import org.json.JSONObject;

import application.client.logik.LogicController;
import application.shared.data.Aufgabe;
import application.shared.network.RequestHandler;
import application.shared.network.ResponseHandler;


public class Aufgabenverwaltung {

	
	private ArrayList<Aufgabe> aufgaben;
	private ReentrantLock mutex;
	
	public Aufgabenverwaltung() {
		aufgaben = new ArrayList<>();
		mutex = new ReentrantLock();
		
		LogicController.getConnection().registerRequestHandler("aufgaben", new RequestHandler() {

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				mutex.lock();
				aufgaben = new ArrayList<>();
				JSONArray arr = jObj.getJSONArray("aufgaben");
				for(int i = 0; i < arr.length(); i++) {
					JSONObject aufgabeJ = arr.getJSONObject(i);
					aufgaben.add(new Aufgabe(
							aufgabeJ.getString("name"),
							aufgabeJ.getBoolean("mo"),
							aufgabeJ.getBoolean("di"),
							aufgabeJ.getBoolean("mi"),
							aufgabeJ.getBoolean("do"),
							aufgabeJ.getBoolean("fr"),
							aufgabeJ.getBoolean("sa"),
							aufgabeJ.getBoolean("so"),
							aufgabeJ.getInt("anzahl")
					));
				}
				mutex.unlock();
				return new JSONObject();
			}
			
		});
	}

	public Aufgabe[] getAufgaben() {
		mutex.lock();
		Aufgabe[] as = new Aufgabe[aufgaben.size()];
		aufgaben.toArray(as);
		mutex.unlock();
		return as;
	}

	public void loeschen(final Aufgabe aufgabe) {
		JSONObject jObj = new JSONObject();
		jObj.put("id", aufgabe.getId());
		LogicController.getConnection().sendRequest("loesche_aufgabe", jObj, new ResponseHandler() {
			@Override
			public void onResponse(JSONObject jObj) {
				mutex.lock();
				aufgaben.remove(aufgabe);
				mutex.unlock();
			}
			
		});
	}

	public void add(final String name, final boolean[] tage, final int anzahl) {
		JSONObject jObj = new JSONObject();
		jObj.put("name", name);
		jObj.put("mo", tage[0]);
		jObj.put("di", tage[1]);
		jObj.put("mi", tage[2]);
		jObj.put("do", tage[3]);
		jObj.put("fr", tage[4]);
		jObj.put("sa", tage[5]);
		jObj.put("so", tage[6]);
		jObj.put("anzahl", anzahl);
		LogicController.getConnection().sendRequest("add_aufgabe", jObj, new ResponseHandler() {
			@Override
			public void onResponse(JSONObject jObj) {
				mutex.lock();
				Aufgabe a = new Aufgabe(name, tage, anzahl);
				a.setId(jObj.getInt("id"));
				aufgaben.add(a);
				mutex.unlock();
			}
			
		});
	}

}
