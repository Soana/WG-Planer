package application.shared.network;

import java.io.*;
import java.net.*;
import java.util.*;

import org.json.JSONObject;

public class Connection extends Thread
{
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	public Map<String, RequestHandler> requests;
	public Map<Integer, ResponseHandler> responses;
	
	private int messageID;

	public Connection(Socket socket)
	{
		messageID = 0;
		requests = new TreeMap<>();
		responses = new HashMap<>();
		try
		{
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void registerRequestHandler(String id, RequestHandler handler)
	{
		requests.put(id, handler);
	}
	
	@Override
	public void run()
	{
		while(!isInterrupted())
		{
			try
			{
				String string = in.readLine();
				System.out.println("Received: " + string);
				JSONObject jObj = new JSONObject(string);
				if(jObj.has("isRequest") && jObj.getBoolean("isRequest"))
				{
					String requestName = jObj.getString("requestName");
					if(requests.containsKey(requestName))
					{
						int uniqueID = jObj.getInt("uniqueID");
						RequestHandler handler = requests.get(requestName);
						JSONObject response = handler.onRequest(jObj);
						response.put("uniqueID", uniqueID);
						response.put("isResponse", true);
						sendResponse(response);
					}
					else
					{
						throw new RuntimeException("Received invalid request with name \"" + requestName + "\"");
					}
				}
				if(jObj.has("isResponse") && jObj.getBoolean("isResponse"))
				{
					int uniqueID = jObj.getInt("uniqueID");
					if(responses.containsKey(uniqueID))
					{
						ResponseHandler handler = responses.get(uniqueID);
						if(handler != null)
						{
							handler.onResponse(jObj);
						}
						responses.remove(uniqueID);
					}
					else
					{
						throw new RuntimeException("Received invalid response with ID " + uniqueID);
					}
				}
			}
			catch(IOException e)
			{
				break;
			}
		}
	}
	
	public void shutdown()
	{
		this.interrupt();
	}
	
	public void sendRequest(String requestName, JSONObject params)
	{
		sendRequest(requestName, params, null);
	}
	
	public void sendRequest(String requestName, JSONObject params, ResponseHandler handler)
	{
		int ourID =  messageID++;
		params.put("isRequest", true);
		params.put("uniqueID", ourID);
		params.put("requestName", requestName);
		responses.put(ourID, handler);
		send(params.toString());
	}
	
	public void sendRequestAndWait(String requestName, JSONObject params)
	{
		sendRequestAndWait(requestName, params, null);
	}
	
	public void sendRequestAndWait(String requestName, JSONObject params, ResponseHandler handler)
	{
		int id = messageID;
		sendRequest(requestName, params, handler);
		while(responses.containsKey(id)){
			try {
				sleep(10);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void sendResponse(JSONObject jObj)
	{
		send(jObj.toString());
	}
	
	private void send(String s)
	{
		out.println(s);
		out.flush();
	}
}
