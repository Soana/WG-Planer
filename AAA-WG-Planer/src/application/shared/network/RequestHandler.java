package application.shared.network;

import org.json.JSONObject;

public interface RequestHandler
{
	public JSONObject onRequest(JSONObject jObj);
}
