package application.shared.network.protokoll;

import java.util.HashMap;

import org.json.JSONObject;

import application.client.logik.Bewohner;
import application.shared.data.*;


public class ProtokollEintrag {
	
	/**
	 * loesche_eintrag
	 * 	Client:
	 * 		id int
	 * 
	 * @author ruebsteck
	 */
	public static class DeleteEintrag{
		//request
		public static final String ID = "id";
		
		public static String getId() {
			return "eintrag_delete";
		}

		public static JSONObject request(Eintrag eintrag){
			JSONObject jobj = new JSONObject();
			jobj.put(ID, (int) eintrag.getId());
			
			return jobj;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
		
	}
	
	/**
	 * loesche_eintraege
	 * 	Client:
	 * 		ids [] int
	 * 	
	 * @author ruebsteck
	 */
	public static class DeleteEintragMutiple{
		//request
		public static final String ID = "id";
		
		public static String getId() {
			return "eintrag_deletemultiple";
		}
	
		public static JSONObject request(Eintrag[] eintraege){
			JSONObject jobj = new JSONObject();
			for(Eintrag e: eintraege){
				jobj.append(ID, (int) e.getId());
			}
			
			return jobj;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
		
	}

	/**
	 * add_eintrag
	 * 	Client:
	 * 		was String
	 * 		anzahl int
	 * 		bewohner [] int
	 * 	Server:
	 * 		id int
	 * 
	 * @author ruebsteck
	 */
	public static class AddEintrag{
		//request
		public static final String WAS = "was";
		public static final String ANZAHL = "anzahl";
		public static final String WER = "bewohner";
		//response
		public static final String ID = "id";
		
		public static String getId() {
			return "eintrag_add";
		}

		public static JSONObject request(String was, int anzahl, Bewohner[] wer){
			JSONObject jobj = new JSONObject();
			jobj.put(WAS, was);
			jobj.put(ANZAHL, anzahl);
			for(Bewohner b: wer){
				jobj.append(WER, (int) b.getId());
			}
			
			return jobj;
		}
		
		public static JSONObject response(int id) {
			JSONObject jobj = new JSONObject();
			jobj.put(ID, id);
			
			return jobj;
		}
	}

	/**
	 * stelle_rechnung
	 * 	Client:
	 * 		kommentar String
	 * 		preis int
	 * 		von int
	 * 		wer [] {}
	 * 			id int
	 * 			gaeste int
	 *	Server:
	 *		anteile [] {}
	 *			id int
	 *			preis int
	 * 		
	 * @author ruebsteck
	 */
	public static class AddRechnung{
		//request
		public static final String KOMMENTAR = "kommentar";
		public static final String GAESTE = "gaeste";
		public static final String VON = "von";
		public static final String FUER = "wer";
		//request & response
		public static final String ID = "id";
		public static final String PREIS = "preis";
		//response
		public static final String ANTEILE = "anteile";
		
		public static String getId() {
			return "rechnung_add";
		}

		public static JSONObject request(String kommentar, int preis, Bewohner von, HashMap<Bewohner, Integer> fuer){
			JSONObject jobj = new JSONObject();
			jobj.put(KOMMENTAR, kommentar);
			jobj.put(PREIS, preis);
			jobj.put(VON, (int) von.getId());
			for(Bewohner b: fuer.keySet()){
				JSONObject jo = new JSONObject();
				jo.put(ID, (int) b.getId());
				jo.put(GAESTE, fuer.get(b));
				
				jobj.append(FUER, jo);
			}
			
			return jobj;
		}
		
		public static JSONObject response(HashMap<Integer, Integer> anteile) {
			JSONObject jObj = new JSONObject();
			
			for(int id: anteile.keySet()){
				JSONObject jo = new JSONObject();
				jo.put(ID, id);
				jo.put(PREIS, anteile.get(id));
				
				jObj.append(ANTEILE, jo);
			}
			
			return jObj;
		}
	}

	/**
	 * eintraege
	 * 	Server:
	 * 		eintraege [] {}
	 * 			bewohner [] int
	 * 			name String
	 * 			anzahl int
	 * 			id int
	 * 
	 * @author ruebsteck
	 */
	public static class GetAllEintraege{
		//request
		public static final String EINTRAEGE = "eintraege";
		public static final String FUER = "bewohner";
		public static final String NAME = "name";
		public static final String ANZAHL = "anzahl";
		public static final String ID = "id";
		
		public static String getId() {
			return "eintraege";
		}

		public static JSONObject request(Eintrag[] eintraege){
			JSONObject jobj = new JSONObject();
			for(Eintrag e: eintraege){
				JSONObject jo = new JSONObject();
				for(Bewohner b: e.getFuer()){
					jo.append(FUER, (int) b.getId());
				}
				jo.put(NAME, e.getWas());
				jo.put(ANZAHL, e.getWieviel());
				jo.put(ID, (int) e.getId());
				
				jobj.append(EINTRAEGE, jo);
			}
			
			return jobj;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
	}
	
	/**
	 * rechnungen
	 * 	Server:
	 * 		rechnungen [] {}
	 * 			bewohner [] int
	 * 			kommentar String
	 * 			von int
	 * 			geld int
	 * 			wann int
	 * 			id int
	 * 
	 * @author ruebsteck
	 */
	public static class GetAllRechnungen{
		//request
		public static final String RECHNUNGEN = "rechnungen";
		public static final String KOMMENTAR = "kommentar";
		public static final String VON = "von";
		public static final String PREIS = "preis";
		public static final String FUER = "bewohner";
		public static final String WANN = "wann";
		
		public static String getId() {
			return "rechnungen";
		}

		public static JSONObject request(Rechnung[] rechnungen){
			JSONObject jobj = new JSONObject();
			for(Rechnung r: rechnungen){
				JSONObject jo = new JSONObject();
				jo.put(KOMMENTAR, r.getWas());
				jo.put(VON, (int) r.getVon().getId());
				jo.put(PREIS, r.getWieviel());
				jo.put(WANN, r.getWann().getTime().getTime());
				for(Bewohner b: r.getFuer()){
					jo.append(FUER, (int) b.getId());
				}

				jobj.append(RECHNUNGEN, jo);
			}
			
			return jobj;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
	}
}
