package application.shared.network.protokoll;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.*;

import application.client.logik.Bewohner;


public class ProtokollAllgemein {
	public static class Login{
		//request
		public static final String WG = "wg";
		public static final String USER = "username";
		public static final String PASSWORD = "password";
		//response
		public static final String ERFOLG = "gueltig";
		public static final String ID = "id";
		public static final String VORNAME = "vorname";
		public static final String NACHNAME = "nachname";
		public static final String TELEFON = "telefon";
		public static final String GEBURTSDATUM = "geburtsdatum";
		public static final String ADMIN = "is_admin";
		
		public static String getId(){
			return "login";
		}
		
		public static JSONObject request(String wg, String username, char[] password){
			JSONObject jObj = new JSONObject();
			
			jObj.put(WG, wg);
			jObj.put(USER, username);
			
			StringBuilder sb = new StringBuilder();
			try {
				StringBuffer sb1 = new StringBuffer();
				for(char c: password){
					sb1.append(c);
				}
				MessageDigest md = MessageDigest.getInstance("SHA1");
				byte[] by = md.digest(sb.toString().getBytes());
				for(byte b: by){
					sb.append(Integer.toHexString(b & 0xff));
				}
			}
			catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			System.out.println("Passwort: " + sb);
			jObj.put(PASSWORD, sb.toString());
			return jObj;
		}
		
		public static JSONObject response(int id, String vorname, String nachname, String telefon, long geburtsdatum, boolean isAdmin){
			JSONObject jObj = new JSONObject();
			
			jObj.put(ERFOLG, true);
			jObj.put(ID, id);
			jObj.put(VORNAME, vorname);
			jObj.put(NACHNAME, nachname);
			jObj.put(TELEFON, telefon);
			jObj.put(GEBURTSDATUM, geburtsdatum);
			jObj.put(ADMIN, isAdmin);
			
			return jObj;
		}
		
		public static JSONObject response(Bewohner bewohner){
			JSONObject jObj = new JSONObject();
			
			jObj.put(ERFOLG, true);
			jObj.put(ID, bewohner.getId());
			jObj.put(VORNAME, bewohner.getVorname());
			jObj.put(NACHNAME, bewohner.getNachname());
			jObj.put(TELEFON, bewohner.getTelefon());
			jObj.put(GEBURTSDATUM, bewohner.getGeburt().getTime());
			jObj.put(ADMIN, bewohner.isAdmin());
			
			return jObj;
		}
		
		public static JSONObject response(){
			JSONObject jObj = new JSONObject();
			
			jObj.put(ERFOLG, false);
			
			return jObj;
		}
	}
	
	public static class Init{
		
		public static String getId(){
			return "init";
		}
		
		public static JSONObject request(){
			return new JSONObject();
		}
		
		public static JSONObject response(){
			return new JSONObject();
		}
	}
	
	public static class LogOut{
		
		public static String getId(){
			return "logout";
		}
		
		public static JSONObject request(){
			return new JSONObject();
		}
		
		public static JSONObject response(){
			return new JSONObject();
		}
	}

	public static class CreateWG{
		//request
		public static final String NAME = "name";
		//response
		public static final String ERFOLG = "erfolg";
	
		public static String getId(){
			return "wg_create";
		}
		
		public static JSONObject request(String name){
			JSONObject jObj = new JSONObject();
			
			jObj.put(NAME, name);
			
			return jObj;
		}
		
		public static JSONObject response(boolean verfuegbar){
			JSONObject jObj = new JSONObject();
			
			jObj.put(ERFOLG, verfuegbar);
			
			return jObj;
		}
	}
}
