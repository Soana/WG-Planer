package application.shared.network.protokoll;

import org.json.JSONObject;

import application.client.logik.Bewohner;
import application.shared.data.Aufgabe;

public class ProtokollAufgabe{
	/**
	 * loesche_aufgabe
	 *	Client:
	 *		id int
	 *
	 * @author ruebsteck
	 */
	public static class Delete{
		//request
		public static final String ID = "id";
		
		public static String getId() {
			return "aufgabe_delete";
		}

		public static JSONObject request(Aufgabe aufgabe){
			JSONObject jobj = new JSONObject();
			jobj.put(ID, (int) aufgabe.getId());
			
			return jobj;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
		
	}
	
	/**
	 * add_aufgabe
	 *	Client:
	 * 		name String
	 * 		mo boolean
	 * 		di boolean
	 * 		mi boolean
	 * 		do boolean
	 * 		fr boolean
	 * 		sa boolean
	 * 		so boolean
	 * 		anzahl int
	 * 	Server:
	 * 		id int
	 *
	 * @author ruebsteck
	 */
	public static class Add{
		//request
		public static final String NAME = "name";
		public static final String MO = "mo";
		public static final String DI = "di";
		public static final String MI = "mi";
		public static final String DO = "do";
		public static final String FR = "fr";
		public static final String SA = "sa";
		public static final String SO = "so";
		public static final String ANZAHL = "anzahl";
		//response
		public static final String ID = "id";
		
		public static String getId() {
			return "aufgabe_add";
		}

		public static JSONObject request(String name, boolean mon, boolean din, boolean mit, boolean don, boolean fre, boolean sam, boolean son, int anzahl){
			JSONObject jobj = new JSONObject();
			jobj.put(NAME, name);
			jobj.put(MO, mon);
			jobj.put(DI, din);
			jobj.put(MI, mit);
			jobj.put(DO, don);
			jobj.put(FR, fre);
			jobj.put(SA, sam);
			jobj.put(SO, son);
			jobj.put(ANZAHL, anzahl);
			
			return jobj;
		}
		
		public static JSONObject response(int id) {
			JSONObject jobj = new JSONObject();
			jobj.put(ID, id);
			
			return jobj;
		}
	}
	
	/**
	 * aufgaben
	 *	Server:
	 * 	aufgaben [] {}
	 * 		name String
	 * 		mo boolean
	 * 		di boolean
	 * 		mi boolean
	 * 		do boolean
	 * 		fr boolean
	 * 		sa boolean
	 * 		so boolean
	 * 		anzahl int
	 * 		id int
	 * 
	 * @author ruebsteck
	 */
	public static class GetAll{
		//request
		public static final String AUFGABEN = "aufgaben";
		public static final String NAME = "name";
		public static final String MO = "mo";
		public static final String DI = "di";
		public static final String MI = "mi";
		public static final String DO = "do";
		public static final String FR = "fr";
		public static final String SA = "sa";
		public static final String SO = "so";
		public static final String ANZAHL = "anzahl";
		public static final String ID = "id";
		
		public static String getId() {
			return "aufgaben";
		}

		public static JSONObject request(Aufgabe[] aufgaben){
			JSONObject jobj = new JSONObject();
			for(Aufgabe a: aufgaben){
				JSONObject jo = new JSONObject();
				
				boolean[] tage = a.getTage();
				
				jo.put(NAME, a.getName());
				jo.put(MO, tage[0]);
				jo.put(DI, tage[1]);
				jo.put(MI, tage[2]);
				jo.put(DO, tage[3]);
				jo.put(FR, tage[4]);
				jo.put(SA, tage[5]);
				jo.put(SO, tage[6]);
				jo.put(ANZAHL, a.getAnzahl());
				jo.put(ID, a.getId());
				
				jobj.append(AUFGABEN, jo);
			}
			
			return jobj;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
	}
}

