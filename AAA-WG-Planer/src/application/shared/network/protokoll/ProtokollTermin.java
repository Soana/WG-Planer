package application.shared.network.protokoll;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.json.JSONObject;

import application.client.logik.Bewohner;
import application.shared.data.Aufgabe;
import application.shared.data.Termin;

public class ProtokollTermin{
	/**
	 * loesche_termin
	 *	Client:
	 *		id int
	 *
	 * @author ruebsteck
	 */
	public static class Delete{
		//request
		public static final String ID = "id";
		
		public static String getId() {
			return "termin_delete";
		}

		public static JSONObject request(Termin termin){
			JSONObject jobj = new JSONObject();
			jobj.put(ID, (int) termin.getId());
			
			return jobj;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
		
	}
	
	/**
	 * add_termin
	 * 	Client:
	 * 		name String
	 * 		von {}
	 * 			tag int
	 * 			monat int
	 * 			jahr int
	 * 			stunde int
	 * 			minute int
	 * 		bis {}
	 * 			tag int
	 * 			monat int
	 * 			jahr int
	 * 			stunde int
	 * 			minute int
	 * 		bewohner [] int
	 * 		sichtbar [] int
	 * 		ganz_tags boolean
	 * 	Server:
	 * 		id int
	 *
	 * @author ruebsteck
	 */
	public static class Add{
		//request
		public static final String NAME = "name";
		public static final String VON = "von";
		public static final String BIS = "bis";
		public static final String TAG = "tag";
		public static final String MONAT = "monat";
		public static final String JAHR = "jahr";
		public static final String STUNDE = "stunde";
		public static final String MINUTE = "minute";
		public static final String WER = "bewohner";
		public static final String SICHTBAR = "sichtbar";
		public static final String GANZ_TAGS = "ganz_tags";
		//response
		public static final String ID = "id";
		
		public static String getId() {
			return "termin_add";
		}

		public static JSONObject request(String name, GregorianCalendar von, GregorianCalendar bis, boolean ganzTags, Bewohner[] teilnehmer, Bewohner[] sichtbar){
			JSONObject jobj = new JSONObject();
			jobj.put(NAME, name);
			
			JSONObject v = new JSONObject();
			v.put(TAG, von.get(Calendar.DAY_OF_MONTH));
			v.put(MONAT, von.get(Calendar.MONTH));
			v.put(JAHR, von.get(Calendar.YEAR));
			v.put(STUNDE, von.get(Calendar.HOUR_OF_DAY));
			v.put(MINUTE, von.get(Calendar.MINUTE));
			JSONObject b = new JSONObject();
			b.put(TAG, bis.get(Calendar.DAY_OF_MONTH));
			b.put(MONAT, bis.get(Calendar.MONTH));
			b.put(JAHR, bis.get(Calendar.YEAR));
			b.put(STUNDE, bis.get(Calendar.HOUR_OF_DAY));
			b.put(MINUTE, bis.get(Calendar.MINUTE));
			
			jobj.put(VON, v);
			jobj.put(BIS, b);
			jobj.put(GANZ_TAGS, ganzTags);
			for(Bewohner be: teilnehmer){
				jobj.append(WER, be.getId());
			}
			for(Bewohner be: sichtbar){
				jobj.append(SICHTBAR, be.getId());
			}
			
			return jobj;
		}
		
		public static JSONObject response(int id) {
			JSONObject jobj = new JSONObject();
			jobj.put(ID, id);
			
			return jobj;
		}
	}
	
	/**
	 * termine
	 * 	Server:
	 * 		termine [] {}
	 * 			ersteller int
	 * 			name String
	 * 			von {}
	 * 				tag int
	 * 				monat int
	 * 				jahr int
	 * 				stunde int
	 * 				minute int
	 * 			bis {}
	 * 				tag int
	 * 				monat int
	 * 				jahr int
	 * 				stunde int
	 * 				minute int
	 * 			bewohner [] int
	 * 			ganz_tags boolean
	 * 			id int
	 * 
	 * @author ruebsteck
	 */
	public static class GetAll{
		//request
		public static final String TERMINE = "termine";
		public static final String NAME = "name";
		public static final String ERSTELLER = "ersteller";
		public static final String VON = "von";
		public static final String BIS = "bis";
		public static final String TAG = "tag";
		public static final String MONAT = "monat";
		public static final String JAHR = "jahr";
		public static final String STUNDE = "stunde";
		public static final String MINUTE = "minute";
		public static final String WER = "bewohner";
		public static final String GANZ_TAGS = "ganz_tags";
		public static final String ID = "id";
		
		public static String getId() {
			return "termine";
		}

		public static JSONObject request(Termin[] termine){
			JSONObject jo = new JSONObject();
			
			for(Termin t: termine){
				JSONObject jobj = new JSONObject();
				jobj.put(NAME, t.getName());
				jobj.put(ERSTELLER, t.getErsteller());
				
				GregorianCalendar von = t.getVon();
				JSONObject v = new JSONObject();
				v.put(TAG, von.get(Calendar.DAY_OF_MONTH));
				v.put(MONAT, von.get(Calendar.MONTH));
				v.put(JAHR, von.get(Calendar.YEAR));
				v.put(STUNDE, von.get(Calendar.HOUR_OF_DAY));
				v.put(MINUTE, von.get(Calendar.MINUTE));
				GregorianCalendar bis = t.getBis();
				JSONObject b = new JSONObject();
				b.put(TAG, bis.get(Calendar.DAY_OF_MONTH));
				b.put(MONAT, bis.get(Calendar.MONTH));
				b.put(JAHR, bis.get(Calendar.YEAR));
				b.put(STUNDE, bis.get(Calendar.HOUR_OF_DAY));
				b.put(MINUTE, bis.get(Calendar.MINUTE));
				
				jobj.put(VON, v);
				jobj.put(BIS, b);
				jobj.put(GANZ_TAGS, t.isGanzTags());
				Bewohner[] teilnehmer = t.getTeilnehmer();
				for(Bewohner be: teilnehmer){
					jobj.append(WER, be.getId());
				}
				
				jobj.put(ID, t.getId());
				
				jo.append(TERMINE, jobj);
			}
			
			return jo;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
	}
}

