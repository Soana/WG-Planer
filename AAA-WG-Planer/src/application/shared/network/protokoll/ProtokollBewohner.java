package application.shared.network.protokoll;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.json.JSONObject;

import application.client.logik.Bewohner;

public class ProtokollBewohner{
	/**
	 * loesche_bewohner
	 *	Client:
	 *		id int
	 *
	 * @author ruebsteck
	 */
	public static class Delete{
		//request
		public static final String ID = "id";
		
		public static String getId() {
			return "bewohner_delete";
		}

		public static JSONObject request(Bewohner bewohner){
			JSONObject jobj = new JSONObject();
			jobj.put(ID, (int) bewohner.getId());
			
			return jobj;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
		
	}
	
	/**
	 * add_bewohner
	 *	Client:
	 *	 	vorname String
	 *		nachname String
	 *	Server:
	 *		vorname String
	 *		nachname String
	 *		id int
	 *		benutzername String
	 *
	 * @author ruebsteck
	 */
	public static class Add{
		//request
		public static final String VORNAME = "vorname";
		public static final String NACHNAME = "nachname";
		//response
		public static final String ID = "id";
		public static final String BENUTZERNAME = "benutzername";
		
		public static String getId() {
			return "bewohner_add";
		}

		public static JSONObject request(String vorname, String nachname){
			JSONObject jobj = new JSONObject();
			jobj.put(VORNAME, vorname);
			jobj.put(NACHNAME, nachname);
			
			return jobj;
		}
		
		public static JSONObject response(int id, String benutzername) {
			JSONObject jobj = new JSONObject();
			jobj.put(ID, id);
			jobj.put(BENUTZERNAME, benutzername);
			
			return jobj;
		}
	}
	
	/**
	 * update_bewohner
	 *	Client:
	 *		id int
	 *	 	vorname String
	 *		nachname String
	 *		telefon String
	 *		geburt	{}
	 *			tag int
	 *			monat int
	 *			jahr int
	 *	Server:
	 *		benutzername String
	 *
	 * @author ruebsteck
	 */
	public static class Update{
		//request
		public static final String VORNAME = "vorname";
		public static final String NACHNAME = "nachname";
		public static final String TELEFON = "telefon";
		public static final String GEBURT = "geburt";
		public static final String TAG = "tag";
		public static final String MONAT = "monat";
		public static final String JAHR = "jahr";
		public static final String ID = "id";
		//response
		public static final String BENUTZERNAME = "benutzername";
		
		public static String getId() {
			return "bewohner_update";
		}

		public static JSONObject request(int id, String vorname, String nachname, GregorianCalendar geburtsdatum, String telefon){
			JSONObject jobj = new JSONObject();
			
			jobj.put(VORNAME, vorname);
			jobj.put(NACHNAME, nachname);
			jobj.put(TELEFON, telefon);
			jobj.put(GEBURT, geburtsdatum.getTime().getTime());
			
			jobj.put(ID, id);
			
			return jobj;
		}
		
		public static JSONObject response(String benutzername) {
			JSONObject jobj = new JSONObject();
			
			jobj.put(BENUTZERNAME, benutzername);
			
			return jobj;
		}
	}
	
	/**
	 * mitbewohner
	 *	Server:
	 *		bewohner [] {}
	 *			vorname String
	 *			nachname String
	 *			geburtsdatum int
	 *			telefon String
	 *			admin boolean
	 *			benutzername String
	 *			id int
	 * @author ruebsteckS
	 */
	public static class GetAll{
		//request
		public static final String BEWOHNER = "bewohner";
		public static final String VORNAME = "vorname";
		public static final String NACHNAME = "nachname";
		public static final String GEBURTSDATUM = "geburtsdatum";
		public static final String TELEFON = "telefon";
		public static final String ADMIN = "admin";
		public static final String ID = "id";
		public static final String BENUTZERNAME = "benutzername";
		
		public static String getId() {
			return "mitbewohner";
		}

		public static JSONObject request(Bewohner[] bewohner){
			JSONObject jobj = new JSONObject();
			for(Bewohner b: bewohner){
				JSONObject jo = new JSONObject();
				jo.put(VORNAME, b.getVorname());
				jo.put(NACHNAME, b.getNachname());
				jo.put(GEBURTSDATUM, b.getGeburt().getTime());
				jo.put(TELEFON, b.getTelefon());
				jo.put(ADMIN, b.isAdmin());
				jo.put(ID, b.getId());
				jo.put(BENUTZERNAME, b.getBenutzername());
				
				jobj.append(BEWOHNER, jo);
			}
			
			return jobj;
		}
		
		public static JSONObject response() {
			return new JSONObject();
		}
	}
}

