package application.shared.network;

import org.json.*;

public interface ResponseHandler
{
	public void onResponse(JSONObject jObj);
}
