package application.client.logik;

import application.shared.Aufgabenverwaltung;
import application.shared.network.protokoll.ProtokollAllgemein;

public class DataObject{

	private static DataObject dobj;
	
	public Einkaufsliste einkaufsliste;
	public Kalender kalender;
	public Mitbewohnerverwaltung mitbewohnerverwaltung;
	public Aufgabenverwaltung aufgabenverwaltung;
	
	private DataObject(Bewohner bewohner){
		mitbewohnerverwaltung = new Mitbewohnerverwaltung(bewohner);
		einkaufsliste = new Einkaufsliste(this);
		kalender = new Kalender(this);
		aufgabenverwaltung = new Aufgabenverwaltung();
		
		LogicController.getConnection().sendRequest(ProtokollAllgemein.Init.getId(), ProtokollAllgemein.Init.request());
	}
	
	public static DataObject getInstance(Bewohner current){
		if(dobj == null){
			dobj = new DataObject(current);
		}
		return dobj;
	}

}
