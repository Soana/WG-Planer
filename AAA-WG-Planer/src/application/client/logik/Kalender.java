package application.client.logik;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

import org.json.JSONArray;
import org.json.JSONObject;

import application.shared.data.Termin;
import application.shared.network.RequestHandler;
import application.shared.network.ResponseHandler;
import application.shared.network.protokoll.ProtokollTermin;


public class Kalender {

	private ReentrantLock mutex;
	private Map<String, List<Termin>> termine;
	private DataObject dobj;
	
	public Kalender(final DataObject dobj) {
		this.dobj = dobj;
		termine = new TreeMap<>();
		mutex = new ReentrantLock();
		
		LogicController.getConnection().registerRequestHandler(ProtokollTermin.GetAll.ID, new RequestHandler() {
			@Override
			public JSONObject onRequest(JSONObject jObj) {
				mutex.lock();
				termine = new TreeMap<>();
				JSONArray arr = jObj.getJSONArray(ProtokollTermin.GetAll.TERMINE);
				for(int i = 0; i < arr.length(); i++) {
					JSONObject terminJ = arr.getJSONObject(i);
					JSONObject von = terminJ.getJSONObject(ProtokollTermin.GetAll.VON);
					JSONObject bis = terminJ.getJSONObject(ProtokollTermin.GetAll.BIS);
					JSONArray jArr = terminJ.getJSONArray(ProtokollTermin.GetAll.WER);
					Bewohner[] bs = new Bewohner[jArr.length()];
					for(int j = 0; j < jArr.length(); j++) {
						bs[j] = dobj.mitbewohnerverwaltung.getBewohnerByID(jArr.getInt(j));
					}
					Termin t = new Termin(
							dobj.mitbewohnerverwaltung.getBewohnerByID(terminJ.getInt(ProtokollTermin.GetAll.ERSTELLER)),
							terminJ.getString(ProtokollTermin.GetAll.NAME),
							new GregorianCalendar(von.getInt(ProtokollTermin.GetAll.JAHR), von.getInt(ProtokollTermin.GetAll.MONAT), von.getInt(ProtokollTermin.GetAll.TAG), von.getInt(ProtokollTermin.GetAll.STUNDE), von.getInt(ProtokollTermin.GetAll.MINUTE)),
							new GregorianCalendar(bis.getInt(ProtokollTermin.GetAll.JAHR), bis.getInt(ProtokollTermin.GetAll.MONAT), bis.getInt(ProtokollTermin.GetAll.TAG), bis.getInt(ProtokollTermin.GetAll.STUNDE), bis.getInt(ProtokollTermin.GetAll.MINUTE)),
							bs,
							terminJ.getBoolean(ProtokollTermin.GetAll.GANZ_TAGS),
							terminJ.getInt(ProtokollTermin.GetAll.ID)
					);
					insertTermin(t);
				}
				mutex.unlock();
				return ProtokollTermin.GetAll.response();
			}
			
		});
	}
	
	private void insertTermin(Termin t)
	{
		String key = t.getVon().get(Calendar.MONTH) + "." + t.getVon().get(Calendar.YEAR);
		if(termine.containsKey(key))
		{
			termine.get(key).add(t);
		}
		else
		{
			List<Termin> l = new ArrayList<>();
			l.add(t);
			termine.put(key, l);
		}
	}

	public Termin[] getTermine(int tag, int monat, int jahr) {
		mutex.lock();
		List<Termin> ts = termine.get(monat + "." + jahr);
		List<Termin> t = new ArrayList<Termin>();
		if(ts != null){
			Termin[] tb = new Termin[t.size()];
			for(int i = 0; i < ts.size(); i++){
				if(ts.get(i).getVon().get(Calendar.DAY_OF_MONTH) == tag){
					t.add(ts.get(i));
				}
			}
			t.toArray(tb);
			mutex.unlock();
			return tb;
		}
		else{
			mutex.unlock();
			return new Termin[]{};
		}
		
		
	}

	public boolean loeschen(final Termin termin) {
		if(! dobj.mitbewohnerverwaltung.getCurrent().equals(termin.getErsteller())){
			return false;
		}
		LogicController.getConnection().sendRequest(ProtokollTermin.Delete.getId(), ProtokollTermin.Delete.request(termin), new ResponseHandler() {

			@Override
			public void onResponse(JSONObject jObj) {
				mutex.lock();
				termine.get(termin.getVon().get(Calendar.MONTH) + "." + termin.getVon().get(Calendar.YEAR)).remove(termin);
				mutex.unlock();
			}
			
		});
		return true;
	}

	public void add(String name, final GregorianCalendar von, GregorianCalendar bis,
			Bewohner[] teilnehmer, Bewohner[] sichtbar, boolean ganzTags) 
	{
		final Termin t = new Termin(dobj.mitbewohnerverwaltung.getCurrent(), name, von, bis, teilnehmer, ganzTags);
		
		JSONObject jObj = ProtokollTermin.Add.request(name, von, bis, ganzTags, teilnehmer, sichtbar);
		LogicController.getConnection().sendRequest(ProtokollTermin.Add.getId(), jObj, new ResponseHandler() {

			@Override
			public void onResponse(JSONObject jObj) {
				mutex.lock();
				t.setId(jObj.getInt(ProtokollTermin.Add.ID));
				termine.get(von.get(Calendar.MONTH) + "." + von.get(Calendar.YEAR)).add(t);
				mutex.unlock();
			}
			
		});
	}
	
	public static boolean isSameDay(Calendar day1, Calendar day2) {
		return (day1.get(Calendar.YEAR) == day2.get(Calendar.YEAR)) && (day1.get(Calendar.MONTH) == day2.get(Calendar.MONTH)) && (day1.get(Calendar.DAY_OF_MONTH) == day2.get(Calendar.DAY_OF_MONTH));
	}
}
