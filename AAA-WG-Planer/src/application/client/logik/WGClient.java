package application.client.logik;

import java.awt.Dimension;

import application.client.gui.background.BackgroundWindow;


public class WGClient {

	public static void main(String[] args){
		BackgroundWindow back = new BackgroundWindow();
		back.setSize(450, 300);
		back.setVisible(true);
		
		//einkommentieren, wenn das Fenster zu klein erscheint und passende Größe ablesen
		Dimension size = back.getSize();
		while(back.isVisible()){
			Dimension s = back.getSize();
			if(!s.equals(size)){
				System.out.println(s);
				size = s;
			}
		}
	}
}
