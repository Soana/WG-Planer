package application.client.logik;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.JSONObject;

import application.shared.network.Connection;
import application.shared.network.ResponseHandler;
import application.shared.network.protokoll.ProtokollAllgemein;
import application.shared.network.protokoll.ProtokollBewohner;


public class LogicController {
	private static Bewohner bewohner;
	private static Connection connection;
	private static String lastKey = "./data/keys";
	
	/**
	 * Sieht nach, ob .key Dateien an dem letzten angegebenen Speicherort oder dem default Speicherort "\data\keys" existieren
	 * @return	true - wenn .key Dateien gefunden wurden
	 */
	public static boolean hasKey(){
		File dir = new File("./data/keys");
		String[] data = dir.list();
		for(String s: data){
			if(s.endsWith(".key")){
				return true;
			}
		}
		if(! lastKey.equals("./data/keys")){
			dir = new File(lastKey);
			data = dir.list();
			for(String s: data){
				if(s.endsWith(".key")){
					return true;
				}
			}
		}
		return false;
	}
	
	
	public static ResourceBundle getLanguageBundle(){
		return ResourceBundle.getBundle("data/lang/Main");
	}

	public static void disconnect()
	{
		if(connection != null){
			connection.shutdown();
		}
	}

	
	private static void sendLogin(String file, final String benutzer, char[] password){
		File dir = new File(file);
		String[] data = dir.list();
		for(String s: data){
			if(s.endsWith(".key")){
				try {
					Scanner sc = new Scanner(new File("/data/keys/" + s));
					String server = sc.next();
					int port = sc.nextInt();
					String name = sc.next();
					sc.close();
					connect(server, port);
					connection.sendRequestAndWait(ProtokollAllgemein.Login.getId(), ProtokollAllgemein.Login.request(name, benutzer, password), new ResponseHandler(){

						@Override
						public void onResponse(JSONObject jObj) {
							if(jObj.getBoolean(ProtokollAllgemein.Login.ERFOLG)){
								int id = jObj.getInt(ProtokollAllgemein.Login.ID);
								String vorname = jObj.getString(ProtokollAllgemein.Login.VORNAME);
								String nachname = jObj.getString(ProtokollAllgemein.Login.NACHNAME);
								String telefon = jObj.getString(ProtokollAllgemein.Login.TELEFON);
								long geb = jObj.getLong(ProtokollAllgemein.Login.GEBURTSDATUM);
								boolean isAdmin = jObj.getBoolean(ProtokollAllgemein.Login.ADMIN);
								
								bewohner = new Bewohner(vorname, nachname, new Date(geb), telefon, id);
								bewohner.setBenutzername(benutzer);
								if(isAdmin){
									bewohner.makeAdmin();
								}
							}
							else{
								bewohner = null;
							}
						}
						
					});
					
					if(bewohner == null){
						disconnect();
					}
					else{
						break;
					}
				}
				catch (FileNotFoundException e) {
					System.out.println(file + "/" + s + " konnte nicht geoeffnet werden.");
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Testet, ob die Login-Daten korrekt sind und ob die richtige Schluesseldatei vorhanden ist 
	 * @param benutzer der Benutzername
	 * @param password das Passwort
	 * @return den Bewohner, der sich angemeldet hat, wenn die Daten korrekt sind oder <code>null</code> wenn sie nicht korrekt sind
	 */
	public static Bewohner checkLogin(final String benutzer, final char[] password) {
		sendLogin("./data/keys", benutzer, password);
		if(bewohner == null){
			sendLogin(lastKey, benutzer, password);
		}
		return bewohner;
	}

	public static boolean connect(String server, int port) {
		try {
			connection = new Connection(new Socket(server, port));
			connection.start();
			return true;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}


	private static boolean available = false;
	public static boolean createWg(String name, String server, int port){
		disconnect();
		connect(server, port);
		connection.sendRequestAndWait(ProtokollAllgemein.CreateWG.getId(), ProtokollAllgemein.CreateWG.request(name), new ResponseHandler(){

			@Override
			public void onResponse(JSONObject jObj) {
				available = jObj.getBoolean(ProtokollAllgemein.CreateWG.ERFOLG);
			}
			
		});
		
		if(available){
			try {
				PrintWriter pw = new PrintWriter("./data/keys/" + name + ".key");
				
				pw.println(server);
				pw.println(port);
				pw.println(name);
				
				pw.flush();
				pw.close();
			}
			catch (FileNotFoundException e) {
				System.out.println("./data/keys/" + name + ".key kann nicht angelegt werden!");
				e.printStackTrace();
			}
			
		}
		else{
			disconnect();
		}
		return available;
	}


	public static Connection getConnection() {
		return connection;
	}
	
	public static Bewohner getAngemeldet() {
		return bewohner;
	}

	public static Bewohner createUser(String vorname, String nachname, GregorianCalendar geburtsdatum, String telefon) {
		if(bewohner != null){
			connection.sendRequestAndWait(ProtokollBewohner.Update.getId(), ProtokollBewohner.Update.request((int) bewohner.getId(), vorname, nachname, geburtsdatum, telefon), new ResponseHandler(){
			
	
				@Override
				public void onResponse(JSONObject jObj) {
					bewohner.setBenutzername(jObj.getString(ProtokollBewohner.Update.BENUTZERNAME));
				}
				
			});
		}
		else{
			//TODO neues Benutzer anlegen, falls gleich nach WG anlegen
		}
		return bewohner;
	}
	
}
