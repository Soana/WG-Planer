package application.client.logik;

import java.util.Date;

import javax.swing.Icon;


public class Bewohner {

	private long id;
	private String benutzername;
	private String kurz;
	private String vorname;
	private String nachname;
	private Date geburt;
	private String telefon;
	private boolean isAdmin = false;
	private int guthaben;

	public Bewohner(){
		guthaben = 0;
	}
	
	public Bewohner(String vorname, String nachname, Date geburtsdatum, String telefon) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburt = geburtsdatum;
		this.telefon = telefon;
		
		guthaben = 0;
		
		kurz = "" + vorname.charAt(0) + vorname.charAt(1) + nachname.charAt(0);
		benutzername = kurz;
	}
	
	public Bewohner(String vorname, String nachname, Date geburtsdatum, String telefon, long id)
	{
		this(vorname, nachname, geburtsdatum, telefon);
		this.id = id;
	}
	
	public boolean isAdmin() {
		return isAdmin;
	}

	public void makeAdmin(){
		isAdmin = true;
	}
	
	public void setName(String first, String last) {
		vorname = first;
		nachname = last;
	}

	public void setGuthaben(double money) {
		guthaben = (int) (money*100);
	}

	public int getGuthaben() {
		return guthaben;
	}

	public void setGuthaben(int guthaben) {
		this.guthaben = guthaben;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String firstname) {
		this.vorname = firstname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String lastname) {
		this.nachname = lastname;
	}

	public String getKurz() {
		return kurz;
	}

	public String getName() {
		return vorname + " " + nachname;
	}
	
	public String toString(){
		return getName();
	}

	public String getBenutzername() {
		return benutzername;
	}

	public Date getGeburt() {
		return geburt;
	}

	
	public void setGeburt(Date geburt) {
		this.geburt = geburt;
	}

	
	public String getTelefon() {
		return telefon;
	}

	
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	
	public void setBenutzername(String benutzername) {
		this.benutzername = benutzername;
		kurz = benutzername;
	}

	public void setKurz(String kurz) {
		this.kurz = kurz;
		benutzername = kurz;
	}

	public void setId(long id){
		this.id = id;
	}
	
	public long getId(){
		return id;
	}

	public void addGuthaben(int anteil) {
		guthaben += anteil;
	}
	
	public boolean equals(Object obj){
		if(obj == null){
			return false;
		}
		if(! (obj instanceof Bewohner)){
			return false;
		}
		return kurz.equals(((Bewohner) obj).getKurz());
	}
}
