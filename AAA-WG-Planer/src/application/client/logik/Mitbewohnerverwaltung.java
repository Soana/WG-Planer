package application.client.logik;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

import org.json.JSONArray;
import org.json.JSONObject;

import application.shared.network.RequestHandler;
import application.shared.network.ResponseHandler;
import application.shared.network.protokoll.ProtokollBewohner;


public class Mitbewohnerverwaltung {

	private Bewohner current;
	private ArrayList<Bewohner> bewohner;
	private ReentrantLock bewohnerMutex;

	public Mitbewohnerverwaltung(Bewohner current) {
		this.current = current;
		bewohnerMutex = new ReentrantLock();
		bewohner = new ArrayList<Bewohner>();
		LogicController.getConnection().registerRequestHandler(ProtokollBewohner.GetAll.getId(), new RequestHandler() {

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				bewohnerMutex.lock();
				bewohner = new ArrayList<Bewohner>();
				JSONArray jArr = jObj.getJSONArray(ProtokollBewohner.GetAll.BEWOHNER);
				for(int i = 0; i < jArr.length(); i++)
				{
					JSONObject bewohnerJ = jArr.getJSONObject(i);
					Bewohner b = new Bewohner(
							bewohnerJ.getString(ProtokollBewohner.GetAll.VORNAME), 
							bewohnerJ.getString(ProtokollBewohner.GetAll.NACHNAME), 
							new Date(bewohnerJ.getInt(ProtokollBewohner.GetAll.GEBURTSDATUM)),
							bewohnerJ.getString(ProtokollBewohner.GetAll.TELEFON),
							bewohnerJ.getLong(ProtokollBewohner.GetAll.ID));
					b.setBenutzername(ProtokollBewohner.GetAll.BENUTZERNAME);
					bewohner.add(b);
				}
				bewohnerMutex.unlock();
				return new JSONObject();
			}
		});
	}

	public void erstelleBewohnerUbersicht(){
		try {
			PrintWriter pw = new PrintWriter("mitbewohneruebersicht.txt");
			pw.println("Nachname, Vorname\tKürzel\tTelefon\tGeb.Datu,\tAdmin");
			bewohnerMutex.lock();
			for(Bewohner b: bewohner){
				pw.print(b.getNachname() + ", " + b.getVorname() + "\t");
				pw.print(b.getKurz() + "\t");
				pw.print(b.getTelefon() + "\t");
				Date cal = b.getGeburt();
				String date = cal.getDate() + "." + cal.getMonth() + "." + cal.getYear();
				pw.print(date + "\t");
				if(b.isAdmin()){
					pw.print("X");
				}
				pw.println();
			}
			bewohnerMutex.unlock();
			pw.flush();
			pw.close();
		}
		catch (FileNotFoundException e) {
			System.err.println("mitbewohneruebersicht.txt kann nicht erstellt werden!");
		}
	}
	
	public Bewohner[] getBewohner(){
		bewohnerMutex.lock();
		Bewohner[] arr = new Bewohner[bewohner.size()];
		bewohner.toArray(arr);
		bewohnerMutex.unlock();
		return arr;
	}


	public void loeschen(final Bewohner mitbewohner) {
		LogicController.getConnection().sendRequest(ProtokollBewohner.Delete.getId(), ProtokollBewohner.Delete.request(mitbewohner), new ResponseHandler() {

			@Override
			public void onResponse(JSONObject jObj)  {
				bewohnerMutex.lock();
				bewohner.remove(mitbewohner);
				bewohnerMutex.unlock();
			}
			
		});
	}

	private int index = -1;
	public String add(final String vorname, final String nachname) {
		LogicController.getConnection().sendRequestAndWait(ProtokollBewohner.Add.getId(), ProtokollBewohner.Add.request(vorname, nachname), new ResponseHandler() {

			@Override
			public void onResponse(JSONObject jObj) {
				Bewohner bw = new Bewohner(
						vorname, 
						nachname, 
						new Date(0),
						"",
						jObj.getLong(ProtokollBewohner.Add.ID));
				bw.setBenutzername(jObj.getString(ProtokollBewohner.Add.BENUTZERNAME));
				bewohnerMutex.lock();
				bewohner.add(bw);
				bewohnerMutex.unlock();
			}
			
		});

		bewohnerMutex.lock();
		String benutzername = bewohner.get(index).getBenutzername();
		bewohnerMutex.unlock();
		return benutzername;
	}

	public Bewohner getBewohnerByID(int id)
	{
		for(Bewohner b : bewohner)
		{
			if(b.getId() == id)
				return b;
		}
		return null;
	}

	public Object[][] getBewohnerAsObjects() {
		Bewohner[] bewohner = getBewohner();
		Object[][] b = new Object[bewohner.length][4];
		for(int i = 0; i < bewohner.length; i++){
			b[i][0] = bewohner[i].getNachname();
			b[i][1] = bewohner[i].getVorname();
			b[i][2] = bewohner[i].getGeburt();
			b[i][3] = bewohner[i].getTelefon();
		}
		return b;
	}

	public void erstelleGuthabenUbersicht() {
		try {
			PrintWriter pw = new PrintWriter("guthabensuebersicht.txt");
			Calendar cal = Calendar.getInstance();
			String d = cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH)+1) + "." + cal.get(Calendar.YEAR);
			pw.println(d);
			bewohnerMutex.lock();
			for(Bewohner b: bewohner){
				pw.println(b.getName() + ": " + (b.getGuthaben()/100.0));
			}
			bewohnerMutex.unlock();
			pw.flush();
			pw.close();
		}
		catch (FileNotFoundException e) {
			System.err.println("guthabensuebersicht.txt kann nicht erstellt werden!");
		}
	}

	public Bewohner getCurrent() {
		return current;
	}

	
	
}
