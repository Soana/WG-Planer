package application.client.logik;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

import org.json.JSONArray;
import org.json.JSONObject;

import application.shared.data.Eintrag;
import application.shared.data.Rechnung;
import application.shared.network.RequestHandler;
import application.shared.network.ResponseHandler;
import application.shared.network.protokoll.ProtokollEintrag;


public class Einkaufsliste {

	private DataObject data;
	
	private ReentrantLock eMutex, rMutex;
	private ArrayList<Eintrag> eintraege;
	private ArrayList<Rechnung> rechnungen;
	
	public Einkaufsliste(final DataObject dobj) {
		data = dobj;
		eMutex = new ReentrantLock();
		rMutex = new ReentrantLock();
		eintraege = new ArrayList<Eintrag>();
		rechnungen = new ArrayList<Rechnung>();
		LogicController.getConnection().registerRequestHandler(ProtokollEintrag.GetAllEintraege.getId(), new RequestHandler()
		{
			@Override
			public JSONObject onRequest(JSONObject jObj) 
			{
				eMutex.lock();
				eintraege = new ArrayList<>();
				JSONArray arr = jObj.getJSONArray(ProtokollEintrag.GetAllEintraege.EINTRAEGE);
				for(int i = 0; i < arr.length(); i++)
				{
					JSONObject eintragJ = arr.getJSONObject(i);
					JSONArray arrB = eintragJ.getJSONArray(ProtokollEintrag.GetAllEintraege.FUER);
					Bewohner[] bs = new Bewohner[arrB.length()];
					for(int j = 0; j < arrB.length(); j++)
						bs[j] = dobj.mitbewohnerverwaltung.getBewohnerByID(arrB.getInt(j));
					Eintrag e = new Eintrag(eintragJ.getString(ProtokollEintrag.GetAllEintraege.NAME), eintragJ.getInt(ProtokollEintrag.GetAllEintraege.ANZAHL), bs);
					e.setId(eintragJ.getInt(ProtokollEintrag.GetAllEintraege.ID));
					eintraege.add(e);
				}
				eMutex.unlock();
				return ProtokollEintrag.GetAllEintraege.response();
			}
			
		});
		LogicController.getConnection().registerRequestHandler(ProtokollEintrag.GetAllRechnungen.getId(), new RequestHandler()
		{
			@Override
			public JSONObject onRequest(JSONObject jObj) 
			{
				rMutex.lock();
				rechnungen = new ArrayList<>();
				JSONArray arr = jObj.getJSONArray(ProtokollEintrag.GetAllRechnungen.RECHNUNGEN);
				for(int i = 0; i < arr.length(); i++)
				{
					JSONObject rechnungJ = arr.getJSONObject(i);
					JSONArray arrB = rechnungJ.getJSONArray(ProtokollEintrag.GetAllRechnungen.FUER);
					Bewohner[] bs = new Bewohner[arrB.length()];
					for(int j = 0; j < arrB.length(); j++) {
						bs[j] = dobj.mitbewohnerverwaltung.getBewohnerByID(arrB.getInt(j));
					}
					rechnungen.add(new Rechnung(dobj.mitbewohnerverwaltung.getBewohnerByID(rechnungJ.getInt(ProtokollEintrag.GetAllRechnungen.VON)), bs, rechnungJ.getString(ProtokollEintrag.GetAllRechnungen.KOMMENTAR), rechnungJ.getInt(ProtokollEintrag.GetAllRechnungen.PREIS)));
				}
				rMutex.unlock();
				return new JSONObject();
			}
			
		});
	}

	public Eintrag[] getEintraege() {
		eMutex.lock();
		Eintrag[] es = new Eintrag[eintraege.size()];
		eintraege.toArray(es);
		eMutex.unlock();
		return es;
	}
	
	public Rechnung[] getRechnungen(){
		rMutex.lock();
		Rechnung[] rs = new Rechnung[rechnungen.size()];
		rechnungen.toArray(rs);
		rMutex.unlock();
		return rs;
	}
	
	public void rechnungStellen(final String kommentar, final int preis, final HashMap<Bewohner, Integer> wer){
		LogicController.getConnection().sendRequest(ProtokollEintrag.AddRechnung.getId(), ProtokollEintrag.AddRechnung.request(kommentar, preis, data.mitbewohnerverwaltung.getCurrent(), wer), new ResponseHandler() {

			@Override
			public void onResponse(JSONObject jObj) {
				
				JSONArray arr = jObj.getJSONArray(ProtokollEintrag.AddRechnung.ANTEILE);
				for(int i = 0; i < arr.length(); i++){
					data.mitbewohnerverwaltung.getBewohnerByID(arr.getJSONObject(i).getInt(ProtokollEintrag.AddRechnung.ID)).addGuthaben(arr.getJSONObject(i).getInt(ProtokollEintrag.AddRechnung.PREIS));
				}
				rMutex.lock();
				rechnungen.add(new Rechnung(data.mitbewohnerverwaltung.getCurrent(), wer.keySet().toArray(new Bewohner[]{}), kommentar, preis));
				rMutex.unlock();
			}
			
		});
	}

	public void loeschen(final Eintrag eintrag) {
		LogicController.getConnection().sendRequest(ProtokollEintrag.DeleteEintrag.getId(), ProtokollEintrag.DeleteEintrag.request(eintrag), new ResponseHandler() {

			@Override
			public void onResponse(JSONObject jObj) {
				eMutex.lock();
				eintraege.remove(eintrag);
				eMutex.unlock();
			}
			
		});
	}

	public void add(final String was, final int wieviel, final Bewohner[] fuer) {
		LogicController.getConnection().sendRequest(ProtokollEintrag.AddEintrag.getId(), ProtokollEintrag.AddEintrag.request(was, wieviel, fuer), new ResponseHandler() {

			@Override
			public void onResponse(JSONObject jObj) {
				eMutex.lock();
				Eintrag e = new Eintrag(was, wieviel, fuer);
				e.setId(jObj.getInt(ProtokollEintrag.AddEintrag.ID));
				eintraege.add(e);
				eMutex.unlock();
			}
			
		});
	}

	public boolean stelleRechnung() {
		try {
			File file = new File("einkaufsliste.txt");
			Scanner sc = new Scanner(file);
			HashMap<String, Integer> sortedEintraege = new HashMap<>();
			final LinkedList<Eintrag> es = new LinkedList<>();
			int l = 0;
			while(sc.hasNext()){
				l++;
				String[] line = sc.nextLine().split("\\s"); 
				if(line.length < 3){
					System.out.println("Fehler in einkaufsliste.txt, Zeile " + l);
					return false;
				}
				if(line.length < 4){
					System.out.println("Für Artikel " + line[1] + " für " + line[0] + "wurde kein Preis eingetragen.");
					continue;
				}
				String[] kurzs = line[0].split(",");
				eMutex.lock();
				for(Eintrag e: eintraege){
					if(e.getWas().equals(line[1])){
						Bewohner[] fuer = e.getFuer();
						boolean matches = true;
						for(String s: kurzs){
							boolean submatch = false;
							for(Bewohner b: fuer){
								if(b.getKurz().equals(s)){
									submatch = true;
									break;
								}
							}
							if(! submatch){
								matches = false;
							}
						}
						if(matches){
							es.add(e);
							e.setPreis((int)Double.parseDouble(line[3].replace(',', '.'))*100);
							if(sortedEintraege.containsKey(line[0])){
								sortedEintraege.put(line[0], sortedEintraege.get(line[0])+e.getPreis());
							}
							else{
								sortedEintraege.put(line[0], e.getPreis());
							}
							break;
						}
					}
				}
				eMutex.unlock();
			}
			sc.close();
			
			HashMap<String, Integer> preisProPerson = new HashMap<>();
			for(String line: sortedEintraege.keySet()){
				String[] kurzs = line.split(",");
				int anteil = sortedEintraege.get(line)/kurzs.length;
				if(sortedEintraege.get(line)%kurzs.length != 0){
					anteil++;
				}
				
				for(String s: kurzs){
					if(preisProPerson.containsKey(s)){
						preisProPerson.put(s, preisProPerson.get(s) + anteil);
					}
					else{
						preisProPerson.put(s, anteil);
					}
				}
			}
			Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
			for(String s: preisProPerson.keySet()){
				for(Bewohner b: bewohner){
					if(b.getKurz().equals(s)){
						HashMap<Bewohner, Integer> hash = new HashMap<>();
						hash.put(b, 0);
						rechnungStellen("Einkauf", preisProPerson.get(s), hash);
						break;
					}
				}
			}
			
			LogicController.getConnection().sendRequest(ProtokollEintrag.DeleteEintragMutiple.getId(), ProtokollEintrag.DeleteEintragMutiple.request(es.toArray(new Eintrag[]{})), new ResponseHandler(){

				@Override
				public void onResponse(JSONObject jObj) {
					for(Eintrag e: es){
						eintraege.remove(e);
					}
				}
				
			});
			return true;
		}
		catch (FileNotFoundException e) {
			System.err.println("einkaufsliste.txt nicht gefunden");
			return false;
		}
	}
	
	public void erstelleRechungsUbersicht() {
		try {
			PrintWriter pw = new PrintWriter("rechnungen.txt");
			
			Calendar cal = Calendar.getInstance();
			String date = cal.get(Calendar.DAY_OF_MONTH) + "." + cal.get(Calendar.MONTH) + "." + cal.get(Calendar.YEAR);
			pw.println("erstellt am: " + date);
			
			rMutex.lock();
			for(Rechnung r: rechnungen){
				Calendar c = r.getWann();
				date = c.get(Calendar.DAY_OF_MONTH) + "." + c.get(Calendar.MONTH) + "." + c.get(Calendar.YEAR);
				pw.print(date + "\t");
				
				pw.print(r.getVon().getKurz());
				pw.print(" für ");
				Bewohner[] b = r.getFuer();
				pw.print(b[0].getKurz());
				for(int i = 1; i < b.length; i++){
					pw.print(", " + b[i].getKurz());
				}
				
				pw.print("\t" + r.getWas());
				pw.println("\t " + r.getWieviel());
			}
			rMutex.unlock();
			
			pw.flush();
			pw.close();
		}
		catch (FileNotFoundException e) {
			System.err.println("rechungen.txt kann nicht erstellt werden!");
		}
	}
	
	public void erstelleEinkaufsliste(){
		try {
			PrintWriter pw = new PrintWriter("einkaufsliste.txt");
			
			eMutex.lock();
			for(Eintrag e: eintraege){
				Bewohner[] fuer = e.getFuer();
				pw.print(fuer[0].getKurz());
				for(int i = 1; i < fuer.length; i++){
					pw.print("," + fuer[i].getKurz());
				}
				pw.print("\t" + e.getWas());
				pw.print("\t" + e.getWieviel());
				pw.println("\t");
			}
			eMutex.unlock();
			
			pw.flush();
			pw.close();
		}
		catch (FileNotFoundException e) {
			System.err.println("einkaufsliste.txt kann nicht erstellt werden!");
		}
	}
	
}
