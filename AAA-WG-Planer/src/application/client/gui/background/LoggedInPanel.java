package application.client.gui.background;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyVetoException;

import javax.swing.*;

import application.client.gui.SubPanel;
import application.client.gui.foreground.*;
import application.client.logik.Bewohner;
import application.client.logik.DataObject;
import application.client.logik.LogicController;


public class LoggedInPanel extends ModePanel{

	private enum Type{
		MITBEWOHNER,
		KALENDER,
		KASSE,
		EINKAUFSLISTE,
		EINKAUFSLISTE_GENERELL,
		EINTRAGEN,
		VERWALTEN,
		EINSTELLUNGEN,
		DATEI
	}
	
	private JDesktopPane pnlContent;
	
	private JCheckBoxMenuItem mitemAbout;
	private JCheckBoxMenuItem mitemMitbewohnerListe;
	private JCheckBoxMenuItem mitemKalender;
	private JCheckBoxMenuItem mitemKasse;
	private JCheckBoxMenuItem mitemMeineEinkaufsliste;
	private JCheckBoxMenuItem mitemEinkaufsliste;
	private JCheckBoxMenuItem mitemEinkaufEintragen;
	private JCheckBoxMenuItem mitemMitbewohnerVerwalten;
	private JCheckBoxMenuItem mitemEinstellungen;
	private JCheckBoxMenuItem mitemDateien;
	
	private Bewohner bewohner;
	private DataObject dobj;
	
	public LoggedInPanel(BackgroundWindow parent) {
		super(parent);
		
		listener = new LoggedInListener();

		dobj = DataObject.getInstance(LogicController.getAngemeldet());
		
		help = "loggedin.help";
		
		
		pnlContent = new JDesktopPane();
		pnlContent.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		
		this.setLayout(new BorderLayout());
		this.add(pnlContent);
		this.validate();
	}
	
	@Override
	public JMenuBar getMenuBar() {
		JMenuBar menuBar= new JMenuBar();
		JMenu menu = new JMenu("Menü");
		JMenu help = new JMenu("Hilfe");

		mitemAbout = new JCheckBoxMenuItem("Über");
		mitemMitbewohnerListe = new JCheckBoxMenuItem("Mitbewohnerliste");
		mitemKalender = new JCheckBoxMenuItem("Kalender");
		mitemKasse = new JCheckBoxMenuItem("Kasse");
		mitemMeineEinkaufsliste = new JCheckBoxMenuItem("Meine Einkaufsliste");
		mitemEinkaufsliste = new JCheckBoxMenuItem("Gesamte Einkaufsliste");
		mitemEinkaufEintragen = new JCheckBoxMenuItem("Einkauf eintragen");
		mitemMitbewohnerVerwalten = new JCheckBoxMenuItem("Mitbewohnerverwaltung");
		mitemEinstellungen = new JCheckBoxMenuItem("WG-Einstellungen");
		mitemDateien = new JCheckBoxMenuItem("Dateien");
		
		mitemAbout.addActionListener(listener);
		mitemMitbewohnerListe.addActionListener(listener);
		mitemKalender.addActionListener(listener);
		mitemKasse.addActionListener(listener);
		mitemMeineEinkaufsliste.addActionListener(listener);
		mitemEinkaufsliste.addActionListener(listener);
		mitemEinkaufEintragen.addActionListener(listener);
		mitemMitbewohnerVerwalten.addActionListener(listener);
		mitemEinstellungen.addActionListener(listener);
		mitemDateien.addActionListener(listener);
		mitemClose.addActionListener(listener);
		mitemHelp.addActionListener(listener);
		
		menu.setMnemonic(KeyEvent.VK_M);
		help.setMnemonic(KeyEvent.VK_H);

		menu.add(mitemMitbewohnerListe);
		menu.add(mitemKalender);
		menu.add(mitemKasse);
		menu.add(mitemMeineEinkaufsliste);
		menu.add(mitemEinkaufsliste);
		menu.add(mitemEinkaufEintragen);
		if(dobj.mitbewohnerverwaltung.getCurrent().isAdmin()){
			menu.add(mitemMitbewohnerVerwalten);
			menu.add(mitemEinstellungen);
		}
		menu.add(mitemDateien);
		menu.add(mitemClose);
		help.add(mitemHelp);
		help.add(mitemAbout);
		
		menuBar.add(menu);
		menuBar.add(help);
		
		return menuBar;
	}

	public void save() {
		for(JInternalFrame internal: pnlContent.getAllFrames()){
			((FunctionPanel) internal.getContentPane()).save();
		}
	}
	
	private class LoggedInListener extends ModePanelListener{
		
		private void addFrame(Type type, JCheckBoxMenuItem mitem){
			if(! mitem.isSelected()){
				for(JInternalFrame frame: pnlContent.getAllFrames()){
					if(frame.getName().equals(type.name())){
						pnlContent.remove(frame);
						pnlContent.repaint();
						break;
					}
				}
				
				return;
			}
			
			FunctionPanel pnl = null;
			
			switch(type){
				case DATEI:
					break;
				case EINKAUFSLISTE:
					break;
				case EINKAUFSLISTE_GENERELL:
					pnl = new EinkaufslistePanel(parent, dobj);
					break;
				case EINSTELLUNGEN:
					pnl = new EinstellungenPanel(parent, dobj);
					break;
				case EINTRAGEN:
					pnl = new EintragenPanel(parent, dobj);
					break;
				case KALENDER:
					pnl = new KalenderPanel(parent, dobj);
					break;
				case KASSE:
					pnl = new KassePanel(parent, dobj);
					break;
				case MITBEWOHNER:
					pnl = new MitbewohnerListePanel(parent, dobj);
					break;
				case VERWALTEN:
					pnl = new MitbewohnerVerwaltenPanel(parent, dobj);
					break;
				default:
					break;
				
			}
			
			JInternalFrame frame = new JInternalFrame(pnl.getName(), pnl.getResizeable(), pnl.getCloseable(), pnl.getMaximizable(), pnl.getIconifyable());
			frame.setName(type.name());
			frame.add(pnl);
			frame.setLocation(pnl.getXPos(), pnl.getYPos());
			frame.setSize(pnl.getXSize(), pnl.getYSize());
			frame.pack();
			frame.setVisible(true);
			
			pnlContent.add(frame);
			try {
				frame.setSelected(true);
			}
			catch (PropertyVetoException e) {
				System.out.println("weird focus problem");
			}
		}
		
		@Override
		public void actionPerformed(ActionEvent evt){
			super.actionPerformed(evt);
			if(evt.getSource().equals(mitemEinkaufEintragen)){
				addFrame(Type.EINTRAGEN, mitemEinkaufEintragen);
			}
			else if(evt.getSource().equals(mitemEinkaufsliste)){
				addFrame(Type.EINKAUFSLISTE_GENERELL, mitemEinkaufsliste);
			}
			else if(evt.getSource().equals(mitemKasse)){
				addFrame(Type.KASSE, mitemKasse);
			}
			else if(evt.getSource().equals(mitemKalender)){
				addFrame(Type.KALENDER, mitemKalender);
			}
			else if(evt.getSource().equals(mitemMitbewohnerListe)){
				addFrame(Type.MITBEWOHNER, mitemMitbewohnerListe);
			}
			else if(evt.getSource().equals(mitemMitbewohnerVerwalten)){
				addFrame(Type.VERWALTEN, mitemMitbewohnerVerwalten);
			}
			else if(evt.getSource().equals(mitemMeineEinkaufsliste)){
				addFrame(Type.EINKAUFSLISTE, mitemMeineEinkaufsliste);
			}
			else if(evt.getSource().equals(mitemEinstellungen)){
				addFrame(Type.EINSTELLUNGEN, mitemEinstellungen);
			}
			else if(evt.getSource().equals(mitemDateien)){
				//Dateidialog
			}
			else if(evt.getSource().equals(mitemClose)){
				LogicController.disconnect();
				save();
				parent.dispose();
			}
			
		}
	}
}
