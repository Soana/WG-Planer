package application.client.gui.background;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import application.client.gui.SubPanel;

public abstract class ModePanel extends SubPanel{
	protected ModePanelListener listener;
	
	protected JMenuItem mitemHelp;
	protected JMenuItem mitemClose;
	protected String help;
	
	public ModePanel(BackgroundWindow parent) {
		super(parent);
		listener = new ModePanelListener();
		
		setupDefaultMenuItems();
	}

	private void setupDefaultMenuItems() {
		mitemHelp = new JMenuItem("Hilfe");
		mitemClose = new JMenuItem("Beenden");
		
		mitemHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
	}

	protected JMenuBar getDefaultMenuBar(ModePanelListener listener){
		JMenuBar menuBar= new JMenuBar();
		JMenu menu = new JMenu("Menü");
		JMenu help = new JMenu("Hilfe");

		mitemClose.addActionListener(listener);
		mitemHelp.addActionListener(listener);
		
		menu.setMnemonic(KeyEvent.VK_M);
		help.setMnemonic(KeyEvent.VK_H);

		menu.add(mitemClose);
		help.add(mitemHelp);
		
		menuBar.add(menu);
		menuBar.add(help);
		
		return menuBar;
	}
	
	public JMenuBar getMenuBar(){
		return getDefaultMenuBar(listener);
	}
	
	public WindowListener getWindowListener(){
		return listener;
	}
	
	protected class ModePanelListener extends SubPanelListener implements WindowListener, ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent evt) {
			if(evt.getSource() == mitemHelp){
				displayHelp(help);
			}
			else if(evt.getSource() == mitemClose){
				parent.dispose();
			}
		}
		
		@Override
		public void windowClosing(WindowEvent e) {
			parent.dispose();
		}
		
		//****************************************************************************************************************
		@Override
		public void windowActivated(WindowEvent e) {
			
		}

		@Override
		public void windowClosed(WindowEvent e) {
			
		}

		

		@Override
		public void windowDeactivated(WindowEvent e) {
			
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			
		}

		@Override
		public void windowOpened(WindowEvent e) {
			
		}

		
		
	}
}
