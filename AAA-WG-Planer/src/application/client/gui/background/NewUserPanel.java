package application.client.gui.background;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import application.client.gui.SubPanel;
import application.client.gui.background.BackgroundWindow.Mode;
import application.client.logik.LogicController;
import application.client.logik.Bewohner;


public class NewUserPanel extends ModePanel {
	
	private JLabel lblVorname;
	private JLabel lblNachname;
	private JLabel lblTelefon;
	private JLabel lblGeburt;
	private JLabel lblPasswort;
	private JLabel lblRepeatPasswort;
	
	private JTextField txtVorname;
	private JTextField txtNachname;
	private JTextField txtTelefon;
	private JSpinner spnTag;
	private JSpinner spnMonat;
	private JSpinner spnJahr;
	private JPasswordField passPasswort;
	private JPasswordField passRepeatPasswort;
	private JButton btnCreate;
	
	private SpinnerNumberModel modelTag;
	private SpinnerNumberModel modelMonat;
	private SpinnerNumberModel modelJahr;
	
	public NewUserPanel(BackgroundWindow parent) {
		super(parent);
		listener = new NewUserListener(this);
		
		help = "newuser.help";
		
		this.setLayout(new BorderLayout());
		setupGUI();
	}

	private void setupGUI(){
		lblVorname = new JLabel("Vorname: ");
		lblNachname = new JLabel("Nachname: ");
		lblTelefon = new JLabel("Telefon: ");
		lblGeburt = new JLabel("Geburtsdatum: ");
		lblPasswort = new JLabel("Passwort: ");
		lblRepeatPasswort = new JLabel("Passwort wiederholen: ");
		
		txtVorname = new JTextField();
		txtNachname = new JTextField();
		txtTelefon = new JTextField();
		JPanel spinner = getSpinners();
		passPasswort = new JPasswordField();
		passRepeatPasswort = new JPasswordField();
		btnCreate = new JButton("Weiter");
		
		btnCreate.addActionListener(listener);
		
		txtVorname.addKeyListener(listener);
		txtNachname.addKeyListener(listener);
		txtTelefon.addKeyListener(listener);
		passPasswort.addKeyListener(listener);
		passRepeatPasswort.addKeyListener(listener);
		btnCreate.addKeyListener(listener);
		
		JPanel gui = new JPanel();
		
		GroupLayout layout = new GroupLayout(gui);
		
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		int textSize = (int) (btnCreate.getPreferredSize().getWidth()*2.5);
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addComponent(lblVorname)
						.addComponent(lblNachname)
						.addComponent(lblTelefon)
						.addComponent(lblGeburt)
						.addComponent(lblPasswort)
						.addComponent(lblRepeatPasswort)
				)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addComponent(txtVorname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, textSize)
						.addComponent(txtNachname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, textSize)
						.addComponent(txtTelefon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, textSize)
						.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, textSize)
						.addComponent(passPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, textSize)
						.addComponent(passRepeatPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, textSize)
						.addComponent(btnCreate)
				)
		);
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(lblVorname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtVorname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(lblNachname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtNachname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(lblTelefon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtTelefon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(lblGeburt, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(spinner, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, spinner.getWidth())
				)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(lblPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(passPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(lblRepeatPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(passRepeatPasswort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				)
				.addComponent(btnCreate)
		);
		
		gui.setLayout(layout);
		gui = this.centerComponent(Axis.BOTH, gui);
		
		this.add(gui, BorderLayout.CENTER);
	}
	
	private JPanel getSpinners(){
		Calendar cal = Calendar.getInstance();
		
		int year = cal.get(Calendar.YEAR);
		modelJahr = new SpinnerNumberModel(year - 18, year - 150, year, 1);
		modelMonat = new SpinnerNumberModel(cal.get(Calendar.MONTH)+1, 1, 12, 1);
		modelTag = new SpinnerNumberModel(cal.get(Calendar.DAY_OF_MONTH), 1, cal.getActualMaximum(Calendar.DAY_OF_MONTH), 1);
		
		spnTag = new JSpinner(modelTag);
		spnMonat = new JSpinner(modelMonat);
		spnJahr = new JSpinner(modelJahr);
		
		spnJahr.setEditor(new JSpinner.NumberEditor(spnJahr, "####"));
		
		spnTag.addChangeListener((ChangeListener) listener);
		spnMonat.addChangeListener((ChangeListener) listener);
		spnJahr.addChangeListener((ChangeListener) listener);
		
		spnTag.addKeyListener(listener);
		spnMonat.addKeyListener(listener);
		spnJahr.addKeyListener(listener);
		
		JPanel spinner = new JPanel();
		spinner.setLayout(new FlowLayout(FlowLayout.CENTER,0, this.hgap));
		spinner.add(spnTag);
		spinner.add(spnMonat);
		spinner.add(spnJahr);
		spinner.add(Box.createHorizontalGlue());
		
		return spinner;
	}
	@Override
	public JMenuBar getMenuBar() {
		return getDefaultMenuBar(listener);
	}

	@Override
	public void save() {
		//nothing to do
	}
	
	private class NewUserListener extends ModePanelListener implements ActionListener, ChangeListener{
		NewUserPanel listenTo;
		
		public NewUserListener(NewUserPanel listento){
			listenTo = listento;
		}

		private void verifyValue(JSpinner spn){
			spn.setValue((Integer) spn.getValue()); 
			Integer value = (Integer) spn.getValue();
			SpinnerNumberModel model = (SpinnerNumberModel) spn.getModel();
			if(value <= (Integer) model.getMaximum() && value >= (Integer) model.getMinimum()){
				model.setValue(value);
			}
			else{
				spn.setValue(model.getValue());
			}
		}
		
		@Override
		public void stateChanged(ChangeEvent evt) {
			if(evt.getSource() instanceof JSpinner){
				verifyValue((JSpinner) evt.getSource());
			}
			
			if(evt.getSource().equals(spnMonat) || evt.getSource().equals(spnJahr)){
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.DAY_OF_WEEK, (Integer) spnTag.getValue());
				cal.set(Calendar.MONTH, ((Integer) spnMonat.getValue())-1);
				cal.set(Calendar.YEAR, (Integer) spnJahr.getValue());
				
				modelTag.setMaximum(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
				
				if((Integer) modelTag.getValue() > (Integer) modelTag.getMaximum()){
					modelTag.setValue(modelTag.getMaximum());
				}
				spnTag.setValue(modelTag.getValue());
			}
		}
		
		@Override
		public void actionPerformed(ActionEvent evt) {
			super.actionPerformed(evt);
			
			if(evt.getSource().equals(btnCreate)){
				System.out.println("Create");
				GregorianCalendar date = new GregorianCalendar((int) spnJahr.getValue(), (int) spnMonat.getValue(), (int) spnTag.getValue());
				Bewohner user = LogicController.createUser(txtVorname.getText(), txtNachname.getText(), date, txtTelefon.getText());
				JOptionPane.showConfirmDialog(listenTo, "Dein Benutzername ist: " + user.getBenutzername(), "Benutzername", JOptionPane.INFORMATION_MESSAGE);
				if(user != null){
					parent.setMode(Mode.LOGGED_IN);
				}
			}
		}
	}
}
