package application.client.gui.background;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.GroupLayout.Group;

import testcases.TestFrame;
import application.client.gui.SubPanel;
import application.client.gui.background.BackgroundWindow.Mode;
import application.client.logik.Bewohner;
import application.client.logik.LogicController;


public class LogInPanel extends ModePanel {
	
	private JMenuItem mitemSearchKey;
	private JMenuItem mitemNewWg;
	
	private JTextField txtUsername;
	private JPasswordField passPassword;
	private JButton btnLogin;
	private JButton btnNewUser;

	private JLabel lblUsername;
	private JLabel lblPassword;
	private JLabel lblInfo;
	
	private String lastKey;
	
	public LogInPanel(BackgroundWindow parent) {
		super(parent);
		listener = new LogInListener();
		
		help = "login.help";
		
		this.setLayout(new BorderLayout());
		this.add(createGUI());
	}
	
	private JPanel createGUI(){
		//create labels
		lblUsername = new JLabel("Benutzername:");
		lblPassword = new JLabel("Passwort:");
		
//		lblUsername.setBorder(BorderFactory.createLineBorder(Color.WHITE));
		
		//create input elements
		txtUsername = new JTextField();
		passPassword = new JPasswordField();
		btnLogin = new JButton("Login");
		btnNewUser = new JButton("Neuer Benutzer");
		
		//add Listeners to elements
		btnLogin.addActionListener(listener);
		btnLogin.addKeyListener(listener);
		btnNewUser.addActionListener(listener);
		btnNewUser.addKeyListener(listener);
		txtUsername.addKeyListener(listener);
		passPassword.addKeyListener(listener);
		
		//generate Panel
		JPanel gui = new JPanel();
		
		GroupLayout layout = new GroupLayout(gui);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		//Funktioniert aus irgendwelchen Gründen nicht, entspricht scheinbar 1*buttonsize...
		int textSize = (int) (btnLogin.getPreferredSize().getWidth()*2.5);
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addComponent(lblUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				)
				.addGroup(layout.createParallelGroup()
						.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, textSize)
						.addComponent(passPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, textSize)
						.addComponent(btnLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				)
		);
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
						.addComponent(lblUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				)
				.addGroup(layout.createParallelGroup()
						.addComponent(lblPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(passPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE)
				)
				.addGroup(layout.createParallelGroup()
						.addComponent(btnLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				)
		);
		
		gui.setLayout(layout);
//		gui.setBorder(BorderFactory.createLineBorder(Color.green));
		
		JPanel superGui = new JPanel();
		superGui.setLayout(new BoxLayout(superGui, BoxLayout.PAGE_AXIS));
//		superGui.setBorder(BorderFactory.createLineBorder(Color.blue));
		lblInfo = new JLabel();
//		lblInfo.setBorder(BorderFactory.createLineBorder(Color.red));
		
		superGui.add(gui);
		superGui.add(lblInfo);
		
		superGui.setSize(200,200);
		superGui = centerComponent(Axis.BOTH, superGui);
//		superGui.setBorder(BorderFactory.createLineBorder(Color.YELLOW));
		return superGui;
	}

	@Override
	public JMenuBar getMenuBar() {
		JMenuBar menuBar= new JMenuBar();
		JMenu menu = new JMenu("Menü");
		JMenu help = new JMenu("Hilfe");
		
		mitemSearchKey = new JMenuItem("Suche .key Dateien");
		mitemNewWg = new JMenuItem("Neue WG erstellen");
		
		mitemSearchKey.addActionListener(listener);
		mitemNewWg.addActionListener(listener);
		mitemClose.addActionListener(listener);
		mitemHelp.addActionListener(listener);
		
		menu.setMnemonic(KeyEvent.VK_M);
		help.setMnemonic(KeyEvent.VK_H);
		
		menu.add(mitemSearchKey);
		menu.add(mitemNewWg);
		menu.add(mitemClose);
		help.add(mitemHelp);
		
		menuBar.add(menu);
		menuBar.add(help);
		
		return menuBar;
	}
	
	@Override
	public void save() {
		//nothing to do
	}
	
	private class LogInListener extends ModePanelListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent evt) {
			super.actionPerformed(evt);
			
			if(evt.getSource() == btnLogin){
				Bewohner b = LogicController.checkLogin(txtUsername.getText(), passPassword.getPassword());
				if( b != null){
					if(b.getVorname() != null){
						parent.setMode(Mode.LOGGED_IN);
					}
					else{
						parent.setMode(Mode.NEW_USER);
					}
				}
				else{
					lblInfo.setText("Benutername oder Password werden nicht erkannt");
				}
			}
			else if(evt.getSource() == mitemSearchKey){
				//notTODO .key dateien Suchen!
				System.out.println("Searching..."); 
			}
			else if(evt.getSource() == mitemNewWg){
				parent.setMode(Mode.NEW_WG); 
			}
		}

		@Override
		public void windowClosing(WindowEvent evt) {
			LogicController.saveKeysLocation(lastKey);
			parent.dispose();
		}
	}
}
