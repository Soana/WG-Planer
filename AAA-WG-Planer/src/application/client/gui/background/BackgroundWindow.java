package application.client.gui.background;

import java.awt.event.WindowListener;

import javax.swing.*;

import application.client.logik.LogicController;


public class BackgroundWindow extends JFrame {
	
	public static enum Mode{
		NO_WG,		//Wenn keine Schluesseldatei vorhanden ist
		NEW_WG,		//Wenn neue WG erstellt werden soll
		LOG_IN,		//Wenn Schluesseldatei vorhanden ist
		NEW_USER,	//Mit Schluesseldatei, ohne guelitgen Login
		LOGGED_IN	//wenn der Benutzer eingeloggt ist
	}
	
	private Mode mode;
	private JMenuBar menuBar;
	private ModePanel pnlContent;
	
	private WindowListener windowListener;
	
	public BackgroundWindow(){
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		if(LogicController.hasKey()){
			setMode(Mode.LOG_IN);
		}
		else{
			setMode(Mode.NO_WG);
		}
		
		this.setSize(300, 300);
	}

	public void setMode(Mode mode){
		this.mode = mode;
		
		switch(mode){
			case NO_WG:
				pnlContent = new NoWgPanel(this);
				break;
			case NEW_WG:
				pnlContent = new NewWgPanel(this);
				break;
			case LOG_IN:
				pnlContent = new LogInPanel(this);
				break;
			case NEW_USER:
				pnlContent = new NewUserPanel(this);
				break;
			case LOGGED_IN:
				pnlContent = new LoggedInPanel(this);
				break;
		}
		menuBar = pnlContent.getMenuBar();
		this.setContentPane(pnlContent);
		this.setJMenuBar(menuBar);
		this.validate();
		
		this.removeWindowListener(windowListener);
		windowListener = pnlContent.getWindowListener();
		this.addWindowListener(windowListener);
	}
	
}
