package application.client.gui.background;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.*;

import javax.swing.*;

import application.client.gui.SubPanel;
import application.client.gui.background.BackgroundWindow.Mode;
import application.client.logik.LogicController;


public class NoWgPanel extends ModePanel {
	
	private JLabel lblInfo;
	private JButton btnSearch;
	private JButton btnRefresh;
	private JButton btnNew;
	
	public NoWgPanel(BackgroundWindow parent) {
		super(parent);
		listener = new NoWgListener();
		
		help = "nowg.help";
		setupGui();
	}

	private void setupGui() {
		lblInfo = new JLabel(lang.getString("nowg.info"));
		btnSearch = new JButton("Schlüsseldatei angeben");
		btnRefresh = new JButton("Suchen");
		btnNew = new JButton("Neue WG erstellen");
		
		btnSearch.addActionListener(listener);
		btnRefresh.addActionListener(listener);
		btnNew.addActionListener(listener);
		
		btnSearch.addKeyListener(listener);
		btnRefresh.addKeyListener(listener);
		btnNew.addKeyListener(listener);
		
		JPanel btn = new JPanel();
		btn.setLayout(new BoxLayout(btn, BoxLayout.LINE_AXIS));
		
		btn.add(btnSearch);
		btn.add(Box.createHorizontalStrut(hgap));
		btn.add(btnRefresh);
		
		JPanel gui = new JPanel();
		
		
		GroupLayout layout = new GroupLayout(gui);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addComponent(lblInfo)
						.addComponent(btn)
						.addComponent(btnNew)
				)
				
		);
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addComponent(lblInfo)
				.addComponent(btn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(btnNew)
		);
		
		gui.setLayout(layout);
		gui = centerComponent(Axis.BOTH, gui);
		
		this.setLayout(new BorderLayout());
		this.add(gui);
	}

	private class NoWgListener extends ModePanelListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent evt) {
			super.actionPerformed(evt);
			
			if(evt.getSource().equals(btnSearch)){
				//notTODO .key Dateien suchen
			}
			else if(evt.getSource().equals(btnRefresh)){
				if(LogicController.hasKey()){
					parent.setMode(Mode.LOG_IN);
				}
			}
			else if(evt.getSource().equals(btnNew)){
				parent.setMode(Mode.NEW_WG);
			}
		}
	}

	@Override
	public void save() {
		// nothing to do
		
	}
}
