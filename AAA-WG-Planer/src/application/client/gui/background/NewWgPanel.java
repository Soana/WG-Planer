package application.client.gui.background;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.*;

import javax.swing.*;

import application.client.gui.SubPanel;
import application.client.gui.background.BackgroundWindow.Mode;
import application.client.logik.LogicController;


public class NewWgPanel extends ModePanel {

	private JLabel lblName;
	private JLabel lblServer;
	private JLabel lblPort;
	private JLabel lblConnectError;
	private JLabel lblNameError;
	
	private JTextField txtName;
	private JTextField txtServer;
	private JTextField txtPort;
	
	private JButton btnCheck;
	private JButton btnCreate;
	
	public NewWgPanel(BackgroundWindow parent) {
		super(parent);
		
		listener = new NewWgListener();
		
		help = "newwg.help";
		
		setupGui();
	}

	private void setupGui() {
		lblName = new JLabel("Name:");
		lblServer = new JLabel("Server:");
		lblPort = new JLabel("Port:");
		lblConnectError = new JLabel();
		lblNameError = new JLabel();
		
		lblConnectError.setForeground(Color.red);
		lblNameError.setForeground(Color.red);
		
		txtName = new JTextField();
		txtServer = new JTextField();
		txtPort = new JTextField("80");
		
		btnCheck = new JButton("Teste Verbindung");
		btnCreate = new JButton("Weiter");
		
		btnCheck.addActionListener(listener);
		btnCreate.addActionListener(listener);
		
		JPanel gui = new JPanel();
		
		GroupLayout layout = new GroupLayout(gui);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		layout.setHorizontalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
						.addComponent(lblName)
						.addComponent(lblServer)
						.addComponent(lblPort)
				)
				.addGroup(layout.createParallelGroup()
						.addComponent(txtName)
						.addComponent(txtServer)
						.addComponent(txtPort)
						.addComponent(btnCreate)
				)
				.addGroup(layout.createParallelGroup()
						.addComponent(lblNameError)
						.addComponent(btnCheck)
						.addComponent(lblConnectError)
				)
		);
		
		layout.setVerticalGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup()
						.addComponent(lblName)
						.addComponent(txtName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNameError)
				)
				.addGroup(layout.createParallelGroup()
						.addComponent(lblServer)
						.addComponent(txtServer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnCheck)
				)
				.addGroup(layout.createParallelGroup()
						.addComponent(lblPort)
						.addComponent(txtPort, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblConnectError)
				)
				.addComponent(btnCreate)
		);
		
		gui.setLayout(layout);
		
		this.setLayout(new BorderLayout());
		this.add(centerComponent(Axis.BOTH, gui));
	}

	private class NewWgListener extends ModePanelListener implements ActionListener, KeyListener{
		
		@Override
		public void actionPerformed(ActionEvent evt){
			super.actionPerformed(evt);
			
			if(evt.getSource().equals(btnCheck)){
				if( !LogicController.connect(txtServer.getText(), Integer.parseInt(txtPort.getText())) ){
					btnCreate.setEnabled(false);
					lblConnectError.setText("Es kann keine Verbindung aufgebaut werden");
				}
			}
			else if(evt.getSource().equals(btnCreate)){
				if(LogicController.createWg(txtName.getText(), txtServer.getText(), Integer.parseInt(txtPort.getText()))){
					parent.setMode(Mode.NEW_USER);
				}
				else{
					if(LogicController.connect(txtServer.getText(), Integer.parseInt(txtPort.getText()))){
						lblNameError.setText("Dieser Name ist bereits vergeben");
					}
					else{
						lblConnectError.setText("Es kann keine Verbindung aufgebaut werden");
					}
				}
			}
			
		}
	}

	@Override
	public void save() {
		// nothing to do
		
	}
}
