package application.client.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import application.client.gui.background.BackgroundWindow;
import application.client.logik.LogicController;



public abstract class SubPanel extends JPanel {
	public enum Axis{
		X,
		Y,
		BOTH
	}
	
	protected ResourceBundle lang;
	
	protected int hgap = 10;
	protected int vgap = 10;
	
	protected BackgroundWindow parent;
	
	public SubPanel(BackgroundWindow parent){
		this.parent = parent;
		this.lang = LogicController.getLanguageBundle();
	}
	
	public static JPanel centerComponent(Axis axis, Component comp){
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		
		Box horizontalBox = null, verticalBox = null;
		if(axis == Axis.X || axis == Axis.BOTH){
			horizontalBox = Box.createHorizontalBox();
			horizontalBox.add(Box.createHorizontalGlue());
			horizontalBox.add(comp);
			horizontalBox.add(Box.createHorizontalGlue());
		}
		if(axis == Axis.Y || axis == Axis.BOTH){
			verticalBox = Box.createVerticalBox();
			verticalBox.add(Box.createVerticalGlue());
			if(axis == Axis.BOTH){
				verticalBox.add(horizontalBox);
			}
			else{
				verticalBox.add(comp);
			}
			verticalBox.add(Box.createVerticalGlue());
			panel.add(verticalBox);
		}
		
		return panel;
	}
	
	public abstract void save();
	
	public class SubPanelListener implements KeyListener, WindowListener{
		
		protected void displayHelp(String which) {
			final JDialog dialog = new JDialog(parent, "Hilfe");

			String h = lang.getString(which);
			JLabel label = new JLabel("<html><p>" + h);
			
			//Schliessen Knopf
			JButton closeButton = new JButton("Close");
			closeButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dialog.setVisible(false);
					dialog.dispose();
				}
			});
			JPanel closePanel = new JPanel();
			closePanel.setLayout(new BoxLayout(closePanel, BoxLayout.LINE_AXIS));
			closePanel.add(Box.createHorizontalGlue());
			closePanel.add(closeButton);
			closePanel.setBorder(BorderFactory.createEmptyBorder(0,0,5,5));

			//Test und Knopf zusammen setzen
			JPanel contentPane = new JPanel(new BorderLayout());
			contentPane.add(label, BorderLayout.CENTER);
			contentPane.add(closePanel, BorderLayout.PAGE_END);
			contentPane.setOpaque(true);
			dialog.setContentPane(contentPane);

			//anzeigen
			dialog.setSize(300, 150);
			dialog.setLocationRelativeTo(parent);
			dialog.setVisible(true);
		}

		@Override
		public void keyPressed(KeyEvent evt) {
			if(evt.getKeyCode() == KeyEvent.VK_ENTER){
				if(evt.getSource() instanceof JButton){
					((JButton) evt.getSource()).doClick();
				}
				else{
					evt.getComponent().transferFocus();
				}
			}
		}
		
		@Override
		public void windowClosing(WindowEvent evt) {
			save();
			LogicController.disconnect();
			parent.dispose();
		}
		

		//********************************************************************************************************************
		@Override
		public void keyReleased(KeyEvent arg0) {
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			
		}

		@Override
		public void windowActivated(WindowEvent arg0) {
			
		}

		@Override
		public void windowClosed(WindowEvent arg0) {
			
		}

		@Override
		public void windowDeactivated(WindowEvent arg0) {
			
		}

		@Override
		public void windowDeiconified(WindowEvent arg0) {
			
		}

		@Override
		public void windowIconified(WindowEvent arg0) {
			
		}

		@Override
		public void windowOpened(WindowEvent arg0) {
			
		}

	}
}
