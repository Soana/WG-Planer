package application.client.gui.foreground;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import application.client.gui.background.BackgroundWindow;
import application.client.logik.DataObject;
import application.client.logik.Mitbewohnerverwaltung;

public class MitbewohnerListePanel extends FunctionPanel {

	private JTable tblData;
	
	private JButton btnUbersicht;
	
	private DataObject data;
	
	public MitbewohnerListePanel(BackgroundWindow parent, DataObject dobj) {
		super(parent);

		this.data = dobj;
		
		this.setLayout(new BorderLayout());
		setupGui();
	}

	private void setupGui(){
		btnUbersicht = new JButton("Übersicht erstellen");
		btnUbersicht.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent evt) {
				data.mitbewohnerverwaltung.erstelleBewohnerUbersicht();
			}
			
		});
		
		String[] columnHeaders = {"Nachname", "Vorname", "Geburtsdatum", "Telefon"};
		
		UnchangeableTableModel modelData = new UnchangeableTableModel(columnHeaders, 0);
		Object[][] bewohner = data.mitbewohnerverwaltung.getBewohnerAsObjects();
		for(int i = 0; i < bewohner.length; i++){
			modelData.addRow(bewohner[i]);
		}
		tblData = new JTable(modelData);
		
		JScrollPane scrollPane = new JScrollPane(tblData);
		tblData.setFillsViewportHeight(true);
		
		System.out.println(getXSize() + " " + getYSize());
		scrollPane.setSize(getXSize(), getYSize());
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		this.add(scrollPane, BorderLayout.CENTER);
		this.add(btnUbersicht, BorderLayout.SOUTH);
		this.validate();
	}
	
	@Override
	public String getName(){
		return "Mitbewohnerliste";
	}
	
	@Override
	public int getXSize(){
		return (int) Math.ceil(tblData.getPreferredSize().getWidth());
	}
	
	@Override
	public int getYSize(){
		return (int) (Math.ceil(tblData.getPreferredSize().getHeight())*2 + btnUbersicht.getPreferredSize().getHeight());
	}
	
	@Override
	public String getBase() {
		return "mitbewohner";
	}
}
