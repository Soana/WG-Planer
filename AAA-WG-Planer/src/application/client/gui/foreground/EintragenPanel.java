package application.client.gui.foreground;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.*;

import application.client.gui.background.BackgroundWindow;
import application.client.logik.Bewohner;
import application.client.logik.DataObject;
import application.client.logik.Einkaufsliste;
import application.client.logik.LogicController;
import application.client.logik.Mitbewohnerverwaltung;
import application.shared.data.Rechnung;


public class EintragenPanel extends FunctionPanel {
	private EintragenListener listener;
	
	private JButton btnListe;
	
	private JTable tblRechnungen;
	private UnchangeableTableModel modelRechnungen;
	
	private JLabel lblKommentar;
	private JLabel lblBetrag;
	private JTextField txtKommentar;
	private JTextField txtBetrag;
	private JButton btnEintragen;
	private JLabel lblForRechts;
	
	private JCheckBox[] cbxsFor;
	private JTextField[] txtsGuest;
	
	private JButton btnPlus;
	private JButton btnMinus;
	private JButton btnMal;
	private JButton btnDurch;
	private JButton btnKomma;
	private JButton btnKlammerAuf;
	private JButton btnKlammerZu;
	private JButton btnGleich;
	private JButton btnClear;
	private JButton btnClearAll;
	private JButton[] btnsZahl;
	
	private JLabel lblPosten;
	private JLabel lblGesamt;

	private DataObject data;
	
	public EintragenPanel(BackgroundWindow parent, DataObject dobj) {
		super(parent);

		listener = new EintragenListener(this);

		this.data = dobj;
		
		setupGui();
	}
	
	private void setupGui(){
		Rechnung[] rechnungen = data.einkaufsliste.getRechnungen();
		Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
		
		btnListe = new JButton("Liste laden");
		
		modelRechnungen = new UnchangeableTableModel(new Object[]{"Von", "Für", "Kommentar", "Betrag [€]"}, rechnungen.length);
		for(int i = 0; i < rechnungen.length; i++){
			Bewohner[] fuer = rechnungen[i].getFuer();
			StringBuilder sb = new StringBuilder("");
			for(int j = 0; j < fuer.length; j++){
				sb.append(fuer[j].getKurz() + ", ");
			}
			sb.delete(sb.length() - 2, sb.length());
			modelRechnungen.addRow(new Object[]{rechnungen[i].getVon().getKurz(), sb.toString(), rechnungen[i].getWas(), rechnungen[i].getWieviel()/100.0});
		}
		tblRechnungen = new JTable(modelRechnungen);
		
		lblKommentar = new JLabel("Kommentar:");
		lblBetrag = new JLabel("Betrag:");
		txtKommentar = new JTextField();
		txtBetrag = new JTextField();
		lblForRechts = new JLabel("Für:");
		btnEintragen = new JButton("Eintragen");
		
		cbxsFor = new JCheckBox[bewohner.length];
		txtsGuest = new JTextField[bewohner.length];
		for(int i = 0; i < cbxsFor.length; i++){
			cbxsFor[i] = new JCheckBox(bewohner[i].getKurz());
			txtsGuest[i] = new JTextField();
		}
		
		btnPlus = new JButton("+");
		btnMinus = new JButton("-");
		btnMal = new JButton("*");
		btnDurch = new JButton("/");
		btnKomma = new JButton(".");
		btnKlammerAuf = new JButton("(");
		btnKlammerZu = new JButton(")");
		btnGleich = new JButton("=");
		btnClear = new JButton("CE");
		btnClearAll = new JButton("C");
		btnsZahl = new JButton[10];
		for(int i = 0; i < 10; i++){
			btnsZahl[i] = new JButton(Integer.toString(i));
		}
		
		btnPlus.setBackground(Color.GREEN);
		btnMinus.setBackground(Color.GREEN);
		btnMal.setBackground(Color.GREEN);
		btnDurch.setBackground(Color.GREEN);
		btnKomma.setBackground(Color.GREEN);
		btnKlammerAuf.setBackground(Color.GREEN);
		btnKlammerZu.setBackground(Color.GREEN);
		btnGleich.setBackground(Color.ORANGE);
		btnClear.setBackground(Color.RED);
		btnClearAll.setBackground(Color.RED);
		
		lblPosten = new JLabel("<html>");//<html>&nbsp;&nbsp;Posten1<br>+ Posten2");
		lblGesamt = new JLabel("0.00");
		
		lblPosten.setFont(new Font(Font.MONOSPACED, Font.PLAIN, lblPosten.getFont().getSize()));
		lblGesamt.setFont(new Font(Font.MONOSPACED, Font.PLAIN, lblPosten.getFont().getSize()));
		
		btnListe.addActionListener(listener);
		btnEintragen.addActionListener(listener);
		btnPlus.addActionListener(listener);
		btnMinus.addActionListener(listener);
		btnMal.addActionListener(listener);
		btnDurch.addActionListener(listener);
		btnKomma.addActionListener(listener);
		btnKlammerAuf.addActionListener(listener);
		btnKlammerZu.addActionListener(listener);
		btnGleich.addActionListener(listener);
		btnClear.addActionListener(listener);
		btnClearAll.addActionListener(listener);
		for(int i = 0; i < btnsZahl.length; i++){
			btnsZahl[i].addActionListener(listener);
		}
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		JPanel pnlLower = new JPanel();
		pnlLower.setLayout(new BoxLayout(pnlLower, BoxLayout.LINE_AXIS));
		pnlLower.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		
		JScrollPane pnlRechnungen = new JScrollPane();
		pnlRechnungen.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		pnlRechnungen.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pnlRechnungen.setViewportView(tblRechnungen);
		pnlLower.add(pnlRechnungen);
		
		JPanel pnlRight = new JPanel();
		pnlRight.setLayout(new BoxLayout(pnlRight, BoxLayout.PAGE_AXIS));
		pnlRight.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		pnlLower.add(pnlRight);
		
		JPanel pnlEintragen = new JPanel();
		pnlEintragen.setLayout(new BoxLayout(pnlEintragen, BoxLayout.PAGE_AXIS));
		JPanel pnlBetrag = new JPanel();
		pnlBetrag.setLayout(new FlowLayout(FlowLayout.LEADING, hgap, vgap));
		pnlBetrag.add(lblBetrag);
		pnlBetrag.add(txtBetrag);
		pnlBetrag.add(btnEintragen);
		JPanel pnlWas = new JPanel();
		pnlWas.setLayout(new FlowLayout(FlowLayout.LEADING, hgap, vgap));
		pnlWas.add(lblKommentar);
		pnlWas.add(txtKommentar);
		JPanel pnlFor = new JPanel();
		pnlFor.setLayout(new GridLayout(cbxsFor.length, 2, hgap, vgap));
		for(int i = 0; i < cbxsFor.length; i++){
			pnlFor.add(cbxsFor[i]);
			pnlFor.add(txtsGuest[i]);
		}
		pnlEintragen.add(pnlBetrag);
		pnlEintragen.add(pnlWas);
		pnlEintragen.add(lblForRechts);
		pnlEintragen.add(pnlFor);
		pnlRight.add(pnlEintragen);
		
		JPanel pnlRechner = new JPanel();
		pnlRechner.setLayout(new BoxLayout(pnlRechner, BoxLayout.LINE_AXIS));
		pnlRechner.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		pnlRight.add(pnlRechner);
		
		JPanel pnlTasten = new JPanel();
		pnlTasten.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		pnlTasten.setLayout(new GridLayout(5, 4, hgap, vgap));
		pnlTasten.add(btnClearAll);
		pnlTasten.add(btnKlammerAuf);
		pnlTasten.add(btnKlammerZu);
		pnlTasten.add(btnDurch);
		pnlTasten.add(btnsZahl[7]);
		pnlTasten.add(btnsZahl[8]);
		pnlTasten.add(btnsZahl[9]);
		pnlTasten.add(btnMal);
		pnlTasten.add(btnsZahl[4]);
		pnlTasten.add(btnsZahl[5]);
		pnlTasten.add(btnsZahl[6]);
		pnlTasten.add(btnMinus);
		pnlTasten.add(btnsZahl[1]);
		pnlTasten.add(btnsZahl[2]);
		pnlTasten.add(btnsZahl[3]);
		pnlTasten.add(btnPlus);
		pnlTasten.add(btnClear);
		pnlTasten.add(btnsZahl[0]);
		pnlTasten.add(btnKomma);
		pnlTasten.add(btnGleich);
		pnlRechner.add(pnlTasten);
		
		JPanel pnlAnzeige = new JPanel();
		pnlAnzeige.setLayout(new BorderLayout(hgap, vgap));
		pnlAnzeige.add(lblPosten, BorderLayout.CENTER);
		pnlAnzeige.add(lblGesamt, BorderLayout.SOUTH);
		pnlRechner.add(pnlAnzeige);
		
		this.add(btnListe);
		this.add(pnlLower);
		this.validate();
	}

	@Override
	public String getName() {
		return "Einkauf eintragen";
	}

	@Override
	public String getBase() {
		return "eintragen";
	}
	
	private class EintragenListener extends SubPanelListener implements ActionListener{
		
		private EintragenPanel listenTo;
		
		public EintragenListener(EintragenPanel listento){
			listenTo = listento;
		}
		
		int tab = 0;
		boolean closed = false;
		boolean komma = false;
		int stellen = 0;
		
		private void addText(String text){
			lblPosten.setText(lblPosten.getText() + text);
		}
		
		private void addText(char text){
			addText(Character.toString(text));
		}
		
		private int getErgebnis() throws ArithmeticException{
			StringBuilder sb = new StringBuilder(lblPosten.getText());
//			System.out.println("cin bevor: " + sb);
			sb.delete(0, 6);
			while(sb.indexOf("<br>") != -1){
				sb.delete(sb.indexOf("<br>"), sb.indexOf("<br>") + 4);
			}
			while(sb.indexOf(" ") != -1){
				sb.deleteCharAt(sb.indexOf(" "));
			}
			while(sb.indexOf("&nbsp;") != -1){
				sb.delete(sb.indexOf("&nbsp;"), sb.indexOf("&nbsp;") + 6);
			}
			while(sb.indexOf(".") != -1){
				sb.deleteCharAt(sb.indexOf("."));
			}
			System.out.println("cin danach: " + sb);
			cin = sb.toString();
			int ergebnis = bewerteAusdruck();
			reset();
			return ergebnis;
		}
		
		char lastSign = '\0';
		private void addZeichen(char zeichen){
			System.out.println("Add Zeichen: " + zeichen);
			if(!closed){
				switch(zeichen){
					case '(':
						tab++;
						addText(zeichen);
						break;
					case ')':
						fill();
						tab--;
						addText(zeichen);
						break;
					case '=':
						break;
					case '+':
					case '-':
						fill();
					case '*':
					case '/':
						System.out.println("Ergebnis: " + getErgebnis());
						setGesamt(Double.toString(getErgebnis()/100.0));
						
						//einruecken bei Klammern
						StringBuilder sb = new StringBuilder("<br>");
						for(int i = 0; i < tab; i++){
							sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
						}
						sb.append(zeichen + " ");
						addText(sb.toString());
						break;
					default:
						addText(zeichen);
				}
			}
		}
		
		private void fill(){
			if(lblPosten.getText().charAt(lblPosten.getText().length()-1) == ')'){
				return;
			}
			if(stellen == 0){
				addText(".00");
			}
			else if(stellen == 1){
				addText("0");
			}
			else if(stellen > 2){
				for(int i = stellen; i > 2; i--){
					btnClear.doClick();
				}
			}
			komma = false;
			stellen = 0;
		}
		
		private void setGesamt(String gesamt){
			lblGesamt.setText(gesamt);
			txtBetrag.setText(gesamt);
		}
		
		@Override
		public void actionPerformed(ActionEvent evt){
			if(btnListe.equals(evt.getSource())){
				boolean erfolg = data.einkaufsliste.stelleRechnung();
				if(erfolg){
					while(modelRechnungen.getRowCount() != 0){
						modelRechnungen.removeRow(modelRechnungen.getRowCount()-1);
					}
					for(Rechnung r: data.einkaufsliste.getRechnungen()){
						Bewohner[] fuer = r.getFuer();
						StringBuilder sb = new StringBuilder("");
						for(int j = 0; j < fuer.length; j++){
							sb.append(fuer[j].getKurz() + ", ");
						}
						sb.delete(sb.length() - 2, sb.length());
						modelRechnungen.addRow(new Object[]{r.getVon().getKurz(), sb.toString(), r.getWas(), r.getWieviel()/100.0});
					}
					modelRechnungen.fireTableDataChanged();
					JOptionPane.showMessageDialog(listenTo, "Einkaufsliste geladen", "Erfolg", JOptionPane.INFORMATION_MESSAGE);
				}
			}
			else if(btnEintragen.equals(evt.getSource())){
				Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
				HashMap<Bewohner, Integer> b = new HashMap<Bewohner, Integer>();
				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < cbxsFor.length; i++){
					if(cbxsFor[i].isSelected()){
						for(int j = 0; j < bewohner.length; j++){
							if(bewohner[j].getKurz().equals(cbxsFor[i].getText())){
								b.put(bewohner[j], Integer.parseInt(txtsGuest[i].getText()));
								sb.append(bewohner[j] + ", ");
							}
						}
					}
				}
				sb.delete(sb.length() - 2, sb.length());
				modelRechnungen.addRow(new Object[]{data.mitbewohnerverwaltung.getCurrent(), sb.toString(), txtKommentar.getText(), Double.parseDouble(txtBetrag.getText())});
				data.einkaufsliste.rechnungStellen(txtKommentar.getText(),(int) Double.parseDouble(txtBetrag.getText()) * 100, b);
			}
			else if(btnPlus.equals(evt.getSource())){
				addZeichen('+');
			}
			else if(btnMinus.equals(evt.getSource())){
				addZeichen('-');
			}
			else if(btnMal.equals(evt.getSource())){
				addZeichen('*');
			}
			else if(btnDurch.equals(evt.getSource())){
				addZeichen('/');
			}
			else if(btnKomma.equals(evt.getSource())){
				addText('.');
				komma = true;
			}
			else if(btnKlammerAuf.equals(evt.getSource())){
				addZeichen('(');
			}
			else if(btnKlammerZu.equals(evt.getSource())){
				addZeichen(')');
			}
			else if(btnGleich.equals(evt.getSource())){
				fill();
				setGesamt(Double.toString(getErgebnis()/100.0));
				closed = true;
			}
			else if(btnClearAll.equals(evt.getSource())){
				lblPosten.setText("<html>");
				setGesamt("0.00");
				reset();
				closed = false;
			}
			else if(btnClear.equals(evt.getSource())){
				String text = lblPosten.getText();
				if(Character.isDigit(text.charAt(text.length() - 1))){
					text = (new StringBuilder(text)).deleteCharAt(text.length() - 1).toString();
				}
				else{
					text = (new StringBuilder(text)).delete(text.length() - 6, text.length()).toString();
				}
				lblPosten.setText(text);
				if(text.length() == 0){
					closed = false;
				}
			}
			else{ 
				if(!closed){
					for(int i = 0; i < 10; i ++){
						if(btnsZahl[i].equals(evt.getSource())){
							if(komma){
								stellen ++;
							}
							addText(Integer.toString(i));
						}
					}
				}
			}
		}
	
	
		String cin = null;
		int next = 0;
		
		void reset(){
			cin = null;
			next = 0;
		}
		
		char peek(){
			if(! ende()){
				return cin.charAt(next);
			}
			else{
				return 4;
			}
		}
		
		boolean ende(){
			return next >= cin.length();
		}
		
		char get(){
			return cin.charAt(next++);
		}
		
		int bewerteAusdruck(){
			int summ = bewerteSummand();
			char op = peek();
			if(ende() && (op == '-' || op == '+')){
				System.out.print("Ungueltiger Ausdruck!\n");
				System.exit(1);
			}
			
			switch(op){
				case '+': get(); summ += bewerteSummand(); if(! ende()){summ += bewerteAusdruck();}break;
				case '-': get(); summ -= bewerteSummand(); if(! ende()){summ += bewerteAusdruck();}break;
			}
	
			return summ;
		}
	
		int bewerteSummand(){
			int fak = bewerteFaktor();
			char op = peek();
			if(ende() && (op == '*' || op == '/')){
				System.out.print("Ungueltiger Summand!\n");
				System.exit(1);
			}
	//		op = peek();
			switch(op){
				case '*': get(); fak *= bewerteFaktor(); break;
				case '/': get(); fak /= bewerteFaktor(); break;
			}
			return fak;
		}
	
		int bewerteFaktor(){
			boolean minus = false;
			int z;
			char v = peek();
			// theoretisch ist hier eine Endlosschleife von Vorzeichen moeglich, daher nehme ich immer das letzte
			while(v == '+' || v == '-'){
				minus = (v == '-');
				get();
				v = peek();
			}
			if(v == '('){
				get();
				z = bewerteAusdruck();
				v = get();
				if(v != ')'){
					System.out.print("Ungueltiger Faktor!\n");
					System.exit(1);
				}
			}
			else{
				z = bewerteZahl();
			}
			if(minus){
				z = -z;
			}
			return z;
		}
	
		int bewerteZahl(){
			int z = 0;
			char c = peek();
			while(c <= '9' && c >= '0'){
				get();
				z += c-'0';
				z *= 10;
				c = peek();
	//			if(c == 4){
	//				break;
	//			}
			}
			z /= 10;
			return z;
		}
	}
}
