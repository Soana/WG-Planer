package application.client.gui.foreground;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import application.client.gui.background.BackgroundWindow;
import application.client.logik.Bewohner;
import application.client.logik.DataObject;
import application.client.logik.Mitbewohnerverwaltung;
import application.shared.Aufgabenverwaltung;
import application.shared.data.Aufgabe;


public class EinstellungenPanel extends FunctionPanel {

	private EinstellungenLister listener;
	
	private JButton btnDelete;
	
	private UnchangeableTableModel modelAufgaben;
	private JTable tblAufgaben;
	
	private JLabel lblName;
	private JLabel lblAnzahl;
	private JTextField txtName;
	private JTextField txtAnzahl;
	private JCheckBox[] cbxsTage;
	private JButton btnNew;
	
	private DataObject data;
	
	public EinstellungenPanel(BackgroundWindow parent, DataObject dobj) {
		super(parent);

		listener = new EinstellungenLister(this);

		this.data = dobj;
		
		setupGui();
	}
	
	private void setupGui(){
		btnDelete = new JButton("Löschen");
		btnDelete.addActionListener(listener);
		
		Aufgabe[] aufgaben = data.aufgabenverwaltung.getAufgaben();
		modelAufgaben = new UnchangeableTableModel(new Object[]{"Name", "Tage"}, aufgaben.length);
		for(int i = 0; i < aufgaben.length; i++){
			modelAufgaben.addRow(new Object[]{aufgaben[i].getName(), aufgaben[i].getTageAsString()});
		}
		tblAufgaben = new JTable(modelAufgaben);
		
		lblName = new JLabel("Name:");
		txtName = new JTextField();
		lblAnzahl = new JLabel("Anzahl:");
		txtAnzahl = new JTextField("1");
		cbxsTage = new JCheckBox[7];
		String[] tage = {"Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"};
		for(int i = 0; i < cbxsTage.length; i++){
			cbxsTage[i] = new JCheckBox(tage[i]);
		}
		btnNew = new JButton("Erstellen");
		btnNew.addActionListener(listener);
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		this.add(btnDelete);
		
		JScrollPane pnlAufgaben = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pnlAufgaben.setViewportView(tblAufgaben);
		this.add(pnlAufgaben);
		
		JPanel pnlNew = new JPanel();
		pnlNew.setLayout(new BoxLayout(pnlNew, BoxLayout.LINE_AXIS));
		pnlNew.add(lblName);
		pnlNew.add(txtName);
		pnlNew.add(lblAnzahl);
		pnlNew.add(txtAnzahl);
		for(int i = 0; i < cbxsTage.length; i++){
			pnlNew.add(cbxsTage[i]);
		}
		pnlNew.add(btnNew);
		this.add(pnlNew);
		this.validate();
	}
	
	@Override
	public String getName() {
		return "WG-Einstellungen";
	}

	@Override
	public String getBase() {
		return "einstellugen";
	}
	
	private class EinstellungenLister extends SubPanelListener implements ActionListener{

		private EinstellungenPanel listenTo;
		
		public EinstellungenLister(EinstellungenPanel listento){
			listenTo = listento;
		}
		
		@Override
		public void actionPerformed(ActionEvent evt) {
			if(evt.getSource().equals(btnDelete)){
				System.out.println("löschen");
				Aufgabe[] aufgaben = data.aufgabenverwaltung.getAufgaben();
				int index = tblAufgaben.getSelectedRow();
				if(index != -1){
					System.out.println("\thardcore");
					for(int i = 0; i < aufgaben.length; i++){
						if(aufgaben[i].getName().equals(modelAufgaben.getValueAt(index, 0))){
							System.out.println("\t\tfound");
							int confirmed = JOptionPane.showConfirmDialog(listenTo, 
									"Bist du sicher, dass du die Aufgabe \"" + aufgaben[i].getName() + "\" löschen willst?", 
									"Aufgabe löschen", 
									JOptionPane.YES_NO_OPTION); 
							
							if(confirmed == JOptionPane.YES_OPTION){
								data.aufgabenverwaltung.loeschen(aufgaben[i]);
								modelAufgaben.removeRow(index);
								modelAufgaben.fireTableRowsDeleted(index, index);
							}
						}
						break;
					}
				}
			}
			else if(evt.getSource().equals(btnNew)){
				System.out.println("erstellen");
				boolean[] tage = new boolean[7];
				for(int i = 0; i < cbxsTage.length; i++){
					tage[i] = cbxsTage[i].isSelected();
				}
				modelAufgaben.addRow(new Object[]{txtName.getText(), Aufgabe.getTageAsString(tage)});
				data.aufgabenverwaltung.add(txtName.getText(), tage, Integer.parseInt(txtAnzahl.getText()));
			}
		}
		
	}

}
