package application.client.gui.foreground;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;


public class UnchangeableTableModel extends DefaultTableModel {
	
	public UnchangeableTableModel(Object[] columnNames, int rowCount){
		super(columnNames, rowCount);
	}
	
    public boolean isCellEditable(int row, int col){
    	return false;
    }
    
    public void setValueAt(Object value, int row, int col) {
        
    }

}
