package application.client.gui.foreground;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;

import application.client.gui.background.BackgroundWindow;
import application.client.logik.Bewohner;
import application.client.logik.DataObject;
import application.client.logik.Mitbewohnerverwaltung;


public class MitbewohnerVerwaltenPanel extends FunctionPanel {

	private MitbewohnerVerwaltenListener listener;
	
	private UnchangeableTableModel modelBewohner;
	private JTable tblBewohner;
	
	private JLabel lblVorname;
	private JLabel lblNachname;
	private JTextField txtVorname;
	private JTextField txtNachname;
	private JButton btnNew;
	private JButton btnDelete;
	
	private DataObject data;
	
	public MitbewohnerVerwaltenPanel(BackgroundWindow parent, DataObject dobj) {
		super(parent);
		
		listener = new MitbewohnerVerwaltenListener(this);

		this.data = dobj;
		
		setupGui();
	}
	
	private void setupGui(){
		Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
		
		modelBewohner = new UnchangeableTableModel(new Object[]{"Name", "Benutzername"}, bewohner.length);
		for(int i = 0; i < bewohner.length; i++){
			modelBewohner.addRow(new Object[]{bewohner[i].getName(), bewohner[i].getBenutzername()});
		}
		tblBewohner = new JTable(modelBewohner);
		tblBewohner.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		lblVorname = new JLabel("Vorname:");
		lblNachname = new JLabel("Nachname:");
		txtVorname = new JTextField();
		txtNachname = new JTextField();
		
		btnDelete = new JButton("Löschen");
		btnNew = new JButton("Neu");
		
		btnDelete.addActionListener(listener);
		btnNew.addActionListener(listener);
		
		JScrollPane pnlBewohner = new JScrollPane();
		pnlBewohner.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		pnlBewohner.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pnlBewohner.setViewportView(tblBewohner);
		
		JPanel pnlButton = new JPanel();
		pnlButton.setLayout(new BorderLayout(hgap, vgap));
		pnlButton.add(btnDelete, BorderLayout.SOUTH);
		JPanel pnlNew = new JPanel();
		pnlNew.setLayout(new GridLayout(3, 2, hgap, vgap));
		pnlNew.add(lblVorname);
		pnlNew.add(txtVorname);
		pnlNew.add(lblNachname);
		pnlNew.add(txtNachname);
		pnlNew.add(new JLabel());
		pnlNew.add(btnNew);
		pnlButton.add(pnlNew, BorderLayout.NORTH);
		
		this.setLayout(new BorderLayout(hgap, vgap));
		this.add(pnlBewohner, BorderLayout.CENTER);
		this.add(pnlButton, BorderLayout.EAST);
	}
	
	@Override
	public String getName() {
		return "Mitbewohner verwalten";
	}

	@Override
	public String getBase() {
		return "verwalten";
	}
	
	private class MitbewohnerVerwaltenListener extends SubPanelListener implements ActionListener{

		private MitbewohnerVerwaltenPanel listenTo;
		
		public MitbewohnerVerwaltenListener(MitbewohnerVerwaltenPanel listento){
			listenTo = listento;
		}
		
		@Override
		public void actionPerformed(ActionEvent evt) {
			if(evt.getSource().equals(btnDelete)){
				Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
				int index = tblBewohner.getSelectedRow();
				if(index != -1){
					for(int i = 0; i < bewohner.length; i++){
						if(bewohner[i].getBenutzername().equals(modelBewohner.getValueAt(index, 1))){
							if(bewohner[i].getGuthaben() == 0){
								int confirmed = JOptionPane.showConfirmDialog(listenTo, 
										"Bist du sicher, dass du \"" + bewohner[i].getName() + "\" löschen willst?", 
										"Bewohner löschen", 
										JOptionPane.YES_NO_OPTION); 
								
								if(confirmed == JOptionPane.YES_OPTION){
									data.mitbewohnerverwaltung.loeschen(bewohner[i]);
									modelBewohner.removeRow(index);
									modelBewohner.fireTableRowsDeleted(index, index);
								}
							}
							else{
								JOptionPane.showMessageDialog(listenTo, bewohner[i].getName() + "\" hat einen Kontostand der ungleich 0 ist. Gleicht das aus, bevor der Bewohner gelöscht werden kann.", "Bewohner kann nicht gelöscht werden", JOptionPane.ERROR_MESSAGE);
							}
							break;
						}
					}
				}
				
			}
			else if(evt.getSource().equals(btnNew)){
				String benutzername = data.mitbewohnerverwaltung.add(txtVorname.getText(), txtNachname.getText());
				modelBewohner.addRow(new String[]{txtVorname.getText() + " " + txtNachname.getText(), benutzername});
				if(modelBewohner.getRowCount() > 5 && modelBewohner.getRowCount()% 5 == 1){
					JPanel pnl = new JPanel();
					DefaultListModel<Bewohner> model = new DefaultListModel<>();
//					for(Bewohner b: data.mitbewohnerverwaltung.getBewohner()){
//						if(! b.isAdmin()){
//							model.addElement(b);
//						}
//					}
					JList<Bewohner> lst = new JList<>(model);
					pnl.add(lst);
					JOptionPane.showMessageDialog(listenTo, lst, "Wähle einen Admin", JOptionPane.QUESTION_MESSAGE);
					model.get(lst.getSelectedIndex()).makeAdmin();

				}
			}
		}
		
	}

}
