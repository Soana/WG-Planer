package application.client.gui.foreground;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import application.client.gui.SubPanel;
import application.client.gui.background.BackgroundWindow;


public abstract class FunctionPanel extends SubPanel {
	
	private enum State{
		OPEN,
		CLOSED,
		MAXIMISED,
		ICONIFYSED
	}
	
	public String base = "general";
	
	private static Properties config;
	
	public FunctionPanel(BackgroundWindow parent) {
		super(parent);

		if(config == null){
			config = new Properties();
		
			try {
				config.load(FunctionPanel.class.getClassLoader().getResourceAsStream("data/function.config"));
			}
			catch (IOException e) {
				System.out.println("data/function.config nicht gefunden");
			}
		}
	}

	public abstract String getName();
	public abstract String getBase();
	
	public void saveState(){
		config.setProperty(getBase() + ".x", Integer.toString(getXPos()));
		config.setProperty(getBase() + ".y", Integer.toString(getYPos()));
		
		config.setProperty(getBase() + ".width", Integer.toString(getXSize()));
		config.setProperty(getBase() + ".height", Integer.toString(getYSize()));
	}
	
	public boolean getResizeable() {
		return true;
	}

	public boolean getCloseable() {
		return true;
	}

	public boolean getMaximizable() {
		return true;
	}

	public boolean getIconifyable() {
		return true;
	}

	public int getXPos() {
		int x = 0;
		if(config.getProperty(getBase() + ".x") == null){
			config.setProperty(getBase() + ".x", Integer.toString(x));
		}
		else{
			x = Integer.parseInt(config.getProperty(getBase() + ".x"));
		}
		return x;
	}

	public int getYPos() {
		int y = 0;
		if(config.getProperty(getBase() + ".y") == null){
			config.setProperty(getBase() + ".y", Integer.toString(y));
		}
		else{
			y = Integer.parseInt(config.getProperty(getBase() + ".y"));
		}
		return y;
	}
	
	public void save(){
		try {
			config.store(new FileWriter(new File("data/function.config")), null);
		}
		catch (IOException e) {
			System.err.println("Feler beim oeffnen von ./data/function.config");
			e.printStackTrace();
		}
	}

	public int getXSize() {
		int x = this.getWidth();
		if(config.getProperty(getBase() + ".width") == null){
			config.setProperty(getBase() + ".width", Integer.toString(this.getWidth()));
		}
		else{
			x = Integer.parseInt(config.getProperty(getBase() + ".width"));
		}
		return x;
	}

	public int getYSize() {
		int y = this.getHeight();
		if(config.getProperty(getBase() + ".height") == null){
			config.setProperty(getBase() + ".height", Integer.toString(this.getHeight()));
		}
		else{
			y = Integer.parseInt(config.getProperty(getBase() + ".height"));
		}
		return y;
	}
}
