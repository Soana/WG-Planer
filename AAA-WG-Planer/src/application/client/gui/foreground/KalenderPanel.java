package application.client.gui.foreground;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import application.client.gui.background.BackgroundWindow;
import application.client.logik.Bewohner;
import application.client.logik.DataObject;
import application.client.logik.Kalender;
import application.client.logik.Mitbewohnerverwaltung;
import application.shared.data.Termin;


public class KalenderPanel extends FunctionPanel {

	private KalenderListener listener;
	
	private JButton btnDelete;
	
	private UnchangeableTableModel modelListe;
	private JTable tblListe;
	private UnchangeableTableModel modelKalender;
	private JTable tblKalender;
	private JButton btnPrevious;
	private JButton btnNext;
	private JLabel lblMonat;
	
	private JLabel lblName;
	private JLabel lblAnfangDatum;
	private JLabel lblEndeDatum;
	private JLabel lblAnfangUhrzeit;
	private JLabel lblEndeUhrzeit;
	private JTextField txtName;
	private JTextField txtAnfangDatum;
	private JTextField txtEndeDatum;
	private JTextField txtAnfangUhrzeit;
	private JTextField txtEndeUhrzeit;
	private JCheckBox cbxGanzTags;
	private JLabel lblTeilnehmer;
	private JLabel lblSichtbar;
	private JCheckBox[] cbxsTeilnehmer;
	private JCheckBox[] cbxsSichtbar;
	private JButton btnNew;
	
	private GregorianCalendar today;
	private int tag;
	private int monat;
	private int jahr;
	private DataObject data;
	
	public KalenderPanel(BackgroundWindow parent, DataObject dobj) {
		super(parent);

		listener = new KalenderListener(this);

		this.data = dobj;
		
		setupGui();
	}

	private void setupGui(){
		btnDelete = new JButton("Löschen");
		
		btnDelete.addActionListener(listener);
		
		Calendar calendar = GregorianCalendar.getInstance();
		today = (GregorianCalendar) calendar.clone();
		tag = calendar.get(Calendar.DAY_OF_MONTH);
		monat = calendar.get(Calendar.MONTH);
		jahr = calendar.get(Calendar.YEAR);
		
		Termin[] termine = data.kalender.getTermine(tag, monat, jahr);
		modelListe = new UnchangeableTableModel(new Object[]{"Name", "Von", "Bis"}, 0);
		for(int i = 0; i < termine.length; i++){
				String von = termine[i].getVon().get(Calendar.HOUR_OF_DAY) + ":" + termine[i].getVon().get(Calendar.MINUTE);
				String bis = termine[i].getBis().get(Calendar.HOUR_OF_DAY) + ":" + termine[i].getBis().get(Calendar.MINUTE);
				modelListe.addRow(new Object[]{termine[i].getName(), von, bis});
		}
		
		tblListe = new JTable(modelListe);
		lblMonat = new JLabel(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
		modelKalender = new UnchangeableTableModel(new Object[]{"Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"}, 0);
		int[] r = makeKalender();
		
		tblKalender = new JTable(modelKalender);
		tblKalender.setCellSelectionEnabled(true);
		tblKalender.getSelectionModel().addListSelectionListener(listener);
		tblKalender.changeSelection(r[0], r[1], false, false);
		btnPrevious = new JButton("<<");
		btnNext = new JButton(">>");
		
		btnPrevious.addActionListener(listener);
		btnNext.addActionListener(listener);
		
		lblName = new JLabel("Name:");
		lblAnfangDatum = new JLabel("Datum von:");
		lblEndeDatum = new JLabel("Datum bis:");
		lblAnfangUhrzeit = new JLabel("Uhrzeit von:");
		lblEndeUhrzeit = new JLabel("Uhrzeit bis:");
		txtName = new JTextField();
		txtAnfangDatum = new JTextField();
		txtEndeDatum = new JTextField();
		txtAnfangUhrzeit = new JTextField();
		txtEndeUhrzeit = new JTextField();
		cbxGanzTags = new JCheckBox("ganztägig");
		lblTeilnehmer = new JLabel("Teilnehmer:");
		Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
		cbxsTeilnehmer = new JCheckBox[bewohner.length];
		for(int i = 0; i < cbxsTeilnehmer.length; i++){
			cbxsTeilnehmer[i] = new JCheckBox(bewohner[i].getName());
		}
		lblSichtbar = new JLabel("Sichtbar für:");
		cbxsSichtbar = new JCheckBox[bewohner.length];
		for(int i = 0; i < cbxsSichtbar.length; i++){
			cbxsSichtbar[i] = new JCheckBox(bewohner[i].getName());
			cbxsSichtbar[i].setSelected(true);
		}
		
		btnNew = new JButton("Erstellen");
		
		btnNew.addActionListener(listener);
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		this.add(btnDelete);
		JPanel pnlUpper = new JPanel();
		pnlUpper.setLayout(new BoxLayout(pnlUpper, BoxLayout.LINE_AXIS));
		
		JScrollPane pnlList = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pnlList.setViewportView(tblListe);
		pnlUpper.add(pnlList);
		
		JPanel pnlKalender = new JPanel();
		pnlKalender.setLayout(new BoxLayout(pnlKalender, BoxLayout.PAGE_AXIS));
		pnlKalender.add(tblKalender);
		
		JPanel pnlDeco = new JPanel();
		pnlDeco.setLayout(new BorderLayout(hgap, vgap));
		pnlDeco.add(btnPrevious, BorderLayout.WEST);
		pnlDeco.add(lblMonat, BorderLayout.CENTER);
		pnlDeco.add(btnNext, BorderLayout.EAST);
		pnlKalender.add(pnlDeco);
		pnlUpper.add(pnlKalender);
		this.add(pnlUpper);
		
		JPanel pnlLower = new JPanel();
		pnlLower.setLayout(new BoxLayout(pnlLower, BoxLayout.LINE_AXIS));
		
		JPanel pnlText = new JPanel();
		pnlText.setLayout(new GridLayout(6, 2, hgap, vgap));
		pnlText.add(lblName);
		pnlText.add(txtName);
		pnlText.add(lblAnfangDatum);
		pnlText.add(lblAnfangUhrzeit);
		pnlText.add(txtAnfangDatum);
		pnlText.add(txtAnfangUhrzeit);
		pnlText.add(lblEndeDatum);
		pnlText.add(lblEndeUhrzeit);
		pnlText.add(txtEndeDatum);
		pnlText.add(txtEndeUhrzeit);
		pnlText.add(cbxGanzTags);
		pnlText.add(btnNew);
		pnlLower.add(pnlText);
		
		JPanel pnlLeute = new JPanel();
		pnlLeute.setLayout(new GridLayout(1, 2, hgap, vgap));
		JPanel pnlTeilnehmer = new JPanel();
		pnlTeilnehmer.setLayout(new BoxLayout(pnlTeilnehmer, BoxLayout.PAGE_AXIS));
		pnlTeilnehmer.add(lblTeilnehmer);
		for(int i = 0; i < cbxsTeilnehmer.length; i++){
			pnlTeilnehmer.add(cbxsTeilnehmer[i]);
		}
		pnlLeute.add(pnlTeilnehmer);
		JPanel pnlSichtbar = new JPanel();
		pnlSichtbar.setLayout(new BoxLayout(pnlSichtbar, BoxLayout.PAGE_AXIS));
		pnlSichtbar.add(lblSichtbar);
		for(int i = 0; i < cbxsSichtbar.length; i++){
			pnlSichtbar.add(cbxsSichtbar[i]);
		}
		pnlLeute.add(pnlSichtbar);
		pnlLower.add(pnlLeute);
		this.add(pnlLower);
		this.validate();
	}
	
	private final String htmlToday = "<html><span style=\"background-color: #00BB00\">";
	private final String htmlSunday = "<html><span style=\"background-color: #F2BBBB\">";
	private final String htmlRight = "</span></html>";
	private int[] makeKalender(){
		//int tag = (int) modelKalender.getValueAt(tblKalender.getSelectedRow(), tblKalender.getSelectedColumn());
		Calendar calendar = new GregorianCalendar(jahr, monat, tag);
		
		if (modelKalender.getRowCount() > 0) {
		    for (int i = modelKalender.getRowCount() - 1; i > -1; i--) {
		        modelKalender.removeRow(i);
		    }
		}
		
		modelKalender.addRow(new Object[]{"Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"});
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		Object[] row = new Object[7];
		int[] r = {-1, -1};
		int tageBevor = (7+(calendar.get(Calendar.DAY_OF_WEEK) - 2)) % 7 - 1;
		for(int i = 0; i < row.length; i++){
			if(calendar.get(Calendar.DAY_OF_MONTH) == tag){
				r[0] = 1;
				r[1] = i;
			}
			if(i - tageBevor <= 0){
				row[i] = null;
			}
			else{
				int t = calendar.get(Calendar.DAY_OF_MONTH);
				if(! Kalender.isSameDay(calendar, today)){
					if(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						row[i] = t;
					}
					else{
						row[i] = htmlSunday + Integer.toString(t) + htmlRight;
					}
				}
				else{
					
					row[i] = htmlToday + Integer.toString(t) + htmlRight;
				}
				calendar.add(Calendar.DAY_OF_MONTH, 1);
			}
		}
		modelKalender.addRow(row);
		int max = (calendar.getActualMaximum(Calendar.DAY_OF_MONTH)-calendar.get(Calendar.DAY_OF_MONTH)+1)/7;
		for(int i = 0; i < max; i++){
			for(int j = 0; j < row.length; j++, calendar.add(Calendar.DAY_OF_MONTH, 1)){
				if(calendar.get(Calendar.DAY_OF_MONTH) == tag){
					r[0] = 2 + i;
					r[1] = j;
				}
				if(! Kalender.isSameDay(calendar, today)){
					if(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						row[j] = calendar.get(Calendar.DAY_OF_MONTH);
					}
					else{
						row[j] = htmlSunday + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + htmlRight;
					}
				}
				else{
					
					row[j] = htmlToday + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + htmlRight;
				}
			}
			modelKalender.addRow(row);
		}
		for(int i = 0; i < row.length; i++){
			if(calendar.get(Calendar.DAY_OF_MONTH) == tag){
				r[0] = 3 + max;
				r[1] = i;
			}
			if(calendar.get(Calendar.DAY_OF_MONTH) != 1){
				if(! Kalender.isSameDay(calendar, today)){
					if(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						row[i] = calendar.get(Calendar.DAY_OF_MONTH);
					}
					else{
						row[i] = htmlSunday + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + htmlRight;
					}
				}
				else{
					row[i] = htmlToday + Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + htmlRight;
				}
				calendar.add(Calendar.DAY_OF_MONTH, 1);
			}
			else{
				row[i] = null;
			}
		}
		modelKalender.addRow(row);
		
		modelKalender.fireTableDataChanged();
		
		return r;
	}
	
	@Override
	public String getName() {
		return "Kalender";
	}

	@Override
	public String getBase() {
		return "kalender";
	}
	
	private class KalenderListener extends SubPanelListener implements ActionListener, ListSelectionListener{

		private KalenderPanel listenTo;
		
		private int counter = 0;
		
		public KalenderListener(KalenderPanel kalenderPanel) {
			listenTo = kalenderPanel;
		}

		@Override
		public void actionPerformed(ActionEvent evt) {
			if(evt.getSource().equals(btnDelete)){
				Termin[] termine = data.kalender.getTermine(tag, monat, jahr);
				int index = tblListe.getSelectedRow();
				if(index != -1){
					for(int i = 0; i < termine.length; i++){
						if(termine[i].getName().equals(modelListe.getValueAt(index, 0))){
								
							int confirmed = JOptionPane.showConfirmDialog(listenTo, 
									"Bist du sicher, dass du den Termin \"" + termine[i].getName() + "\" löschen willst?", 
									"Termin löschen", 
									JOptionPane.YES_NO_OPTION); 
							
							if(confirmed == JOptionPane.YES_OPTION){
								boolean ok = data.kalender.loeschen(termine[i]);
								if(ok){
									modelListe.removeRow(index);
									modelListe.fireTableRowsDeleted(index, index);
								}
								else{
									JOptionPane.showMessageDialog(listenTo, "Du bist nicht zum löschen dieses Termins berechtigt.", "Erlaubnis verweiget", JOptionPane.ERROR_MESSAGE);
								}
							}
							
							break;
						}
					}
				}
			}
			else if(evt.getSource().equals(btnPrevious)){
				Calendar calendar = new GregorianCalendar(jahr, monat, tag);
				calendar.add(Calendar.MONTH, -1);
				jahr = calendar.get(Calendar.YEAR);
				monat = calendar.get(Calendar.MONTH);
				tag = calendar.get(Calendar.DAY_OF_MONTH);

				int[] r = makeKalender();
				tblKalender.changeSelection(r[0], r[1], false, false);
				lblMonat.setText(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
			}
			else if(evt.getSource().equals(btnNext)){
				Calendar calendar = new GregorianCalendar(jahr, monat, tag);
				calendar.add(Calendar.MONTH, 1);
				jahr = calendar.get(Calendar.YEAR);
				monat = calendar.get(Calendar.MONTH);
				tag = calendar.get(Calendar.DAY_OF_MONTH);
				
				int[] r = makeKalender();
				tblKalender.changeSelection(r[0], r[1], false, false);
				lblMonat.setText(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
			}
			else if(evt.getSource().equals(btnNew)){
				String[] date = txtAnfangDatum.getText().split("\\.");
				String[] time = txtAnfangUhrzeit.getText().split(":");
				GregorianCalendar von = new GregorianCalendar(Integer.parseInt(date[2]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[0]), Integer.parseInt(time[0]), Integer.parseInt(time[1]));
				date = txtEndeDatum.getText().split("\\.");
				time = txtEndeUhrzeit.getText().split(":");
				GregorianCalendar bis = new GregorianCalendar(Integer.parseInt(date[2]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[0]), Integer.parseInt(time[0]), Integer.parseInt(time[1]));
				ArrayList<Bewohner> teilnehmer = new ArrayList<Bewohner>();
				Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
				for(int i = 0; i < cbxsTeilnehmer.length; i++){
					if(! cbxsTeilnehmer[i].isSelected()){
						continue;
					}
					for(int j = 0; j < bewohner.length; j++){
						if(bewohner[j].getName().equals(cbxsTeilnehmer[i].getText())){
							teilnehmer.add(bewohner[j]);
						}
					}
				}
				ArrayList<Bewohner> sichtbar = new ArrayList<Bewohner>();
				for(int i = 0; i < cbxsSichtbar.length; i++){
					if(! cbxsSichtbar[i].isSelected()){
						continue;
					}
					for(int j = 0; j < bewohner.length; j++){
						if(bewohner[j].getName().equals(cbxsSichtbar[i].getText())){
							sichtbar.add(bewohner[j]);
						}
					}
				}
				data.kalender.add(txtName.getName(), von, bis, teilnehmer.toArray(new Bewohner[]{}), sichtbar.toArray(new Bewohner[]{}), cbxGanzTags.isSelected());
				if(von.get(Calendar.DAY_OF_MONTH) == tag){
					String von1 = von.get(Calendar.HOUR_OF_DAY) + ":" + von.get(Calendar.MINUTE);
					String bis1 = bis.get(Calendar.HOUR_OF_DAY) + ":" + bis.get(Calendar.MINUTE);
					modelListe.addRow(new Object[]{txtName.getText(), von1, bis1});
				}
			}
		}

		@Override
		public void valueChanged(ListSelectionEvent evt) {
			if(! evt.getValueIsAdjusting()){
				if(tblKalender.getSelectedRow() > 0 && tblKalender.getSelectedColumn() != -1){
					Object o = tblKalender.getValueAt(tblKalender.getSelectedRow(), tblKalender.getSelectedColumn());
					if(o != null){
						String t = o.toString();
						t = t.replace(htmlSunday, "");
						t = t.replace(htmlToday, "");
						t = t.replace(htmlRight, "");
						tag = Integer.parseInt(t);
						
						Termin[] termine = data.kalender.getTermine(tag, monat, jahr);
						if (modelListe.getRowCount() > 0) {
						    for (int i = modelListe.getRowCount() - 1; i > -1; i--) {
						        modelListe.removeRow(i);
						    }
						}
						
						for(int i = 0; i < termine.length; i++){
							String von = termine[i].getVon().get(Calendar.HOUR_OF_DAY) + ":" + termine[i].getVon().get(Calendar.MINUTE);
							String bis = termine[i].getBis().get(Calendar.HOUR_OF_DAY) + ":" + termine[i].getBis().get(Calendar.MINUTE);
							modelListe.addRow(new Object[]{termine[i].getName(), von, bis});
						}
						
						modelListe.fireTableDataChanged();
					}
				}
				else{
					if (modelListe.getRowCount() > 0) {
					    for (int i = modelListe.getRowCount() - 1; i > -1; i--) {
					        modelListe.removeRow(i);
					    }
					    modelListe.fireTableDataChanged();
					}
				}
			}
		}
	}
}
