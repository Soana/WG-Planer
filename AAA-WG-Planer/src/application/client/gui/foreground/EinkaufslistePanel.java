package application.client.gui.foreground;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.*;

import application.client.gui.background.BackgroundWindow;
import application.client.logik.Bewohner;
import application.client.logik.DataObject;
import application.client.logik.Einkaufsliste;
import application.client.logik.Mitbewohnerverwaltung;
import application.shared.data.Eintrag;


public class EinkaufslistePanel extends FunctionPanel {

	private EinkaufslisteListener listener;

	private JButton btnDelete;

	private JButton btnExport;
	
	private UnchangeableTableModel modelEintrag;
	private JTable tblEintrag;
	
	private JLabel lblWas;
	private JLabel lblWieviel;
	private JTextField txtWas;
	private JTextField txtWieviel;
	private JButton btnNew;
	
	private JCheckBox[] cbxsFuer;
	
	private DataObject data;
	
	public EinkaufslistePanel(BackgroundWindow parent, DataObject dobj) {
		super(parent);

		this.data = dobj;
		listener = new EinkaufslisteListener(this);
		
		setupGui();
	}

	private void setupGui() {
		btnDelete = new JButton("Löschen");
		btnDelete.addActionListener(listener);

		btnExport = new JButton("Einkaufsliste erstellen");
		btnExport.addActionListener(listener);
		
		Eintrag[] eintraege = data.einkaufsliste.getEintraege();
		modelEintrag = new UnchangeableTableModel(new Object[]{"Was", "Wieviel", "Für"}, eintraege.length);
		for(int i = 0; i < eintraege.length; i++){
			modelEintrag.addRow(new Object[]{eintraege[i].getWas(), eintraege[i].getWieviel(), eintraege[i].getFuerAsString()});
		}
		tblEintrag = new JTable(modelEintrag);
		
		lblWas = new JLabel("Was:");
		lblWieviel = new JLabel("Wieviel:");
		txtWas = new JTextField();
		txtWieviel = new JTextField();
		btnNew = new JButton("Eintragen");
		btnNew.addActionListener(listener);
		
		Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
		cbxsFuer = new JCheckBox[bewohner.length];
		for(int i = 0; i < bewohner.length; i++){
			cbxsFuer[i] = new JCheckBox(bewohner[i].getName());
		}
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		this.add(btnExport);
		this.add(btnDelete);
		
		JScrollPane pnlEintrag = new JScrollPane();
		pnlEintrag.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		pnlEintrag.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pnlEintrag.setViewportView(tblEintrag);
		this.add(pnlEintrag);
		
		JPanel pnlNew = new JPanel();
		pnlNew.setLayout(new BoxLayout(pnlNew, BoxLayout.LINE_AXIS));
		
		JPanel pnlText = new JPanel();
		pnlText.setLayout(new GridLayout(3, 2, hgap, vgap));
		pnlText.add(lblWas);
		pnlText.add(txtWas);
		pnlText.add(lblWieviel);
		pnlText.add(txtWieviel);
		pnlText.add(btnNew);
		pnlNew.add(pnlText);
		
		JPanel pnlFuer = new JPanel();
		pnlFuer.setLayout(new BoxLayout(pnlFuer, BoxLayout.PAGE_AXIS));
		for(int i = 0; i < cbxsFuer.length; i++){
			pnlFuer.add(cbxsFuer[i]);
		}
		pnlNew.add(pnlFuer);
		this.add(pnlNew);
		this.validate();
	}

	private class EinkaufslisteListener extends SubPanelListener implements ActionListener{

		private EinkaufslistePanel listenTo;
		
		public EinkaufslisteListener(EinkaufslistePanel listento){
			listenTo = listento;
		}
		
		@Override
		public void actionPerformed(ActionEvent evt) {
			if(evt.getSource().equals(btnDelete)){
				Eintrag[] eintraege = data.einkaufsliste.getEintraege();
				int index = tblEintrag.getSelectedRow();
				if(index != -1){
					for(int i = 0; i < eintraege.length; i++){
						if(eintraege[i].getWas().equals(modelEintrag.getValueAt(index, 0))
								&& eintraege[i].getWieviel() == (int) modelEintrag.getValueAt(index, 1)
								&& eintraege[i].getFuerAsString().equals(modelEintrag.getValueAt(index, 2))){
							int confirmed = JOptionPane.showConfirmDialog(listenTo, 
									"Bist du sicher, dass du den markierten Eintrag löschen willst?", 
									"Eintrag löschen", 
									JOptionPane.YES_NO_OPTION); 
							
							if(confirmed == JOptionPane.YES_OPTION){
								data.einkaufsliste.loeschen(eintraege[i]);
								modelEintrag.removeRow(index);
								modelEintrag.fireTableRowsDeleted(index, index);
							}
						}
						break;
					}
				}
			}
			else if(evt.getSource().equals(btnNew)){
				Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
				ArrayList<Bewohner> b = new ArrayList<Bewohner>();
				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < cbxsFuer.length; i++){
					if(cbxsFuer[i].isSelected()){
						for(int j = 0; j < bewohner.length; j++){
							if(bewohner[j].getName().equals(cbxsFuer[i].getText())){
								b.add(bewohner[j]);
								sb.append(bewohner[j].getKurz() + ", ");
							}
						}
					}
				}
				sb.delete(sb.length() - 2, sb.length());
				modelEintrag.addRow(new Object[]{txtWas.getText(), Integer.parseInt(txtWieviel.getText()), sb.toString()});
				data.einkaufsliste.add(txtWas.getText(), Integer.parseInt(txtWieviel.getText()), b.toArray(new Bewohner[]{}));
			}
			else if(btnExport.equals(evt.getSource())){
				data.einkaufsliste.erstelleEinkaufsliste();
				JOptionPane.showMessageDialog(listenTo, "Einkaufsliste wurde exportiert.", "Export", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		
	}

	@Override
	public String getName() {
		return "Gesamte Einkaufsliste";
	}

	@Override
	public String getBase() {
		return "einkaufsliste";
	}
}
