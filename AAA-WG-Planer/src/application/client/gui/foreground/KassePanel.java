package application.client.gui.foreground;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import application.client.gui.background.BackgroundWindow;
import application.client.logik.Bewohner;
import application.client.logik.DataObject;
import application.client.logik.Einkaufsliste;
import application.client.logik.Mitbewohnerverwaltung;

public class KassePanel extends FunctionPanel {

	private KasseListener listener;
	
	private UnchangeableTableModel modelData;
	private JTable tblData;
	private JButton btnUbersicht;
	private JButton btnRechnung;
	
	private DataObject data;
	
	public KassePanel(BackgroundWindow parent, DataObject dobj) {
		super(parent);
		
		listener = new KasseListener();
		
		this.data = dobj;
		
		setupGui();
	}
	
	private void setupGui(){
		Bewohner[] bewohner = data.mitbewohnerverwaltung.getBewohner();
		
		modelData = new UnchangeableTableModel(new Object[]{"Mitbewohner", "Guthaben"}, 0);
		
		for(int i = 0; i < bewohner.length; i++){
			modelData.addRow(new Object[]{bewohner[i].getName(), bewohner[i].getGuthaben()});
		}
		tblData = new JTable(modelData);
		
		JScrollPane pnlData = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pnlData.setViewportView(tblData);
		
		btnUbersicht = new JButton("Übersicht erstellen");
		btnRechnung = new JButton("Rechnungsübersicht erstellen");
		
		btnUbersicht.addActionListener(listener);
		btnUbersicht.addActionListener(listener);
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		this.add(pnlData);
		this.add(btnUbersicht);
		this.add(btnRechnung);
		this.validate();
	}

	@Override
	public String getName() {
		return "Kasse";
	}

	@Override
	public String getBase() {
		return "kasse";
	}
	
	private class KasseListener extends SubPanelListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent evt) {
			if(evt.getSource().equals(btnRechnung)){
				data.einkaufsliste.erstelleRechungsUbersicht();
			}
			else if(evt.getSource().equals(btnUbersicht)){
				data.mitbewohnerverwaltung.erstelleGuthabenUbersicht();
			}
			
		}
		
	}
}
