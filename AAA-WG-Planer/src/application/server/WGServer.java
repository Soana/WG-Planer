package application.server;

import java.sql.*;
import java.util.Properties;

import net.sourceforge.argparse4j.*;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;

import java.util.*;

public class WGServer {
	static Properties props;
	static String dbuser;
	static String dbpass;
	static String dbname;
	private WelcomeSocket ws;
	private DatabaseHandler db;
	private List<User> users;
	private int port;
	
	public int getPort()
	{
		return port;
	}
	
	public void registerConnection(application.shared.network.Connection c)
	{
		users.add(new User(c, db));
	}
	
	public WGServer()
	{
		users = new LinkedList<User>();
		//db = new DatabaseHandler();

		port = 1234;
		ws = new WelcomeSocket(this);
		ws.start();
	}
	

	public static void main(String[] args) {
		/*//TODO 
		ArgumentParser argpars = ArgumentParsers.newArgumentParser("WGServer");
		argpars.description("Helps with the administration and social life of communes.");
		//argpars.addArgument("configfile").metavar("C").type(String.class).help("Location of the config file.");
		argpars.addArgument("--new", "-n").action(Arguments.storeTrue()).type(Boolean.class).help("Create a new Database interactively.");
		argpars.addArgument("--user", "-u").metavar("USER").type(String.class).help("SQL user");
		argpars.addArgument("--password", "-p").metavar("PASSWORD").type(String.class).help("SQL password");
		argpars.addArgument("-d", "--database").metavar("DB").type(String.class).help("SQL DB name.");
		argpars.addArgument("--simulate").action(Arguments.storeTrue()).type(Boolean.class);
		
		net.sourceforge.argparse4j.inf.Namespace arg = null;
		try{
			arg = argpars.parseArgs(args);
			dbname = arg.getString("database");
			dbuser = arg.getString("user");
			dbpass = arg.getString("password");
			
			
			if (arg.getBoolean("new") != null){
				
				return;
			}
		}
		catch (ArgumentParserException e){
			argpars.handleError(e);
			System.exit(1);
		}*/
		
		new WGServer();		
	}
	
	
	
}
