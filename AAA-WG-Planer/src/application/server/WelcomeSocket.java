package application.server;

import java.io.IOException;
import java.net.*;

import application.server.WGServer;
import application.shared.network.*;

public class WelcomeSocket extends Thread
{
	private ServerSocket socket;
	private WGServer server;
	
	public WelcomeSocket(WGServer server)
	{
		try
		{
			socket = new ServerSocket(server.getPort()); //Neuen Serversocket öffnen, der neue Sockets akzeptieren wird
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		this.server = server;//Pointer auf Server
	}
	
	@Override
	public void run()
	{
		System.out.println("Welcomesocket up and listening!");
		while(!isInterrupted()) //Solange, bis uns jemand sagt, wir sollen aufhören
		{
			try
			{
				Socket sock = socket.accept(); //Der Serversocket ist ein Türsteher, der auf neue Gäste wartet und ihnen ihre "Zimmer" zeigt
				Connection conn = new Connection(sock);//Ein neues Zimmer herstellen und den Gast hineinstecken
				server.registerConnection(conn); //Dem Meister bescheidsagen, dass ein neuer Gast und sein Zimmer da sind
			}
			catch(IOException e)
			{
				e.printStackTrace();
				break; //Jemand hat die Strasse gesprengt, und keine neues Gäste können mehr kommen
			}
			
		}
	}
	
	public void shutdown() //Man will uns nicht mehr
	{
		interrupt();
	}
}
