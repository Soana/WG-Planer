package application.server; 
/**
 * This class contains the SQL statements which are used on the server.
 * This is implemented as a singleton, so the database can be specified in a config file.
 * 
 */
public class SqlStatements {
	private volatile static SqlStatements singleton;
		
	private SqlStatements(){
		//db = WGServer.dbname;
		db = "Test";
		isUserAdmin = "SELECT admin FROM " + db + ".users WHERE id = ?";
		
		insertAufgabe = "INSERT INTO " + db + ".aufgaben " + 
				"(id, name, descrip, anzahl, tage, wg_id) "
				+ "VALUES (default, ?, ?, ?, ? , ?)";
		
		insertTermin = "INSERT INTO " + db + ".termine" + 
				"(id, start_date, end_date, ganztag, " ;
		insertEintrag = "INSERT INTO " + db + ".eintraege "	
				+ "(id, name, descr, number, wg) "
				+ "VALUES (default, ?, ?, ?, ?)";
		insertWG = "INSERT INTO " + db + ".wg (id, name) VALUES (default, ?)";
		insertUser = "INSERT INTO " + db + ".user "
				+ "(id, first_name, last_name, kuerzel, wg_id) "
				+ "VALUES (default, ?,?,?,?)";
		
		
		getUserExpiredDate = "SELECT expdate FROM " + db + ".users WHERE id = ?";
		
		getUserID = "SELECT id FROM " + db + ".users WHERE kuerzel = ?";
		getUserHash = "SELECT hash FROM " + db + ".users WHERE id = ?";
		getUIDandHash = "SELECT id, hash FROM " + db + ".users WHERE kuerzel = ?";
		
		getAufgabenSorted = "SELECT id, name, tage, anzahl FROM " + db + ".aufgaben WHERE deleted IS NULL AND wg_id = ? ORDER BY id";
		getWGs = "SELECT id, name FROM "+ db +".wg ORDER BY id";
		getUsersInWG = "SELECT id FROM " + db +".user WHERE wg_id = ? ORDER BY id";
		
		insertKonkreteAufgabe = "INSERT INTO " + db + ".konkrete_aufgaben ( datum, user_id, aufgaben_id) VALUES ( ?, ?,?)";
		//setTerminForAufgabe = "INSERT INTO " + db + ".termine (id, name, start_date, creator) VALUES (default, ?, ?, -1)";
	}
	
	public static SqlStatements getSqlStatements(){
		if(singleton == null){
			synchronized (SqlStatements.class){
				if(singleton == null){
					singleton = new SqlStatements();
				}
			}
		}
		return singleton;
	}
	
	
	/**
	 * Stores the database name;
	 */
	private final String db;
	public final String isUserAdmin;
	public final String getUserExpiredDate;
	public final String getUserID, getUserHash, getUIDandHash;
	
	public final String getAufgabenSorted, getWGs, getUsersInWG;
	public final String insertKonkreteAufgabe;
	
	public final String insertAufgabe;
	public final String insertEintrag;
	public final String insertTermin;
	public final String insertWG;
	public final String insertUser;

}
