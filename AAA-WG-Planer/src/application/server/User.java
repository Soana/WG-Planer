package application.server;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import application.client.logik.Bewohner;
import application.server.DatabaseHandler;
import application.shared.data.*;
import application.shared.network.*;
import application.shared.network.protokoll.*;

public class User
{
	private Connection conn;
	private DatabaseHandler db;
	
	private int userid;
	
	public User(Connection conn, final DatabaseHandler db)
	{

		this.conn = conn;
		this.db = db;
		
		
		conn.start();
		
		//TODO: Daten fuer Requesthandler aus db holen
	}
	
	private void addProtokollAufgabe(){
		this.conn.registerRequestHandler(ProtokollAufgabe.Add.getId(), new RequestHandler(){
			//Fuegt eine Aufgabe hinzu
			@Override
			public JSONObject onRequest(JSONObject jObj) {
				boolean[] tage = new boolean[7];
				tage[0] = jObj.getBoolean(ProtokollAufgabe.Add.MO);
				tage[1] = jObj.getBoolean(ProtokollAufgabe.Add.DI);
				tage[2] = jObj.getBoolean(ProtokollAufgabe.Add.MI);
				tage[3] = jObj.getBoolean(ProtokollAufgabe.Add.DO);
				tage[4] = jObj.getBoolean(ProtokollAufgabe.Add.FR);
				tage[5] = jObj.getBoolean(ProtokollAufgabe.Add.SA);
				tage[6] = jObj.getBoolean(ProtokollAufgabe.Add.SO);
				int id = db.addAufgabe(jObj.getString(ProtokollAufgabe.Add.NAME), jObj.getInt(ProtokollAufgabe.Add.ANZAHL), tage);
				if( id == -1){
					throw new RuntimeException("Aufgabe " + jObj.getString(ProtokollAufgabe.Add.NAME) + " konnte nicht erstellt werden.");
				}
				return ProtokollAufgabe.Add.response(id);
			}
			
		});
		this.conn.registerRequestHandler(ProtokollAufgabe.Delete.getId(), new RequestHandler(){
			//Loescht eine Aufgabe
			@Override
			public JSONObject onRequest(JSONObject jObj) {
				db.deleteAufgabe(jObj.getInt(ProtokollAufgabe.Delete.ID));
				return ProtokollAufgabe.Delete.response();
			}
			
		});
	}

	private void addProtokollBewohner(){
		this.conn.registerRequestHandler(ProtokollBewohner.Add.getId(), new RequestHandler(){
			//Fuegt einen Bewohner hinzu
			@Override
			public JSONObject onRequest(JSONObject jObj) {
				JSONObject dbreturn = db.addBewohner(jObj.getString(ProtokollBewohner.Add.VORNAME), jObj.getString(ProtokollBewohner.Add.NACHNAME));
				if(dbreturn == null){
					throw new RuntimeException("Bewohner " + jObj.getString(ProtokollBewohner.Add.VORNAME) + " " + jObj.getString(ProtokollBewohner.Add.NACHNAME) + " konnte nicht erstellt werden.");
				}
				return ProtokollBewohner.Add.response(dbreturn.getInt("id"), dbreturn.getString("benutzername"));
			}
			
		});
		this.conn.registerRequestHandler(ProtokollBewohner.Delete.getId(), new RequestHandler(){
			//Loescht einen Bewohner
			@Override
			public JSONObject onRequest(JSONObject jObj) {
				db.deleteBewohner(jObj.getInt(ProtokollBewohner.Delete.ID));
				return ProtokollBewohner.Delete.response();
			}
			
		});
		this.conn.registerRequestHandler(ProtokollBewohner.Update.getId(), new RequestHandler(){

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				int id = jObj.getInt(ProtokollBewohner.Update.ID);
				String vorname = jObj.getString(ProtokollBewohner.Update.VORNAME);
				String nachname = jObj.getString(ProtokollBewohner.Update.NACHNAME);
				String telefon = jObj.getString(ProtokollBewohner.Update.TELEFON);
				long geburt = jObj.getLong(ProtokollBewohner.Update.GEBURT);
				
				String benutzer = db.updateBewoher(id, vorname, nachname, telefon, geburt);
				
				return ProtokollBewohner.Update.response(benutzer);
			}
			
		});
	}

	private void addProtokollEintrag(){
		this.conn.registerRequestHandler(ProtokollEintrag.AddEintrag.getId(), new RequestHandler(){
			//Fuegt einen Eintrag in die Einkaufsliste ein
			@Override
			public JSONObject onRequest(JSONObject jObj) {
				JSONArray arr = jObj.getJSONArray(ProtokollEintrag.AddEintrag.WER);
				int[] wer = new int[arr.length()];
				for(int i = 0; i < arr.length(); i++){
					wer[i] = arr.getInt(i);
				}
				int id = db.addEintrag(jObj.getString(ProtokollEintrag.AddEintrag.WAS), jObj.getInt(ProtokollEintrag.AddEintrag.ANZAHL), wer);
				return ProtokollEintrag.AddEintrag.response(id);
			}
			
		});
		this.conn.registerRequestHandler(ProtokollEintrag.AddRechnung.getId(), new RequestHandler(){
			//Erstellt eine neue Rechnung
			@Override
			public JSONObject onRequest(JSONObject jObj) {
				JSONArray arr = jObj.getJSONArray(ProtokollEintrag.AddRechnung.FUER);
				//Rechnung in die DB eintragen
				int[] fuer = new int[arr.length()];
				for(int i = 0; i < arr.length(); i++){
					fuer[i] = arr.getJSONObject(i).getInt(ProtokollEintrag.AddRechnung.ID);
				}
				
				db.addRechnung(jObj.getString(ProtokollEintrag.AddRechnung.KOMMENTAR), jObj.getInt(ProtokollEintrag.AddRechnung.VON), jObj.getInt(ProtokollEintrag.AddRechnung.PREIS), fuer);
				
				//brechne die Preisanteile der Personen
				int preis = jObj.getInt(ProtokollEintrag.AddRechnung.PREIS);
				int anzahl = 0;
				for(int i = 0; i < arr.length(); i++){
					anzahl++;
					anzahl += arr.getJSONObject(i).getInt(ProtokollEintrag.AddRechnung.GAESTE);
				}
				int anteil = preis/anzahl;
				if(preis%anzahl != 0){
					anteil++;
				}
				
				HashMap<Integer, Integer> anteile = new HashMap<>();
				for(int i = 0; i < arr.length(); i++){
					JSONObject jo = arr.getJSONObject(i);
					anteile.put(jo.getInt(ProtokollEintrag.AddRechnung.ID), -anteil * jo.getInt(ProtokollEintrag.AddRechnung.GAESTE));
					db.changeGuthaben(jo.getInt(ProtokollEintrag.AddRechnung.ID), -anteil * jo.getInt(ProtokollEintrag.AddRechnung.GAESTE));
				}
				anteile.put(jObj.getInt(ProtokollEintrag.AddRechnung.VON), anteil*anzahl);
				db.changeGuthaben(jObj.getInt(ProtokollEintrag.AddRechnung.VON), anteil*anzahl);
				
				//Preisanteile zurueck senden
				return ProtokollEintrag.AddRechnung.response(anteile);
			}
			
		});
		this.conn.registerRequestHandler(ProtokollEintrag.DeleteEintrag.getId(), new RequestHandler(){

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				db.deleteEintrag(jObj.getInt(ProtokollEintrag.DeleteEintrag.ID));
				return ProtokollEintrag.DeleteEintrag.response();
			}
			
		});
		this.conn.registerRequestHandler(ProtokollEintrag.DeleteEintragMutiple.getId(), new RequestHandler(){

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				JSONArray arr = jObj.getJSONArray(ProtokollEintrag.DeleteEintragMutiple.ID);
				for(int i = 0; i < arr.length(); i++){
					db.deleteEintrag(arr.getInt(i));
				}
				return ProtokollEintrag.DeleteEintragMutiple.response();
			}
			
		});
	}

	private void addProtokollTermin(){
		this.conn.registerRequestHandler(ProtokollTermin.Add.getId(), new RequestHandler(){

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				JSONObject v = jObj.getJSONObject(ProtokollTermin.Add.VON);
				GregorianCalendar von = new GregorianCalendar(v.getInt(ProtokollTermin.Add.JAHR), v.getInt(ProtokollTermin.Add.MONAT), v.getInt(ProtokollTermin.Add.TAG), v.getInt(ProtokollTermin.Add.STUNDE), v.getInt(ProtokollTermin.Add.MINUTE));
				JSONObject b = jObj.getJSONObject(ProtokollTermin.Add.VON);
				GregorianCalendar bis = new GregorianCalendar(b.getInt(ProtokollTermin.Add.JAHR), b.getInt(ProtokollTermin.Add.MONAT), b.getInt(ProtokollTermin.Add.TAG), b.getInt(ProtokollTermin.Add.STUNDE), b.getInt(ProtokollTermin.Add.MINUTE));
				JSONArray arr = jObj.getJSONArray(ProtokollTermin.Add.WER);
				int[] teilnehmer = new int[arr.length()];
				for(int i = 0; i < arr.length(); i++){
					teilnehmer[i] = arr.getInt(i);
				}
				arr = jObj.getJSONArray(ProtokollTermin.Add.SICHTBAR);
				int[] sichtbar = new int[arr.length()];
				for(int i = 0; i < arr.length(); i++){
					sichtbar[i] = arr.getInt(i);
				}
				int id = db.addTermin(jObj.getString(ProtokollTermin.Add.NAME), userid, von, bis, teilnehmer, sichtbar, jObj.getBoolean(ProtokollTermin.Add.GANZ_TAGS));
				return ProtokollTermin.Add.response(id);
			}
			
		});
		this.conn.registerRequestHandler(ProtokollTermin.Delete.getId(), new RequestHandler(){

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				db.deleteTermin(jObj.getInt(ProtokollTermin.Delete.ID));
				return ProtokollTermin.Delete.response();
			}
			
		});
	}

	private void addProtokollAllgemein(){
		this.conn.registerRequestHandler(ProtokollAllgemein.CreateWG.getId(), new RequestHandler(){

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				// TODO Auto-generated method stub
				return null;
			}
			
		});
		
		this.conn.registerRequestHandler(ProtokollAllgemein.Init.getId(), new RequestHandler(){

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				Aufgabe[] aufgaben = db.getAufgaben();
				conn.sendRequest(ProtokollAufgabe.GetAll.getId(), ProtokollAufgabe.GetAll.request(aufgaben));
				
				Bewohner[] bewohner = db.getBewohner();
				conn.sendRequest(ProtokollBewohner.GetAll.getId(), ProtokollBewohner.GetAll.request(bewohner));
				
				Eintrag[] eintraege = db.getEintrage();
				conn.sendRequest(ProtokollEintrag.GetAllEintraege.getId(),ProtokollEintrag.GetAllEintraege.request(eintraege));
				
				Rechnung[] rechnungen = db.getRechnungen();
				conn.sendRequest(ProtokollEintrag.GetAllRechnungen.getId(), ProtokollEintrag.GetAllRechnungen.request(rechnungen));
				
				Termin[] termine = db.getTermine(userid);
				conn.sendRequest(ProtokollTermin.GetAll.getId(), ProtokollTermin.GetAll.request(termine));
				
				return ProtokollAllgemein.Init.response();
			}
			
		});
		
		this.conn.registerRequestHandler(ProtokollAllgemein.Login.getId(), new RequestHandler(){

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				Bewohner bewohner = db.checkLogin(jObj.getString(ProtokollAllgemein.Login.USER), jObj.getString(ProtokollAllgemein.Login.PASSWORD));
				if(bewohner != null){
					return ProtokollAllgemein.Login.response(bewohner);
				}
				else{
					return ProtokollAllgemein.Login.response();
				}
			}
			
		});
		
		this.conn.registerRequestHandler(ProtokollAllgemein.LogOut.getId(), new RequestHandler(){

			@Override
			public JSONObject onRequest(JSONObject jObj) {
				// TODO ausloggen
				return null;
			}
			
		});
	}
}
