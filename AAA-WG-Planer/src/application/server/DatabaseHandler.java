package application.server;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import org.json.JSONObject;

import application.client.logik.Bewohner;
import application.shared.data.Aufgabe;
import application.shared.data.Eintrag;
import application.shared.data.Rechnung;
import application.shared.data.Termin;

public class DatabaseHandler {

	java.sql.Connection connection;
	int wg;
	
	public DatabaseHandler(){
		/*register the jdbc driver */
		try {
			Class.forName( "com.mysql.jdbc.Driver" );
		} catch (ClassNotFoundException e) {
			System.err.println("Fatal error: MySQL JDBC Driver could not be loaded.");
			System.exit(1);
		}
		
		//create the DB if necessary
		Properties config = new Properties();
	
		try {
			config.load(DatabaseHandler.class.getClassLoader().getResourceAsStream("data/Database.properties"));
		}
		catch (IOException e) {
			System.out.println("data/Database.properties nicht gefunden");
		}
		String server = config.getProperty("dbserver");
		String rawPort = config.getProperty("dbport");
		String user = config.getProperty("dbuser");
		String passwort = config.getProperty("dbpasswort");
		String name = config.getProperty("dbname");
		
		if(!(server == null || rawPort == null || name == null || user == null || passwort == null)){
			int port = Integer.parseInt(rawPort);
//			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/", dbuser, dbpassword);
			try {
				connection = DriverManager.getConnection("jdbc:mysql://" + server + ":" + port + "/", user, passwort);
			}
			catch (SQLException e) {
				System.err.println("Fatal Error: Could not connect to database.\n\t" + e.getMessage());
				System.exit(1);
			}
			
			System.out.println("Datenbank wird angelegt..");
			try {
				create_db(name);
			} 
			catch (SQLException e) {
				System.err.println("Fatal error: Database could not be created.\n\t" + e.getMessage());
				e.printStackTrace();
				System.exit(1);
			}
			System.out.println("Datenbank angelegt.");
		}
	}
	
	public void exit(){
		try {
			connection.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public int addAufgabe(String name, int anzahl, boolean[] tage) {
		try {
			PreparedStatement stm = connection.prepareStatement("INSERT INTO aufgaben (wg_id, name, anzahl, tage) VALUES (?, ?, ?, ?)");
			stm.setInt(1, wg);
			stm.setString(2, name);
			stm.setInt(3, anzahl);
			int days = 0;
			int curr = 64;
			for(boolean b: tage){
				if(b){
					days +=  curr;
				}
				curr /= 2;
			}
			stm.setInt(4, days);
			stm.executeUpdate();
			stm = connection.prepareStatement("SELECT id FROM aufgaben WHERE name LIKE ? AND wg_id = ?");
			stm.setString(1, name);
			stm.setInt(2, wg);
			ResultSet rs = stm.executeQuery();
			int id = rs.getInt(1);
			stm.close();
			return id;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public void create_db(String dbname) throws SQLException {
//		Statement stm = connection.createStatement(); 
		
		PreparedStatement pstmt = connection.prepareStatement("CREATE DATABASE IF NOT EXISTS ?;");
		pstmt.setString(1, dbname);
		pstmt.executeUpdate();
//		connection.prepareStatement("CREATE DATABASE IF NOT EXISTS hello2").execute();
//		stm.executeUpdate("CREATE DATABASE IF NOT EXISTS " + dbname + ";");
		
//		stm.execute("use " + dbname+";");
		pstmt = connection.prepareStatement("use ?;");
		pstmt.setString(1, dbname);
		pstmt.execute();
		pstmt.close();
		
		Statement stm = connection.createStatement();
		stm.execute("CREATE TABLE IF NOT EXISTS wg (" +
				"id BIGINT NOT NULL auto_increment PRIMARY KEY," +
				"name VARCHAR(30) NOT NULL" +
				") ENGINE=InnoDB");
		
		stm.execute("CREATE TABLE IF NOT EXISTS user (" +
				"id BIGINT NOT NULL auto_increment," +
				"first_name VARCHAR(20) NOT NULL, " +
				"last_name VARCHAR(20) NOT NULL, " +
				"phone_num VARCHAR(40), "  +
				"birth_date DATE, " +
				"is_admin BOOLEAN DEFAULT FALSE NOT NULL," +
				"wg_id BIGINT NOT NULL," +
				"kuerzel VARCHAR(10) NOT NULL, " +
				"guthaben INTEGER DEFAULT 0 NOT NULL, " +
				"pw_hash BIGINT ," + 
			//	"pw_salt BIGINT, " +
				"deleted_date TIMESTAMP DEFAULT 0," +
				"last_login TIMESTAMP DEFAULT 0, "
				+ "PRIMARY KEY (id),"
				+ "FOREIGN KEY (wg_id) REFERENCES wg(id)" +
				") ENGINE=InnoDB");
		
		
		stm.execute("CREATE TABLE IF NOT EXISTS termine (" +
				"id BIGINT NOT NULL auto_increment PRIMARY KEY, " +
				"name VARCHAR(30) NOT NULL, " +
				"descrip VARCHAR(200), " +
				"creator_id BIGINT NOT NULL, " +
				"start TIMESTAMP NOT NULL DEFAULT 0, " +
				"end TIMESTAMP DEFAULT 0, " +
				"ganztags BOOLEAN DEFAULT FALSE, " +
				"last_change TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,"
				+ "FOREIGN KEY (creator_id) REFERENCES user(id) " + 
				") ENGINE=InnoDB");
		
		stm.execute("CREATE TABLE IF NOT EXISTS aufgaben (" +
				"id BIGINT NOT NULL auto_increment PRIMARY KEY, " +
				"name VARCHAR(30) NOT NULL, " +
				"descrip VARCHAR(200), " +
				"anzahl INT NOT NULL, " +
				"tage INT NOT NULL, " +
				"wg_id BIGINT NOT NULL, " +
				"last_change TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, " +
				"FOREIGN KEY (wg_id) REFERENCES wg(id), " +
				"deleted DATE" +
				") ENGINE=InnoDB;");
		
		stm.execute("CREATE TABLE IF NOT EXISTS termine_sichtbar (" +
				"termin_id BIGINT NOT NULL," +
				"user_id BIGINT NOT NULL," +
				"FOREIGN KEY (termin_id) REFERENCES termine(id) ON DELETE CASCADE," +
				"FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE" +
				") ENGINE=InnoDB");
		
		stm.execute("CREATE TABLEIF NOT EXISTS  t_nimmt_teil (" +
				"termin_id BIGINT NOT NULL," +
				"user_id BIGINT NOT NULL," +
				"nimmt_teil BOOLEAN default TRUE NOT NULL," +
				"last_change TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, " +
				"FOREIGN KEY (termin_id) REFERENCES termine(id) ON DELETE CASCADE," +
				"FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE" +
				") ENGINE=InnoDB");
		
		stm.execute("CREATE TABLE IF NOT EXISTS eintraege ( " +
				"name VARCHAR(20) NOT NULL," +
				"id BIGINT PRIMARY KEY auto_increment," +
				"anzahl INT NOT NULL," +
				"preis INT DEFAULT NULL," +
				"wg_id BIGINT NOT NULL," +
				"last_change TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," +
				"FOREIGN KEY (wg_id) REFERENCES wg(id)" +
				") ENGINE=InnoDB");
		
		stm.execute("CREATE TABLE IF NOT EXISTS finanzen (" +
				"id BIGINT PRIMARY KEY auto_increment," +
				"descrip VARCHAR(50)," +
				"user_id BIGINT NOT NULL," +
				"wg_id BIGINT NOT NULL," +
				"betrag INT NOT NULL," +
				"last_change TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," +
				"FOREIGN KEY (user_id) REFERENCES user(id)" +
				"FOREIGN KEY (wg_id) REFERENCES wg(id)" +
				") ENGINE=InnoDB");
		
		stm.execute("CREATE TABLE IF NOT EXISTS beteiligung ( " +
				"user_id BIGINT NOT NULL," +
				"eintrag_id BIGINT NOT NULL," +
				"FOREIGN KEY (user_id) REFERENCES user(id)," +
				"FOREIGN KEY (eintrag_id) REFERENCES eintraege(id)," +
				"anteil INT NOT NULL " +
				") ENGINE=InnoDB");
		
		stm.execute("CREATE TABLE IF NOT EXISTS geldfluss ( " +
				"finanz_id BIGINT NOT NULL," +
				"user_id BIGINT NOT NULL," +
				"FOREIGN KEY (user_id) REFERENCES user(id)," +
				"FOREIGN KEY (finanz_id) REFERENCES finanzen(id)" +
				") ENGINE=InnoDB");
		
		stm.execute("CREATE TABLE IF NOT EXISTS konkrete_aufgaben (" +
				"aufgaben_id BIGINT," +
				"datum DATE," + 
				"user_id BIGINT," +
				"woche INT," + 
				"FOREIGN KEY (aufgaben_id) REFERENCES aufgaben(id)," + 
				"FOREIGN KEY (user_id) REFERENCES user(id)" +
				") ENGINE=InnoDB");
		stm.close();
	}

	public JSONObject addBewohner(String vorname, String nachname) {
		try {
			PreparedStatement stm = connection.prepareStatement("INSERT INTO user (wg_id, first_name, last_name) VALUES (?, ?, ?)");
			stm.setInt(1, wg);
			stm.setString(2, vorname);
			stm.setString(3, nachname);
			stm.executeUpdate();
			
			stm = connection.prepareStatement("SELECT id, kuerzel FROM user WHERE wg_id = ? AND first_name LIKE ? AND last_name LIKE ?");
			stm.setInt(1, wg);
			stm.setString(2, vorname);
			stm.setString(3, nachname);
			ResultSet rs = stm.executeQuery();
			rs.next();
			
			JSONObject ret = new JSONObject();
			ret.put("id", rs.getInt(1));
			ret.put("benutzername", rs.getInt(2));
			
			stm.close();
			return ret;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int addEintrag(String was, int anzahl, int[] fuer) {
		try {
			PreparedStatement stm = connection.prepareStatement("INSERT INTO eintraege (wg_id name, anzahl) VALUES (?, ?, ?)");
			stm.setInt(1, wg);
			stm.setString(2, was);
			stm.setInt(3, anzahl);
			stm.executeUpdate();
			
			stm = connection.prepareStatement("SELECT id FROM eintraege WHERE wg_id = ? AND name = ?");
			stm.setInt(1, wg);
			stm.setString(2, was);
			ResultSet rs = stm.executeQuery();
			rs.next();
			int id = rs.getInt(1);
			
			for(int i: fuer){
				stm = connection.prepareStatement("INSERT INTO beteiligung (user_id, eintrag_id) VALUES(?, ?)");
				stm.setInt(1, i);
				stm.setInt(2, id);
				stm.executeUpdate();
			}
			
			stm.close();
			return id;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public void deleteAufgabe(int id) {
		
		try {
			PreparedStatement stm = connection.prepareStatement("DELETE FROM aufgaben WHERE wg_id = ? AND id = ?");
			stm.setInt(1, wg);
			stm.setInt(2, id);
			stm.executeUpdate();
			stm.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void deleteBewohner(int id) {
		try {
			PreparedStatement stm = connection.prepareStatement("UPDATE TABLE user SET deleted_date = SYSDATE WHERE id = ?");
			stm.setInt(1, id);
			stm.executeUpdate();
			
			stm.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addRechnung(String kommentar, int von, int preis, int[] fuer) {
		try {
			PreparedStatement stm = connection.prepareStatement("INSERT INTO finanzen (wg_id, descrip, user_id, betrag) VALUES (?, ?, ? ,?)");
			stm.setInt(1, wg);
			stm.setString(2, kommentar);
			stm.setInt(3, von);
			stm.setInt(4, preis);
			stm.executeUpdate();
			
			stm = connection.prepareStatement("SELECT id FROM finanzen WHERE wg_id = ? AND descrip = ? AND user_id = ? AND betrag = ?");
			stm.setInt(1, wg);
			stm.setString(2, kommentar);
			stm.setInt(3, von);
			stm.setInt(4, preis);
			ResultSet rs = stm.executeQuery();
			rs.next();
			int id = rs.getInt(1);
			
			for(int i: fuer){
				stm = connection.prepareStatement("INSERT INTO geldfluss (user_id, eintrag_id) VALUES (?, ?)");
				stm.setInt(1, i);
				stm.setInt(2, id);
				stm.executeUpdate();
			}
			
			stm.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void deleteEintrag(int id) {
		try {
			PreparedStatement stm = connection.prepareStatement("DELETE FROM beteiligung WHERE eintrag_id = ?");
			stm.setInt(1, id);
			stm.executeUpdate();
			
			stm = connection.prepareStatement("DELETE FROM eintraege WHERE id = ?");
			stm.setInt(1, id);
			stm.executeUpdate();
			
			stm.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int addTermin(String name, int ersteller, GregorianCalendar von, GregorianCalendar bis,
			int[] teilnehmer, int[] sichtbar, boolean ganzTags) {
		try {
			PreparedStatement stm = connection.prepareStatement("INSERT INTO termine (name, creator_id, start, end, ganztags) VALUES (?, ?, ?, ?, ?)");
			stm.setString(1, name);
			stm.setInt(2, ersteller);
			stm.setLong(3, von.getTime().getTime());
			stm.setLong(4, bis.getTime().getTime());
			stm.setBoolean(5, ganzTags);
			stm.executeUpdate();
			
			stm = connection.prepareStatement("SELECT id FROM termine WHERE name = ? AND creator_id = ?");
			stm.setString(1, name);
			stm.setInt(2, ersteller);
			ResultSet rs = stm.executeQuery();
			rs.next();
			int id = rs.getInt(1);
			
			for(int i: teilnehmer){
				stm = connection.prepareStatement("INSERT INTO t_nimmt_teil (termin_id, user_id) VALUES (?, ?)");
				stm.setInt(1, id);
				stm.setInt(2, i);
				stm.executeUpdate();
			}
			
			for(int i: sichtbar){
				stm = connection.prepareStatement("INSERT INTO termine_sichtbar (termin_id, user_id) VALUES (?, ?)");
				stm.setInt(1, id);
				stm.setInt(2, i);
				stm.executeUpdate();
			}
			
			stm.close();
			return id;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return -1;
	}

	public void deleteTermin(int id) {
		try {
			PreparedStatement stm = connection.prepareStatement("DELETE FROM termine_sichtbar WHERE termin_id = ?");
			stm.setInt(1, id);
			stm.executeUpdate();
			
			stm = connection.prepareStatement("DELETE FROM t_nimmt_teil WHERE termin_id = ?");
			stm.setInt(1, id);
			stm.executeUpdate();
			
			stm = connection.prepareStatement("DELETE FROM termine WHERE id = ?");
			stm.setInt(1, id);
			stm.executeUpdate();
			
			stm.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void changeGuthaben(int id, int preis) {
		try {
			PreparedStatement stm = connection.prepareStatement("SELECT guthaben FROM user WHERE id = ?");
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();
			rs.next();
			int guthaben = rs.getInt(1);
			
			stm = connection.prepareStatement("UPDATE user SET guthaben = ? WHERE id = ?");
			stm.setInt(1, guthaben + preis);
			stm.setInt(2, id);
			stm.executeUpdate();
			
			stm.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String updateBewoher(int id, String vorname, String nachname, String telefon, long geburt) {
		try {
			PreparedStatement stm = connection.prepareStatement("UPDATE bewohner SET first_name = ?, last_name = ?, phone_num = ?, birth_date = ? WHERE id = ?");
			stm.setString(1, vorname);
			stm.setString(2, nachname);
			stm.setString(3, telefon);
			stm.setDate(4, new Date(geburt));
			stm.setInt(5, id);
			stm.executeUpdate();
			String benutzer = null;
			stm = connection.prepareStatement("SELECT kuerzel FROM user WHERE id = ?");
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();
			rs.next();
			benutzer = rs.getString(1);
			stm.close();
			return benutzer;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Aufgabe[] getAufgaben() {
		try {
			PreparedStatement stm = connection.prepareStatement("SELECT * FROM Aufgaben WHERE wg_id = ?");
			stm.setInt(1, wg);
			ResultSet rs = stm.executeQuery();
			List<Aufgabe> aufgaben = new ArrayList<>();
			int id = rs.findColumn("id");
			int name = rs.findColumn("name");
			int desc = rs.findColumn("descrip");
			int anzahl = rs.findColumn("anzahl");
			int tage = rs.getInt(rs.findColumn("tage"));
			
			rs.next();
			while(! rs.isAfterLast()){
				
				Aufgabe a = new Aufgabe(rs.getString(name), (tage & 0x40) != 0, (tage & 0x20) != 0, (tage & 0x10) != 0, (tage & 0x8) != 0, (tage & 0x4) != 0, (tage & 0x2) != 0, (tage & 0x1) != 0, rs.getInt(anzahl));
				a.setId(rs.getInt(id));
				//TODO set description
				aufgaben.add(a);
				rs.next();
			}
			Aufgabe[] auf = new Aufgabe[aufgaben.size()];
			stm.close();
			return aufgaben.toArray(auf);
			
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Bewohner[] getBewohner() {
		try {
			PreparedStatement stm = connection.prepareStatement("SELECT id FROM user WHERE wg_id = ? AND deleted_date IS NULL");
			stm.setInt(1, wg);
			ResultSet rs = stm.executeQuery();
			List<Bewohner> bewohner = new ArrayList<>();
			
			rs.next();
			while(! rs.isAfterLast()){
				bewohner.add(getBewohner(rs.getInt(1)));
				rs.next();
			}
			
			Bewohner[] bew = new Bewohner[bewohner.size()];
			stm.close();
			return bewohner.toArray(bew);
			
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Eintrag[] getEintrage() {
		try {
			PreparedStatement stm = connection.prepareStatement("SELECT * FROM eintraege WHERE wg_id = ?");
			stm.setInt(1, wg);
			ResultSet rs = stm.executeQuery();
			List<Eintrag> eintraege = new ArrayList<>();
			int id = rs.findColumn("id");
			int name = rs.findColumn("name");
			int anzahl = rs.findColumn("anzahl");
			
			rs.next();
			while(! rs.isAfterLast()){
				List<Bewohner> bewohner = new ArrayList<>();
				PreparedStatement pstm = connection.prepareStatement("SELECT user_id FROM beteiligung WHERE eintrag_id = ?");
				pstm.setInt(1, rs.getInt(id));
				ResultSet rs2 = pstm.executeQuery();
				rs2.next();
				while(! rs2.isAfterLast()){
					bewohner.add(getBewohner(rs2.getInt(1)));
					rs2.next();
				}
				pstm.close();
				Bewohner[] bew = new Bewohner[bewohner.size()];
				Eintrag e = new Eintrag(rs.getString(name), rs.getInt(anzahl), bewohner.toArray(bew));
				e.setId(rs.getInt(id));
				eintraege.add(e);
				rs.next();
			}
			Eintrag[] ein = new Eintrag[eintraege.size()];
			stm.close();
			return eintraege.toArray(ein);
			
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Bewohner getBewohner(int id) {
		PreparedStatement stm;
		try {
			stm = connection.prepareStatement("SELECT * FROM user WHERE id = ?");
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();
			int vorname = rs.findColumn("first_name");
			int nachname = rs.findColumn("last_name");
			int telefon = rs.findColumn("phone_num");
			int geburt = rs.findColumn("birth_date");
			int kuerzel = rs.findColumn("kuerzel");
			int admin = rs.findColumn("is_admin");
			
			Bewohner b = new Bewohner(rs.getString(vorname), rs.getString(nachname), rs.getDate(geburt), rs.getString(telefon));
			b.setKurz(rs.getString(kuerzel));
			b.setBenutzername(rs.getString(kuerzel));
			b.setId(id);
			if(rs.getBoolean(admin)){
				b.makeAdmin();
			}
			stm.close();
			return b;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Rechnung[] getRechnungen() {
		try {
			PreparedStatement stm = connection.prepareStatement("SELECT * FROM finanzen WHERE wg_id = ?");
			stm.setInt(1, wg);
			ResultSet rs = stm.executeQuery();
			List<Rechnung> rechnungen = new ArrayList<>();
			int id = rs.findColumn("id");
			int desc = rs.findColumn("descrip");
			int ersteller = rs.findColumn("user_id");
			int betrag = rs.findColumn("betrag");
			
			rs.next();
			while(! rs.isAfterLast()){
				List<Bewohner> bewohner = new ArrayList<>();
				PreparedStatement pstm = connection.prepareStatement("SELECT user_id FROM geldfluss WHERE finanz_id = ?");
				pstm.setInt(1, rs.getInt(id));
				ResultSet rs2 = pstm.executeQuery();
				rs2.next();
				while(! rs2.isAfterLast()){
					bewohner.add(getBewohner(rs2.getInt(1)));
					rs2.next();
				}
				pstm.close();
				Bewohner[] bew = new Bewohner[bewohner.size()];
				Rechnung r = new Rechnung(getBewohner(rs.getInt(ersteller)), bewohner.toArray(bew), rs.getString(desc), rs.getInt(betrag));
				rechnungen.add(r);
				rs.next();
			}
			Rechnung[] re = new Rechnung[rechnungen.size()];
			stm.close();
			return rechnungen.toArray(re);
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Termin[] getTermine(int user) {
		try {
			PreparedStatement stm = connection.prepareStatement("SELECT temin_id FROM termine_sichtbar WHERE user_id = ?");
			stm.setInt(1, user);
			ResultSet rs = stm.executeQuery();
			int tid = rs.findColumn("id");
			rs.next();
			List<Termin> termine = new ArrayList<>();
			while(! rs.isAfterLast()){
				PreparedStatement pstm = connection.prepareStatement("SELECT * FROM Termine WHERE id = ?");
				pstm.setInt(1, tid);
				ResultSet rs2 = stm.executeQuery();
				
				int id = rs.findColumn("id");
				int name = rs2.findColumn("name");
				int desc = rs2.findColumn("descrip");
				int ersteller = rs2.findColumn("creator_id");
				int start = rs.findColumn("start");
				int end = rs.findColumn("end");
				int ganztags = rs.findColumn("ganztags");
			
				List<Bewohner> bewohner = new ArrayList<>();
				PreparedStatement pstm2 = connection.prepareStatement("SELECT user_id FROM geldfluss WHERE finanz_id = ?");
				pstm.setInt(1, rs.getInt(id));
				ResultSet rs3 = pstm2.executeQuery();
				rs3.next();
				while(! rs3.isAfterLast()){
					bewohner.add(getBewohner(rs3.getInt(1)));
					rs3.next();
				}
				pstm2.close();
				
				Bewohner[] bew = new Bewohner[bewohner.size()];
				Calendar c1 = Calendar.getInstance();
				c1.setTime(rs2.getDate(start));
				Calendar c2 = Calendar.getInstance();
				c2.setTime(rs2.getDate(end));
				Termin t = new Termin(getBewohner(rs2.getInt(ersteller)), rs2.getString(name), c1, c2, bewohner.toArray(bew), rs2.getBoolean(ganztags));
				t.setId(rs2.getInt(id));
				//TODO set description
				termine.add(t);
				
				pstm.close();
			}
			Termin[] ter = new Termin[termine.size()];
			return termine.toArray(ter);
			
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public Bewohner checkLogin(String username, String password) {
		try {
			PreparedStatement stm = connection.prepareStatement("SELECT id FROM user WHERE kuerzel = ? AND pw_hash = ? AND deleted_date IS NULL");
			stm.setString(1, username);
			stm.setString(1, password);
			ResultSet rs = stm.executeQuery();
			rs.next();
			Bewohner bewohner = null;
			if(! rs.isAfterLast()){
				getBewohner(rs.getInt(1));
			}
			stm.close();
			return bewohner;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
